#ifndef VGPUGETTERUNIFORM_H
#define VGPUGETTERUNIFORM_H

#include <cstdint>
#include "common/assert.hpp"
#include "common/logger.hpp"

namespace vgpu {

class GetterUniform {
public:
    template<typename T>
    void Set(T value) {
        VGPU_STATIC_ERROR("Wrong usage of GetterUniform Set method");
    }
    template<>
    void Set(bool value) {
        boolean_ = value;

        auto v = value ? GL_TRUE : GL_FALSE;
        double_ = static_cast<double>(v);
        float_ = static_cast<float>(v);
        integer_ = static_cast<int>(v);
        integer64_ = static_cast<uint64_t>(v);
    }
    template<>
    void Set(double value) {
        double_ = value;

        boolean_ = value != 0.0;
        float_ = static_cast<float>(value);
        VGPU_LOG_IF(sizeof(int) == sizeof(long), "Warning: Make sure double=>int conversion is correct");
        VGPU_LOG_IF(sizeof(int64_t) == sizeof(long long), "Warning: Make sure double=>int64 conversion is correct");
        integer_ = lround(value);
        integer64_ = llround(value);
    }
    template<>
    void Set(float value) {
        float_ = value;

        boolean_ = value != 0.0f;
        double_ = value;
        VGPU_LOG_IF(sizeof(int) == sizeof(long), "Warning: Make sure float=>int conversion is correct");
        VGPU_LOG_IF(sizeof(int64_t) == sizeof(long long), "Warning: Make sure float=>int64 conversion is correct");
        integer_ = lround(value);
        integer64_ = llround(value);
    }
    template<>
    void Set(int value) {
        integer_ = value;

        boolean_ = value != 0;
        double_ = static_cast<double>(value);
        float_ = static_cast<float>(value);
        integer64_ = value;
    }
    template<>
    void Set(int64_t value) {
        integer64_ = value;

        boolean_ = value != 0;
        double_ = static_cast<double>(value);
        float_ = static_cast<float>(value);
        VGPU_LOG_IF(value > std::numeric_limits<int>::max() || value < std::numeric_limits<int>::min(),
                    "Warning: Value cannot be represented by int(32) - need reimplementation");
        integer_ = static_cast<int>(value);
    }
    template<typename T>
    void SetInterpolated(T value) {
        VGPU_STATIC_ERROR("Wrong usage of GetterUniform SetInterpolated method");
    }
    bool GetBoolean() const {
        return boolean_;
    }
    double GetDouble() const {
        return double_;
    }
    float GetFloat() const {
        return float_;
    }
    int GetInteger() const {
        return integer_;
    }
    int64_t GetInteger64() const {
        return integer64_;
    }

private:
    bool boolean_{false};
    double double_{0.0};
    float float_{0.0f};
    int integer_{0};
    int64_t integer64_{0};
};

}

#endif // !VGPUGETTERUNIFORM_H
