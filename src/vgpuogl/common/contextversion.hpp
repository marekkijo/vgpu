#ifndef VGPUCONTEXTVERSION_H
#define VGPUCONTEXTVERSION_H

#include <cstdint>

namespace vgpu {

enum class ContextVersion : uint16_t {
    Undefined       = 0x0000,
    Version_1_0     = 0x0100,
    Version_1_1     = 0x0101,
    Version_1_2     = 0x0102,
    Version_1_3     = 0x0103,
    Version_1_4     = 0x0104,
    Version_1_5     = 0x0105,
    Version_2_0     = 0x0200,
    Version_2_1     = 0x0201,
    Version_3_0     = 0x0300,
    Version_3_1     = 0x0301,
    Version_3_2     = 0x0302,
    Version_3_3     = 0x0303,
    Version_4_0     = 0x0400,
    Version_4_1     = 0x0401,
    Version_4_2     = 0x0402,
    Version_4_3     = 0x0403,
    Version_4_4     = 0x0404,
    Version_4_5     = 0x0405
};

constexpr ContextVersion ContextVersion_ToContextVersion(uint16_t major, uint16_t minor) {
    return
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_0) ?
        ContextVersion::Version_1_0 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_1) ?
        ContextVersion::Version_1_1 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_2) ?
        ContextVersion::Version_1_2 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_3) ?
        ContextVersion::Version_1_3 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_4) ?
        ContextVersion::Version_1_4 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_1_5) ?
        ContextVersion::Version_1_5 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_2_0) ?
        ContextVersion::Version_2_0 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_2_1) ?
        ContextVersion::Version_2_1 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_3_0) ?
        ContextVersion::Version_3_0 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_3_1) ?
        ContextVersion::Version_3_1 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_3_2) ?
        ContextVersion::Version_3_2 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_3_3) ?
        ContextVersion::Version_3_3 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_0) ?
        ContextVersion::Version_4_0 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_1) ?
        ContextVersion::Version_4_1 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_2) ?
        ContextVersion::Version_4_2 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_3) ?
        ContextVersion::Version_4_3 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_4) ?
        ContextVersion::Version_4_4 :
        (major << 8 | minor) == static_cast<uint16_t>(ContextVersion::Version_4_5) ?
        ContextVersion::Version_4_5 :
        ContextVersion::Undefined;
}

constexpr uint16_t ContextVersion_ToVersionMajor(ContextVersion cv) {
    return static_cast<uint16_t>(cv) & 0xFF00 >> 8;
}

constexpr uint16_t ContextVersion_ToVersionMinor(ContextVersion cv) {
    return static_cast<uint16_t>(cv) & 0x00FF;
}

}

#endif // !VGPUCONTEXTVERSION_H