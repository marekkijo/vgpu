#ifndef VGPUFRAGMENTSHADER_H
#define VGPUFRAGMENTSHADER_H

#include "shader.hpp"
#include "shadertype.hpp"

namespace vgpu {

class FragmentShader : public Shader {
public:
    ShaderType GetShaderType() const override {
        return ShaderType::Fragment;
    }
};

}

#endif // !VGPUFRAGMENTSHADER_H
