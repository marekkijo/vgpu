#ifndef VGPUVERTEXSHADER_H
#define VGPUVERTEXSHADER_H

#include "shader.hpp"
#include "shadertype.hpp"

namespace vgpu {

class VertexShader : public Shader {
public:
    ShaderType GetShaderType() const override {
        return ShaderType::Vertex;
    }
};

}

#endif // !VGPUVERTEXSHADER_H
