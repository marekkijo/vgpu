#ifndef VGPUWIN32CONTEXT_H
#define VGPUWIN32CONTEXT_H

#include <memory>
#include <map>
#include <string>
#include "vgpuicd.hpp"
#include "contextprivateresources.hpp"
#include "common/contextdescriptor.hpp"
#include "general_purpose/getteruniform.hpp"
#include "gl/enums.hpp"
#include "state/state.hpp"

namespace vgpu {

class VGPU;
class ContextSharedResources;

class Win32Context {
public:
    Win32Context(HDC hdc, const ContextDescriptor &cd, std::shared_ptr<VGPU> vgpu, std::shared_ptr<Win32Context> shared_context = nullptr);

    static PGLCLTPROCTABLE GetGL110ProcTable();
    static PROC wgl_GetProcAddress(LPCSTR lpszProc);

    void gl_Finish();
    void gl_Flush();
    void gl_SetError(gl::Error error);
    void gl_GetBooleanv(gl::ParamName param_name, GLboolean *data);
    void gl_GetDoublev(gl::ParamName param_name, GLdouble *data);
    GLenum gl_GetError();
    void gl_GetFloatv(gl::ParamName param_name, GLfloat *data);
    void gl_GetIntegerv(gl::ParamName param_name, GLint *data);
    const GLubyte *gl_GetString(gl::StringName name) const;
    // GL_VERSION_2_0
    GLuint gl_CreateShader(gl::ShaderType shader_type);
    void gl_ShaderSource(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
    // GL_VERSION_3_2
    void gl_GetInteger64v(gl::ParamName param_name, GLint64 *data);
    
    const ContextDescriptor &GetContextDescriptor() const;
    void SetSharedContext(std::shared_ptr<Win32Context> shared_context);

private:
    static const uint32_t ogl_110_entries_{336};
    static const GLCLTPROCTABLE gl_proc_table_;
    static const std::map<std::string, void *> wgl_proc_map_;
    static const std::map<std::string, void *> gl_proc_map_;
    // returns true if there exist value for (index + 1) of given param_name
    bool gl_Get(gl::ParamName param_name, size_t index, GetterUniform &data);

    HDC hdc_{NULL};
    ContextDescriptor cd_{ContextVersion::Undefined};
    std::shared_ptr<VGPU> vgpu_;
    State state_;
    ContextPrivateResources private_resources_;
    std::shared_ptr<ContextSharedResources> shared_resources_{nullptr};
    gl::Error error_{gl::Error::NoError};
};

}

#endif // !VGPUWIN32CONTEXT_H
