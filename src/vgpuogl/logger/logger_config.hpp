#ifndef VGPULOGGER_CONFIG_H
#define VGPULOGGER_CONFIG_H

#if _DEBUG
# define VGPU_LOG_GENERAL_IS_ON     1
# define VGPU_LOG_ICD_IS_ON         1
# define VGPU_LOG_WGL_IS_ON         1
# define VGPU_LOG_GL_IS_ON          1
# define VGPU_LOG_DRV_IS_ON         1
# define VGPU_LOG_CTX_IS_ON         1
# define VGPU_LOG_VGPU_IS_ON        1

# define VGPU_LOG_TO_FILE           1
# define VGPU_LOG_FILE_LOCATION     "C:\\vgpu\\"
# define VGPU_LOG_FILE_LOCATION_W   L"C:\\vgpu\\"
# define VGPU_LOG_ARG_TYPE_ON       1
# define VGPU_LOG_LABEL_ON          1
# define VGPU_LOG_TIMER_ON          1
# define VGPU_LOG_TIMER_DURATION    1 // if == 1 then timer logs are durations between every single log
                                      // if == 0 then timer logs are milliseconds from first log
# define VGPU_LOG_HEX_INTEGER       1
#else // !_DEBUG
#endif // _DEBUG

#endif // !VGPULOGGER_CONFIG_H
