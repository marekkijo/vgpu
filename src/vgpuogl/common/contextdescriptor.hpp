#ifndef VGPUCONTEXTDESCRIPTOR_H
#define VGPUCONTEXTDESCRIPTOR_H

#include "common/contextversion.hpp"
#include "pixelformat/pixelformat.hpp"

namespace vgpu {

class ContextDescriptor {
public:
    ContextDescriptor(
        ContextVersion version, int layer_plane = 0,
        bool debug = false, bool forward_compatible = false,
        bool core_profile = false, bool compatibility_profile = false) :
        version_{version}, layer_plane_{layer_plane},
        debug_{debug}, forward_compatible_{forward_compatible},
        core_profile_{core_profile}, compatibility_profile_{compatibility_profile} {
    }
    ContextDescriptor(
        const ContextDescriptor &cd,
        PixelFormat color_pf, PixelFormat depth_stencil_pf) :
        version_{cd.version_}, layer_plane_{cd.layer_plane_},
        debug_{cd.debug_}, forward_compatible_{cd.forward_compatible_},
        core_profile_{cd.core_profile_}, compatibility_profile_{cd.compatibility_profile_},
        color_pf_{color_pf}, depth_stencil_pf_{depth_stencil_pf} {
    }

    ContextVersion GetVersion() const {
        return version_;
    }
    int GetVersionMajor() const {
        return ContextVersion_ToVersionMajor(version_);
    }
    int GetVersionMinor() const {
        return ContextVersion_ToVersionMinor(version_);
    }
    int GetLayerPlane() const {
        return layer_plane_;
    }
    bool IsDebug() const {
        return debug_;
    }
    bool IsForwardCompatible() const {
        return forward_compatible_;
    }
    bool IsCoreProfile() const {
        return core_profile_;
    }
    bool IsCompatibilityProfile() const {
        return compatibility_profile_;
    }
    PixelFormat GetColorPixelFormat() {
        return color_pf_;
    }
    PixelFormat GetDepthStencilPixelFormat() {
        return depth_stencil_pf_;
    }

private:
    ContextVersion version_{ContextVersion::Undefined};
    int layer_plane_{0};
    bool debug_{false};
    bool forward_compatible_{false};
    bool core_profile_{false};
    bool compatibility_profile_{false};
    PixelFormat color_pf_{PixelFormat::Undefined};
    PixelFormat depth_stencil_pf_{PixelFormat::Undefined};
};

}

#endif // !VGPUCONTEXTDESCRIPTOR_H
