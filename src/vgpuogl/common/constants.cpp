#include "constants.hpp"

namespace vgpu {

const std::map<ContextVersion, const char *> Constants::gl_version_{
    {ContextVersion::Version_1_0, "OpenGL 1.0.0 VGPU 0.01"},
    {ContextVersion::Version_1_1, "OpenGL 1.1.0 VGPU 0.01"},
    {ContextVersion::Version_1_2, "OpenGL 1.2.0 VGPU 0.01"},
    {ContextVersion::Version_1_3, "OpenGL 1.3.0 VGPU 0.01"},
    {ContextVersion::Version_1_4, "OpenGL 1.4.0 VGPU 0.01"},
    {ContextVersion::Version_1_5, "OpenGL 1.5.0 VGPU 0.01"},
    {ContextVersion::Version_2_0, "OpenGL 2.0.0 VGPU 0.01"},
    {ContextVersion::Version_2_1, "OpenGL 2.1.0 VGPU 0.01"},
    {ContextVersion::Version_3_0, "OpenGL 3.0.0 VGPU 0.01"},
    {ContextVersion::Version_3_1, "OpenGL 3.1.0 VGPU 0.01"},
    {ContextVersion::Version_3_2, "OpenGL 3.2.0 VGPU 0.01"},
    {ContextVersion::Version_3_3, "OpenGL 3.3.0 VGPU 0.01"},
    {ContextVersion::Version_4_0, "OpenGL 4.0.0 VGPU 0.01"},
    {ContextVersion::Version_4_1, "OpenGL 4.1.0 VGPU 0.01"},
    {ContextVersion::Version_4_2, "OpenGL 4.2.0 VGPU 0.01"},
    {ContextVersion::Version_4_3, "OpenGL 4.3.0 VGPU 0.01"},
    {ContextVersion::Version_4_4, "OpenGL 4.4.0 VGPU 0.01"},
    {ContextVersion::Version_4_5, "OpenGL 4.5.0 VGPU 0.01"},
    {ContextVersion::Undefined, "ERROR"}
};

const std::map<ContextVersion, const char *> Constants::gl_shading_language_version_{
    {ContextVersion::Version_1_0, "none"},
    {ContextVersion::Version_1_1, "none"},
    {ContextVersion::Version_1_2, "none"},
    {ContextVersion::Version_1_3, "none"},
    {ContextVersion::Version_1_4, "none"},
    {ContextVersion::Version_1_5, "none"},
    {ContextVersion::Version_2_0, "OpenGL GLSL 1.10"},
    {ContextVersion::Version_2_1, "OpenGL GLSL 1.20"},
    {ContextVersion::Version_3_0, "OpenGL GLSL 1.30"},
    {ContextVersion::Version_3_1, "OpenGL GLSL 1.40"},
    {ContextVersion::Version_3_2, "OpenGL GLSL 1.50"},
    {ContextVersion::Version_3_3, "OpenGL GLSL 3.30"},
    {ContextVersion::Version_4_0, "OpenGL GLSL 4.00"},
    {ContextVersion::Version_4_1, "OpenGL GLSL 4.10"},
    {ContextVersion::Version_4_2, "OpenGL GLSL 4.20"},
    {ContextVersion::Version_4_3, "OpenGL GLSL 4.30"},
    {ContextVersion::Version_4_4, "OpenGL GLSL 4.40"},
    {ContextVersion::Version_4_5, "OpenGL GLSL 4.50"},
    {ContextVersion::Undefined, "ERROR"}
};

}
