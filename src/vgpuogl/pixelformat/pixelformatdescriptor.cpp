#include "pixelformatdescriptor.hpp"
#include <iostream>
#include "common/assert.hpp"

namespace vgpu {

const PixelFormatDescriptor &PixelFormatDescriptor::Get(PixelFormat pf) {
    VGPU_ASSERT(pfds_.count(pf) == 1, "Unknown format requested");
    return pfds_.at(pf);
}

bool PixelFormatDescriptor::HasChannel(ChannelType ct) const {
    return ChannelTypeToIndex(ct) != -1;
}

int PixelFormatDescriptor::ChannelTypeToIndex(ChannelType ct) const {
    for (int i = 0; i < static_cast<int>(channels_.size()); i++) {
        if (channels_[i] == ct) {
            return i;
        }
    }
    return -1;
}

ChannelType PixelFormatDescriptor::IndexToChannelType(int index) const {
    if (index < 0 || index >= 4)
        return ChannelType::Undefined;
    return channels_[index];
}

uint8_t PixelFormatDescriptor::GetBits(ChannelType ct) const {
    return GetBits(ChannelTypeToIndex(ct));
}

uint8_t PixelFormatDescriptor::GetBits(int index) const {
    if (index < 0 || index >= 4)
        return 0;
    return bits_[index];
}

uint8_t PixelFormatDescriptor::GetShift(ChannelType ct) const {
    return GetShift(ChannelTypeToIndex(ct));
}

uint8_t PixelFormatDescriptor::GetShift(int index) const {
    if (index < 0 || index >= 4)
        return 0;
    return shift_[index];
}

uint8_t PixelFormatDescriptor::GetTexelSize() const {
    return texel_size;
}

std::map<PixelFormat, PixelFormatDescriptor> PixelFormatDescriptor::pfds_{
    {PixelFormat::Undefined, PixelFormatDescriptor{PixelFormat::Undefined}},
    {PixelFormat::B8G8R8A8_UNorm, PixelFormatDescriptor{PixelFormat::B8G8R8A8_UNorm}},
    {PixelFormat::B8G8R8X8_UNorm, PixelFormatDescriptor{PixelFormat::B8G8R8X8_UNorm}},
    {PixelFormat::A8R8G8B8_UNorm, PixelFormatDescriptor{PixelFormat::A8R8G8B8_UNorm}},
    {PixelFormat::X8R8G8B8_UNorm, PixelFormatDescriptor{PixelFormat::X8R8G8B8_UNorm}},
    {PixelFormat::B5G5R5A1_UNorm, PixelFormatDescriptor{PixelFormat::B5G5R5A1_UNorm}},
    {PixelFormat::B4G4R4A4_UNorm, PixelFormatDescriptor{PixelFormat::B4G4R4A4_UNorm}},
    {PixelFormat::B5G6R5_UNorm, PixelFormatDescriptor{PixelFormat::B5G6R5_UNorm}},
    {PixelFormat::R10G10B10A2_UNorm, PixelFormatDescriptor{PixelFormat::R10G10B10A2_UNorm}},
    {PixelFormat::Z16_UNorm, PixelFormatDescriptor{PixelFormat::Z16_UNorm}},
    {PixelFormat::Z32_UNorm, PixelFormatDescriptor{PixelFormat::Z32_UNorm}},
    {PixelFormat::Z24_UNorm_S8_UInt, PixelFormatDescriptor{PixelFormat::Z24_UNorm_S8_UInt}},
    {PixelFormat::S8_UInt_Z24_UNorm, PixelFormatDescriptor{PixelFormat::S8_UInt_Z24_UNorm}},
    {PixelFormat::Z24X8_UNorm, PixelFormatDescriptor{PixelFormat::Z24X8_UNorm}},
    {PixelFormat::X8Z24_UNorm, PixelFormatDescriptor{PixelFormat::X8Z24_UNorm}},
    {PixelFormat::S8_UInt, PixelFormatDescriptor{PixelFormat::S8_UInt}}
};

PixelFormatDescriptor::PixelFormatDescriptor(PixelFormat pf) :
    pf_{pf} {
    UpdateChannelIndices();
    UpdateChannelBits();
    UpdateChannelShift();
    UpdateTexelSize();
}

void PixelFormatDescriptor::UpdateChannelIndices() {
    switch (pf_) {
    case PixelFormat::Undefined:
        break;

    case PixelFormat::R8G8B8A8_UNorm:
    case PixelFormat::R10G10B10A2_UNorm:
        channels_[3] = ChannelType::Alpha;
        // no_break
    case PixelFormat::R8G8B8X8_UNorm:
        channels_[0] = ChannelType::Red;
        channels_[1] = ChannelType::Green;
        channels_[2] = ChannelType::Blue;
        break;

    case PixelFormat::B8G8R8A8_UNorm:
    case PixelFormat::B5G5R5A1_UNorm:
    case PixelFormat::B4G4R4A4_UNorm:
        channels_[3] = ChannelType::Alpha;
        // no_break
    case PixelFormat::B8G8R8X8_UNorm:
    case PixelFormat::B5G6R5_UNorm:
        channels_[0] = ChannelType::Blue;
        channels_[1] = ChannelType::Green;
        channels_[2] = ChannelType::Red;
        break;

    case PixelFormat::A8R8G8B8_UNorm:
        channels_[0] = ChannelType::Alpha;
        // no_break
    case PixelFormat::X8R8G8B8_UNorm:
        channels_[1] = ChannelType::Red;
        channels_[2] = ChannelType::Green;
        channels_[3] = ChannelType::Blue;
        break;

    case PixelFormat::Z16_UNorm:
    case PixelFormat::Z32_UNorm:
        channels_[0] = ChannelType::Depth;
        break;

    case PixelFormat::Z24_UNorm_S8_UInt:
        channels_[0] = ChannelType::Depth;
        channels_[1] = ChannelType::Stencil;
        break;

    case PixelFormat::S8_UInt_Z24_UNorm:
        channels_[0] = ChannelType::Stencil;
        channels_[1] = ChannelType::Depth;
        break;

    case PixelFormat::Z24X8_UNorm:
        channels_[0] = ChannelType::Depth;
        break;

    case PixelFormat::X8Z24_UNorm:
        channels_[1] = ChannelType::Depth;
        break;

    case PixelFormat::S8_UInt:
        channels_[0] = ChannelType::Stencil;
        break;

    default:
        VGPU_ERROR("Unhandled PixelFormat");
        break;
    }
}

void PixelFormatDescriptor::UpdateChannelBits() {
    switch (pf_) {
    case PixelFormat::Undefined:
        break;

    case PixelFormat::R8G8B8A8_UNorm:
    case PixelFormat::B8G8R8A8_UNorm:
        bits_[3] = 8;
        // no_break
    case PixelFormat::R8G8B8X8_UNorm:
    case PixelFormat::B8G8R8X8_UNorm:
        bits_[0] = 8;
        bits_[1] = 8;
        bits_[2] = 8;
        break;

    case PixelFormat::A8R8G8B8_UNorm:
        bits_[0] = 8;
        // no_break
    case PixelFormat::X8R8G8B8_UNorm:
        bits_[1] = 8;
        bits_[2] = 8;
        bits_[3] = 8;
        break;

    case PixelFormat::B5G5R5A1_UNorm:
        bits_[3] = 1;
        // no_break
    case PixelFormat::B5G6R5_UNorm:
        bits_[0] = 5;
        bits_[1] = 5;
        bits_[2] = 5;
        break;

    case PixelFormat::B4G4R4A4_UNorm:
        bits_[0] = 4;
        bits_[1] = 4;
        bits_[2] = 4;
        bits_[3] = 4;
        break;

    case PixelFormat::R10G10B10A2_UNorm:
        bits_[0] = 10;
        bits_[1] = 10;
        bits_[2] = 10;
        bits_[3] = 2;
        break;

    case PixelFormat::Z16_UNorm:
        bits_[0] = 16;
        break;

    case PixelFormat::Z32_UNorm:
        bits_[0] = 32;
        break;

    case PixelFormat::Z24_UNorm_S8_UInt:
        bits_[0] = 24;
        bits_[1] = 8;
        break;

    case PixelFormat::S8_UInt_Z24_UNorm:
        bits_[0] = 8;
        bits_[1] = 24;
        break;

    case PixelFormat::Z24X8_UNorm:
        bits_[0] = 24;
        break;

    case PixelFormat::X8Z24_UNorm:
        bits_[1] = 24;
        break;

    case PixelFormat::S8_UInt:
        bits_[0] = 8;
        break;

    default:
        VGPU_ERROR("Unhandled PixelFormat");
        break;
    }
}

void PixelFormatDescriptor::UpdateChannelShift() {
    switch (pf_) {
    case PixelFormat::Undefined:
        break;

    case PixelFormat::R8G8B8A8_UNorm:
    case PixelFormat::B8G8R8A8_UNorm:
    case PixelFormat::A8R8G8B8_UNorm:
    case PixelFormat::X8R8G8B8_UNorm:
        shift_[3] = 24;
        // no_break
    case PixelFormat::B8G8R8X8_UNorm:
    case PixelFormat::R8G8B8X8_UNorm:
        shift_[0] = 0;
        shift_[1] = 8;
        shift_[2] = 16;
        break;

    case PixelFormat::B5G5R5A1_UNorm:
        shift_[3] = 15;
        // no_break
    case PixelFormat::B5G6R5_UNorm:
        shift_[0] = 0;
        shift_[1] = 5;
        shift_[2] = 10;
        break;

    case PixelFormat::B4G4R4A4_UNorm:
        shift_[0] = 0;
        shift_[1] = 4;
        shift_[2] = 8;
        shift_[3] = 12;
        break;

    case PixelFormat::R10G10B10A2_UNorm:
        shift_[0] = 0;
        shift_[1] = 10;
        shift_[2] = 20;
        shift_[3] = 30;
        break;

    case PixelFormat::S8_UInt_Z24_UNorm:
    case PixelFormat::X8Z24_UNorm:
        shift_[1] = 8;
        // no_break
    case PixelFormat::Z16_UNorm:
    case PixelFormat::Z32_UNorm:
    case PixelFormat::Z24X8_UNorm:
    case PixelFormat::S8_UInt:
        shift_[0] = 0;
        break;

    case PixelFormat::Z24_UNorm_S8_UInt:
        shift_[0] = 0;
        shift_[1] = 24;
        break;

    default:
        VGPU_ERROR("Unhandled PixelFormat");
        break;
    }
}

void PixelFormatDescriptor::UpdateTexelSize() {
    switch (pf_) {
    case PixelFormat::Undefined:
        break;

    case PixelFormat::R8G8B8A8_UNorm:
    case PixelFormat::B8G8R8A8_UNorm:
    case PixelFormat::B8G8R8X8_UNorm:
    case PixelFormat::R8G8B8X8_UNorm:
    case PixelFormat::A8R8G8B8_UNorm:
    case PixelFormat::X8R8G8B8_UNorm:
    case PixelFormat::R10G10B10A2_UNorm:
    case PixelFormat::Z32_UNorm:
    case PixelFormat::Z24_UNorm_S8_UInt:
    case PixelFormat::S8_UInt_Z24_UNorm:
    case PixelFormat::Z24X8_UNorm:
    case PixelFormat::X8Z24_UNorm:
        texel_size = 4;
        break;

    case PixelFormat::B5G5R5A1_UNorm:
    case PixelFormat::B4G4R4A4_UNorm:
    case PixelFormat::B5G6R5_UNorm:
    case PixelFormat::Z16_UNorm:
        texel_size = 2;
        break;

    case PixelFormat::S8_UInt:
        texel_size = 1;
        break;

    default:
        VGPU_ERROR("Unhandled PixelFormat");
        break;
    }
}

}
