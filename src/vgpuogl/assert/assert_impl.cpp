#include "assert_impl.hpp"
#include <cassert>

namespace vgpu {

#if _DEBUG
void Assert(bool test, const std::string &msg) {
    (test || (_wassert(std::wstring(msg.begin(), msg.end()).c_str(), _CRT_WIDE(__FILE__), (unsigned)(__LINE__)), 0));
}
#endif // _DEBUG

}
