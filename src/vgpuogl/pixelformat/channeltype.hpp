#ifndef VGPUCHANNELTYPE_H
#define VGPUCHANNELTYPE_H

namespace vgpu {

enum class ChannelType {
    Undefined,
    Red,
    Green,
    Blue,
    Alpha,
    Depth,
    Stencil
};

}

#endif // !VGPUCHANNELTYPE_H