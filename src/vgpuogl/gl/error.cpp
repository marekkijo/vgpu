#include "error.hpp"

namespace vgpu {
namespace gl {

bool ToError(GLenum arg, Error &error) {
    Error tmp_error = static_cast<Error>(arg);

    switch (tmp_error) {
    case Error::NoError:
    case Error::InvalidEnum:
    case Error::InvalidValue:
    case Error::InvalidOperation:
    case Error::InvalidFrameBufferOperation:
    case Error::OutOfMemory:
    case Error::StackUnderflow:
    case Error::StackOverflow:
        break;
    default:
        return false;
    }

    error = tmp_error;
    return true;
}

std::ostream &operator<<(std::ostream &os, Error error) {
    switch (error) {
    case Error::NoError:
        os << "Error::NoError";
        break;
    case Error::InvalidEnum:
        os << "Error::InvalidEnum";
        break;
    case Error::InvalidValue:
        os << "Error::InvalidValue";
        break;
    case Error::InvalidOperation:
        os << "Error::InvalidOperation";
        break;
    case Error::InvalidFrameBufferOperation:
        os << "Error::InvalidFrameBufferOperation";
        break;
    case Error::OutOfMemory:
        os << "Error::OutOfMemory";
        break;
    case Error::StackUnderflow:
        os << "Error::StackUnderflow";
        break;
    case Error::StackOverflow:
        os << "Error::StackOverflow";
        break;
    }

    return os;
}

}
}
