#ifndef VGPUWIN32DRIVER_H
#define VGPUWIN32DRIVER_H

#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include "vgpuicd.hpp"
#include "common/contextdescriptor.hpp"
#include "pixelformat/pixelformat.hpp"
#include "vgpu/vgpu.hpp"

namespace vgpu {

class Win32Context;

class Win32Driver {
public:
    Win32Driver();
    ~Win32Driver();

    DHGLRC icd_CreateContext(HDC hdc, INT iLayerPlane = 0);
    BOOL icd_DeleteContext(DHGLRC dhglrc);
    LONG icd_DescribePixelFormat(HDC hdc, LONG iPixelFormat, ULONG cjpfd, PIXELFORMATDESCRIPTOR *ppfd);
    PROC icd_GetProcAddress(LPCSTR lpszProc);
    BOOL icd_ReleaseContext(DHGLRC dhglrc);
    void icd_SetCallbackProcs(INT nProcs, PROC *pProcs);
    PGLCLTPROCTABLE icd_SetContext(HDC hdc, DHGLRC dhglrc, PFN_SETPROCTABLE pfnSetProcTable);
    BOOL icd_SetPixelFormat(HDC hdc, LONG iPixelFormat);
    BOOL icd_SwapBuffers(HDC hdc);
    BOOL icd_ValidateVersion(ULONG ulVersion);

    void icd_ThreadAttach();
    void icd_ThreadDetach();

    BOOL wgl_ChoosePixelFormat(HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);
    HGLRC wgl_CreateContextAttribs(HDC hdc, HGLRC hShareContext, const int *attribList);

    std::shared_ptr<Win32Context> CurrentContext() {
        return thread_current_contex_;
    }

private:
    struct CallbackTable {
        PFN_SETCURRENTVALUE SetCurrentValue{nullptr};
        PFN_GETCURRENTVALUE GetCurrentValue{nullptr};
        PFN_GETDHGLRC GetDHGLRC{nullptr};
        PFN_GETDDHANDLE GetDDHandle{nullptr};
        PFN_PRESENTBUFFERS PresentBuffers{nullptr};
    };

    struct PixelFormatEntity {
        PIXELFORMATDESCRIPTOR pfd;
        PixelFormat color_format;
        PixelFormat depthstencil_format;
        bool operator<(const Win32Driver::PixelFormatEntity &rhs) const;
    };

    static std::vector<PixelFormatEntity> pfds_;
    static thread_local uintptr_t context_creation_notifier_;
    static thread_local uintptr_t thread_current_pfd_index_;
    static thread_local bool thread_pfd_index_locked_;
    static thread_local std::shared_ptr<Win32Context> thread_current_contex_;

    void GeneratePixelFormats();
    PIXELFORMATDESCRIPTOR AttribsToPixelFormatDescriptor(const int *piAttribIList, const FLOAT *pfAttribFList);
    ContextDescriptor AttribsToContextDescriptor(const int *attribList);
    uintptr_t CreateContext(HDC hdc, const ContextDescriptor &cd, uintptr_t shared_context = 0, bool from_attribs = false);

    std::shared_ptr<VGPU> vgpu_{nullptr};
    std::thread vgpu_thread_;

    std::mutex mutex_;
    CallbackTable wgl_callbacks_;
    std::map<uintptr_t, std::shared_ptr<Win32Context>> contexts_{};
    uintptr_t last_context_id_created_{0};
};

}

#endif // !VGPUWIN32DRIVER_H
