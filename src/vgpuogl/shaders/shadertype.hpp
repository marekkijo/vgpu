#ifndef VGPUSHADERTYPE_H
#define VGPUSHADERTYPE_H

namespace vgpu {

enum class ShaderType {
    Undefined,
    Vertex,
    Fragment
};

}

#endif // !VGPUSHADERTYPE_H
