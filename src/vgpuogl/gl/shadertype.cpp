#include "shadertype.hpp"
#include "common/contextdescriptor.hpp"

namespace vgpu {
namespace gl {

bool ToShaderType(GLenum arg, ShaderType &shader_type, const ContextDescriptor &cd) {
    ShaderType tmp_shader_type = static_cast<ShaderType>(arg);

    switch (tmp_shader_type) {
    case ShaderType::CumputeShader:
    case ShaderType::VertexShader:
    case ShaderType::TessControlShader:
    case ShaderType::TessEvaluationShader:
    case ShaderType::GeometryShader:
    case ShaderType::FragmentShader:
        break;
    default:
        return false;
    }
    
    shader_type = tmp_shader_type;
    return true;
}

std::ostream &operator<<(std::ostream &os, ShaderType shader_type) {
    switch (shader_type) {
    case ShaderType::CumputeShader:
        os << "ShaderType::CumputeShader";
        break;
    case ShaderType::VertexShader:
        os << "ShaderType::VertexShader";
        break;
    case ShaderType::TessControlShader:
        os << "ShaderType::TessControlShader";
        break;
    case ShaderType::TessEvaluationShader:
        os << "ShaderType::TessEvaluationShader";
        break;
    case ShaderType::GeometryShader:
        os << "ShaderType::GeometryShader";
        break;
    case ShaderType::FragmentShader:
        os << "ShaderType::FragmentShader";
        break;
    }

    return os;
}

}
}
