#include "paramname.hpp"
#include "common/contextdescriptor.hpp"

namespace vgpu {
namespace gl {

bool ToParamName(GLenum arg, ParamName &param_name, const ContextDescriptor &cd) {
    ParamName tmp_param_name = static_cast<ParamName>(arg);

    switch (tmp_param_name) {
    case ParamName::AccumAlphaBits:
    case ParamName::AccumBlueBits:
    case ParamName::AccumClearValue:
    case ParamName::AccumGreenBits:
    case ParamName::AccumRedBits:
    case ParamName::AliasedPointSizeRange:
    case ParamName::AlphaBias:
    case ParamName::AlphaBits:
    case ParamName::AlphaScale:
    case ParamName::AlphaTest:
    case ParamName::AlphaTestFunc:
    case ParamName::AlphaTestRef:
    case ParamName::AttribStackDepth:
    case ParamName::AutoNormal:
    case ParamName::AuxBuffers:
    case ParamName::BlueBias:
    case ParamName::BlueBits:
    case ParamName::BlueScale:
    case ParamName::ClientActiveTexture:
    case ParamName::ClientAttribStackDepth:
    case ParamName::ClipPlane0:
    case ParamName::ClipPlane1:
    case ParamName::ClipPlane2:
    case ParamName::ClipPlane3:
    case ParamName::ClipPlane4:
    case ParamName::ClipPlane5:
    case ParamName::ColorArray:
    case ParamName::ColorArrayBufferBinding:
    case ParamName::ColorArraySize:
    case ParamName::ColorArrayStride:
    case ParamName::ColorArrayType:
    case ParamName::ColorMaterial:
    case ParamName::ColorMaterialFace:
    case ParamName::ColorMaterialParameter:
    case ParamName::ColorMatrix:
    case ParamName::ColorMatrixStackDepth:
    case ParamName::ColorSum:
    case ParamName::ColorTable:
    case ParamName::Convolution1d:
    case ParamName::Convolution2d:
    case ParamName::CullFaceMode:
    case ParamName::CurrentColor:
    case ParamName::CurrentFogCoord:
    case ParamName::CurrentIndex:
    case ParamName::CurrentNormal:
    case ParamName::CurrentRasterColor:
    case ParamName::CurrentRasterDistance:
    case ParamName::CurrentRasterIndex:
    case ParamName::CurrentRasterPosition:
    case ParamName::CurrentRasterPositionValid:
    case ParamName::CurrentRasterSecondaryColor:
    case ParamName::CurrentRasterTextureCoords:
    case ParamName::CurrentSecondaryColor:
    case ParamName::CurrentTextureCoords:
    case ParamName::DepthBias:
    case ParamName::DepthBits:
    case ParamName::DepthScale:
    case ParamName::EdgeFlag:
    case ParamName::EdgeFlagArray:
    case ParamName::EdgeFlagArrayBufferBinding:
    case ParamName::EdgeFlagArrayStride:
    case ParamName::FeedbackBufferSize:
    case ParamName::FeedbackBufferType:
    case ParamName::Fog:
    case ParamName::FogColor:
    case ParamName::FogCoordArray:
    case ParamName::FogCoordArrayBufferBinding:
    case ParamName::FogCoordArrayStride:
    case ParamName::FogCoordArrayType:
    case ParamName::FogCoordSrc:
    case ParamName::FogDensity:
    case ParamName::FogEnd:
    case ParamName::FogHint:
    case ParamName::FogIndex:
    case ParamName::FogMode:
    case ParamName::FogStart:
    case ParamName::FrontFace:
    case ParamName::GenerateMipmapHint:
    case ParamName::GreenBias:
    case ParamName::GreenBits:
    case ParamName::GreenScale:
    case ParamName::Histogram:
    case ParamName::IndexArray:
    case ParamName::IndexArrayBufferBinding:
    case ParamName::IndexArrayStride:
    case ParamName::IndexArrayType:
    case ParamName::IndexBits:
    case ParamName::IndexClearValue:
    case ParamName::IndexLogicOp:
    case ParamName::IndexMode:
    case ParamName::IndexOffset:
    case ParamName::IndexShift:
    case ParamName::IndexWritemask:
    case ParamName::LightModelAmbient:
    case ParamName::LightModelColorControl:
    case ParamName::LightModelLocalViewer:
    case ParamName::LightModelTwoSide:
    case ParamName::Light0:
    case ParamName::Light1:
    case ParamName::Light2:
    case ParamName::Light3:
    case ParamName::Light4:
    case ParamName::Light5:
    case ParamName::Light6:
    case ParamName::Light7:
    case ParamName::Lighting:
    case ParamName::LineStipple:
    case ParamName::LineStipplePattern:
    case ParamName::LineStippleRepeat:
    case ParamName::ListBase:
    case ParamName::ListIndex:
    case ParamName::ListMode:
    case ParamName::Map1Color4:
    case ParamName::Map1GridDomain:
    case ParamName::Map1GridSegments:
    case ParamName::Map1Index:
    case ParamName::Map1Normal:
    case ParamName::Map1TextureCoord1:
    case ParamName::Map1TextureCoord2:
    case ParamName::Map1TextureCoord3:
    case ParamName::Map1TextureCoord4:
    case ParamName::Map1Vertex3:
    case ParamName::Map1Vertex4:
    case ParamName::Map2Color4:
    case ParamName::Map2GridDomain:
    case ParamName::Map2GridSegments:
    case ParamName::Map2Index:
    case ParamName::Map2Normal:
    case ParamName::Map2TextureCoord1:
    case ParamName::Map2TextureCoord2:
    case ParamName::Map2TextureCoord3:
    case ParamName::Map2TextureCoord4:
    case ParamName::Map2Vertex3:
    case ParamName::Map2Vertex4:
    case ParamName::MapColor:
    case ParamName::MapStencil:
    case ParamName::MatrixMode:
    case ParamName::MaxAttribStackDepth:
    case ParamName::MaxClientAttribStackDepth:
    case ParamName::MaxColorMatrixStackDepth:
    case ParamName::MaxEvalOrder:
    case ParamName::MaxLights:
    case ParamName::MaxListNesting:
    case ParamName::MaxModelviewStackDepth:
    case ParamName::MaxNameStackDepth:
    case ParamName::MaxPixelMapTable:
    case ParamName::MaxProjectionStackDepth:
    case ParamName::MaxTextureCoords:
    case ParamName::MaxTextureStackDepth:
    case ParamName::MaxTextureUnits:
    case ParamName::Minmax:
    case ParamName::ModelviewMatrix:
    case ParamName::ModelviewStackDepth:
    case ParamName::NameStackDepth:
    case ParamName::NormalArray:
    case ParamName::NormalArrayBufferBinding:
    case ParamName::NormalArrayStride:
    case ParamName::NormalArrayType:
    case ParamName::Normalize:
    case ParamName::PerspectiveCorrectionHint:
    case ParamName::PixelMapAToASize:
    case ParamName::PixelMapBToBSize:
    case ParamName::PixelMapGToGSize:
    case ParamName::PixelMapIToASize:
    case ParamName::PixelMapIToBSize:
    case ParamName::PixelMapIToGSize:
    case ParamName::PixelMapIToISize:
    case ParamName::PixelMapIToRSize:
    case ParamName::PixelMapRToRSize:
    case ParamName::PixelMapSToSSize:
    case ParamName::PointDistanceAttenuation:
    case ParamName::PointSizeMax:
    case ParamName::PointSizeMin:
    case ParamName::PointSmooth:
    case ParamName::PointSmoothHint:
    case ParamName::PointSprite:
    case ParamName::PolygonMode:
    case ParamName::PolygonStipple:
    case ParamName::PostColorMatrixAlphaBias:
    case ParamName::PostColorMatrixAlphaScale:
    case ParamName::PostColorMatrixBlueBias:
    case ParamName::PostColorMatrixBlueScale:
    case ParamName::PostColorMatrixColorTable:
    case ParamName::PostColorMatrixGreenBias:
    case ParamName::PostColorMatrixGreenScale:
    case ParamName::PostColorMatrixRedBias:
    case ParamName::PostColorMatrixRedScale:
    case ParamName::PostConvolutionAlphaBias:
    case ParamName::PostConvolutionAlphaScale:
    case ParamName::PostConvolutionBlueBias:
    case ParamName::PostConvolutionBlueScale:
    case ParamName::PostConvolutionColorTable:
    case ParamName::PostConvolutionGreenBias:
    case ParamName::PostConvolutionGreenScale:
    case ParamName::PostConvolutionRedBias:
    case ParamName::PostConvolutionRedScale:
    case ParamName::ProjectionMatrix:
    case ParamName::ProjectionStackDepth:
    case ParamName::RedBias:
    case ParamName::RedBits:
    case ParamName::RedScale:
    case ParamName::RenderMode:
    case ParamName::RescaleNormal:
    case ParamName::RgbaMode:
    case ParamName::SecondaryColorArray:
    case ParamName::SecondaryColorArrayBufferBinding:
    case ParamName::SecondaryColorArraySize:
    case ParamName::SecondaryColorArrayStride:
    case ParamName::SecondaryColorArrayType:
    case ParamName::SelectionBufferSize:
    case ParamName::Separable2d:
    case ParamName::ShadeModel:
    case ParamName::StencilBits:
    case ParamName::Texture1d:
    case ParamName::Texture2d:
    case ParamName::Texture3d:
    case ParamName::TextureCoordArray:
    case ParamName::TextureCoordArrayBufferBinding:
    case ParamName::TextureCoordArraySize:
    case ParamName::TextureCoordArrayStride:
    case ParamName::TextureCoordArrayType:
    case ParamName::TextureCubeMap:
    case ParamName::TextureGenQ:
    case ParamName::TextureGenR:
    case ParamName::TextureGenS:
    case ParamName::TextureGenT:
    case ParamName::TextureMatrix:
    case ParamName::TextureStackDepth:
    case ParamName::TransposeColorMatrix:
    case ParamName::TransposeModelviewMatrix:
    case ParamName::TransposeProjectionMatrix:
    case ParamName::TransposeTextureMatrix:
    case ParamName::VertexArray:
    case ParamName::VertexArrayBufferBinding:
    case ParamName::VertexArraySize:
    case ParamName::VertexArrayStride:
    case ParamName::VertexArrayType:
    case ParamName::VertexProgramTwoSide:
    case ParamName::ZoomX:
    case ParamName::ZoomY:
        // more info: https://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts
        if (cd.GetVersion() < ContextVersion::Version_3_0) {
            break;
        }
        if (cd.IsForwardCompatible() || cd.GetVersion() >= ContextVersion::Version_3_1)
            return false;
    case ParamName::ActiveTexture:
    case ParamName::AliasedLineWidthRange:
    case ParamName::ArrayBufferBinding:
    case ParamName::Blend:
    case ParamName::BlendColor:
    case ParamName::BlendDstAlpha:
    case ParamName::BlendDstRgb:
    case ParamName::BlendEquationAlpha:
    case ParamName::BlendEquationRgb:
    case ParamName::BlendSrcAlpha:
    case ParamName::BlendSrcRgb:
    case ParamName::ColorClearValue:
    case ParamName::ColorLogicOp:
    case ParamName::ColorWritemask:
    case ParamName::CompressedTextureFormats:
    case ParamName::ContextFlags:
    case ParamName::CullFace:
    case ParamName::CurrentProgram:
    case ParamName::DebugGroupStackDepth:
    case ParamName::DepthClearValue:
    case ParamName::DepthFunc:
    case ParamName::DepthRange:
    case ParamName::DepthTest:
    case ParamName::DepthWritemask:
    case ParamName::DispatchIndirectBufferBinding:
    case ParamName::Dither:
    case ParamName::Doublebuffer:
    case ParamName::DrawBuffer0:
    case ParamName::DrawBuffer1:
    case ParamName::DrawBuffer2:
    case ParamName::DrawBuffer3:
    case ParamName::DrawBuffer4:
    case ParamName::DrawBuffer5:
    case ParamName::DrawBuffer6:
    case ParamName::DrawBuffer7:
    case ParamName::DrawBuffer8:
    case ParamName::DrawBuffer9:
    case ParamName::DrawBuffer10:
    case ParamName::DrawBuffer11:
    case ParamName::DrawBuffer12:
    case ParamName::DrawBuffer13:
    case ParamName::DrawBuffer14:
    case ParamName::DrawBuffer15:
    case ParamName::DrawBuffer:
    case ParamName::DrawFramebufferBinding:
    case ParamName::ElementArrayBufferBinding:
    case ParamName::FragmentShaderDerivativeHint:
    case ParamName::ImplementationColorReadFormat:
    case ParamName::ImplementationColorReadType:
    case ParamName::LayerProvokingVertex:
    case ParamName::LineSmooth:
    case ParamName::LineSmoothHint:
    case ParamName::LineWidth:
    case ParamName::LineWidthGranularity:
    case ParamName::LineWidthRange:
    case ParamName::LogicOpMode:
    case ParamName::MajorVersion:
    case ParamName::Max3dTextureSize:
    case ParamName::MaxArrayTextureLayers:
    case ParamName::MaxClipDistances:
    case ParamName::MaxColorTextureSamples:
    case ParamName::MaxCombinedAtomicCounters:
    case ParamName::MaxCombinedComputeUniformComponents:
    case ParamName::MaxCombinedFragmentUniformComponents:
    case ParamName::MaxCombinedGeometryUniformComponents:
    case ParamName::MaxCombinedShaderStorageBlocks:
    case ParamName::MaxCombinedTextureImageUnits:
    case ParamName::MaxCombinedUniformBlocks:
    case ParamName::MaxCombinedVertexUniformComponents:
    case ParamName::MaxComputeAtomicCounterBuffers:
    case ParamName::MaxComputeAtomicCounters:
    case ParamName::MaxComputeShaderStorageBlocks:
    case ParamName::MaxComputeTextureImageUnits:
    case ParamName::MaxComputeUniformBlocks:
    case ParamName::MaxComputeUniformComponents:
    case ParamName::MaxComputeWorkGroupCount:
    case ParamName::MaxComputeWorkGroupInvocations:
    case ParamName::MaxComputeWorkGroupSize:
    case ParamName::MaxCubeMapTextureSize:
    case ParamName::MaxDebugGroupStackDepth:
    case ParamName::MaxDepthTextureSamples:
    case ParamName::MaxDrawBuffers:
    case ParamName::MaxDualSourceDrawBuffers:
    case ParamName::MaxElementIndex:
    case ParamName::MaxElementsIndices:
    case ParamName::MaxElementsVertices:
    case ParamName::MaxFragmentAtomicCounters:
    case ParamName::MaxFragmentInputComponents:
    case ParamName::MaxFragmentShaderStorageBlocks:
    case ParamName::MaxFragmentUniformBlocks:
    case ParamName::MaxFragmentUniformComponents:
    case ParamName::MaxFragmentUniformVectors:
    case ParamName::MaxFramebufferHeight:
    case ParamName::MaxFramebufferLayers:
    case ParamName::MaxFramebufferSamples:
    case ParamName::MaxFramebufferWidth:
    case ParamName::MaxGeometryAtomicCounters:
    case ParamName::MaxGeometryInputComponents:
    case ParamName::MaxGeometryOutputComponents:
    case ParamName::MaxGeometryShaderStorageBlocks:
    case ParamName::MaxGeometryTextureImageUnits:
    case ParamName::MaxGeometryUniformBlocks:
    case ParamName::MaxGeometryUniformComponents:
    case ParamName::MaxIntegerSamples:
    case ParamName::MaxLabelLength:
    case ParamName::MaxProgramTexelOffset:
    case ParamName::MaxRectangleTextureSize:
    case ParamName::MaxRenderbufferSize:
    case ParamName::MaxSampleMaskWords:
    case ParamName::MaxServerWaitTimeout:
    case ParamName::MaxShaderStorageBufferBindings:
    case ParamName::MaxTessControlAtomicCounters:
    case ParamName::MaxTessControlShaderStorageBlocks:
    case ParamName::MaxTessEvaluationAtomicCounters:
    case ParamName::MaxTessEvaluationShaderStorageBlocks:
    case ParamName::MaxTextureBufferSize:
    case ParamName::MaxTextureImageUnits:
    case ParamName::MaxTextureLodBias:
    case ParamName::MaxTextureSize:
    case ParamName::MaxUniformBlockSize:
    case ParamName::MaxUniformBufferBindings:
    case ParamName::MaxUniformLocations:
    case ParamName::MaxVaryingComponents:
    case ParamName::MaxVaryingVectors:
    case ParamName::MaxVertexAtomicCounters:
    case ParamName::MaxVertexAttribBindings:
    case ParamName::MaxVertexAttribRelativeOffset:
    case ParamName::MaxVertexAttribs:
    case ParamName::MaxVertexOutputComponents:
    case ParamName::MaxVertexShaderStorageBlocks:
    case ParamName::MaxVertexTextureImageUnits:
    case ParamName::MaxVertexUniformBlocks:
    case ParamName::MaxVertexUniformComponents:
    case ParamName::MaxVertexUniformVectors:
    case ParamName::MaxViewportDims:
    case ParamName::MaxViewports:
    case ParamName::MinMapBufferAlignment:
    case ParamName::MinorVersion:
    case ParamName::MinProgramTexelOffset:
    case ParamName::NumCompressedTextureFormats:
    case ParamName::NumExtensions:
    case ParamName::NumProgramBinaryFormats:
    case ParamName::NumShaderBinaryFormats:
    case ParamName::PackAlignment:
    case ParamName::PackImageHeight:
    case ParamName::PackLsbFirst:
    case ParamName::PackRowLength:
    case ParamName::PackSkipImages:
    case ParamName::PackSkipPixels:
    case ParamName::PackSkipRows:
    case ParamName::PackSwapBytes:
    case ParamName::PixelPackBufferBinding:
    case ParamName::PixelUnpackBufferBinding:
    case ParamName::PointFadeThresholdSize:
    case ParamName::PointSize:
    case ParamName::PointSizeGranularity:
    case ParamName::PointSizeRange:
    case ParamName::PolygonOffsetFactor:
    case ParamName::PolygonOffsetFill:
    case ParamName::PolygonOffsetLine:
    case ParamName::PolygonOffsetPoint:
    case ParamName::PolygonOffsetUnits:
    case ParamName::PolygonSmooth:
    case ParamName::PolygonSmoothHint:
    case ParamName::PrimitiveRestartIndex:
    case ParamName::ProgramBinaryFormats:
    case ParamName::ProgramPipelineBinding:
    case ParamName::ProgramPointSize:
    case ParamName::ProvokingVertex:
    case ParamName::ReadBuffer:
    case ParamName::ReadFramebufferBinding:
    case ParamName::RenderbufferBinding:
    case ParamName::SampleBuffers:
    case ParamName::SampleCoverageInvert:
    case ParamName::SampleCoverageValue:
    case ParamName::SamplerBinding:
    case ParamName::Samples:
    case ParamName::ScissorBox:
    case ParamName::ScissorTest:
    case ParamName::ShaderCompiler:
    case ParamName::ShaderStorageBufferBinding:
    case ParamName::ShaderStorageBufferOffsetAlignment:
    case ParamName::ShaderStorageBufferSize:
    case ParamName::ShaderStorageBufferStart:
    case ParamName::StencilBackFail:
    case ParamName::StencilBackFunc:
    case ParamName::StencilBackPassDepthFail:
    case ParamName::StencilBackPassDepthPass:
    case ParamName::StencilBackRef:
    case ParamName::StencilBackValueMask:
    case ParamName::StencilBackWritemask:
    case ParamName::StencilClearValue:
    case ParamName::StencilFail:
    case ParamName::StencilFunc:
    case ParamName::StencilPassDepthFail:
    case ParamName::StencilPassDepthPass:
    case ParamName::StencilRef:
    case ParamName::StencilTest:
    case ParamName::StencilValueMask:
    case ParamName::StencilWritemask:
    case ParamName::Stereo:
    case ParamName::SubpixelBits:
    case ParamName::TextureBinding1d:
    case ParamName::TextureBinding1dArray:
    case ParamName::TextureBinding2d:
    case ParamName::TextureBinding2dArray:
    case ParamName::TextureBinding2dMultisample:
    case ParamName::TextureBinding2dMultisampleArray:
    case ParamName::TextureBinding3d:
    case ParamName::TextureBindingBuffer:
    case ParamName::TextureBindingCubeMap:
    case ParamName::TextureBindingRectangle:
    case ParamName::TextureBufferOffsetAlignment:
    case ParamName::TextureCompressionHint:
    case ParamName::Timestamp:
    case ParamName::TransformFeedbackBufferBinding:
    case ParamName::TransformFeedbackBufferSize:
    case ParamName::TransformFeedbackBufferStart:
    case ParamName::UniformBufferBinding:
    case ParamName::UniformBufferOffsetAlignment:
    case ParamName::UniformBufferSize:
    case ParamName::UniformBufferStart:
    case ParamName::UnpackAlignment:
    case ParamName::UnpackImageHeight:
    case ParamName::UnpackLsbFirst:
    case ParamName::UnpackRowLength:
    case ParamName::UnpackSkipImages:
    case ParamName::UnpackSkipPixels:
    case ParamName::UnpackSkipRows:
    case ParamName::UnpackSwapBytes:
    case ParamName::VertexArrayBinding:
    case ParamName::VertexBindingDivisor:
    case ParamName::VertexBindingOffset:
    case ParamName::VertexBindingStride:
    case ParamName::Viewport:
    case ParamName::ViewportBoundsRange:
    case ParamName::ViewportIndexProvokingVertex:
    case ParamName::ViewportSubpixelBits:
        break;
    default:
        return false;
    }

    param_name = tmp_param_name;
    return true;
}

std::ostream &operator<<(std::ostream &os, ParamName param_name) {
    switch (param_name) {
    case ParamName::AccumAlphaBits:
        os << "ParamName::AccumAlphaBits";
        break;
    case ParamName::AccumBlueBits:
        os << "ParamName::AccumBlueBits";
        break;
    case ParamName::AccumClearValue:
        os << "ParamName::AccumClearValue";
        break;
    case ParamName::AccumGreenBits:
        os << "ParamName::AccumGreenBits";
        break;
    case ParamName::AccumRedBits:
        os << "ParamName::AccumRedBits";
        break;
    case ParamName::AliasedPointSizeRange:
        os << "ParamName::AliasedPointSizeRange";
        break;
    case ParamName::AlphaBias:
        os << "ParamName::AlphaBias";
        break;
    case ParamName::AlphaBits:
        os << "ParamName::AlphaBits";
        break;
    case ParamName::AlphaScale:
        os << "ParamName::AlphaScale";
        break;
    case ParamName::AlphaTest:
        os << "ParamName::AlphaTest";
        break;
    case ParamName::AlphaTestFunc:
        os << "ParamName::AlphaTestFunc";
        break;
    case ParamName::AlphaTestRef:
        os << "ParamName::AlphaTestRef";
        break;
    case ParamName::AttribStackDepth:
        os << "ParamName::AttribStackDepth";
        break;
    case ParamName::AutoNormal:
        os << "ParamName::AutoNormal";
        break;
    case ParamName::AuxBuffers:
        os << "ParamName::AuxBuffers";
        break;
    case ParamName::BlueBias:
        os << "ParamName::BlueBias";
        break;
    case ParamName::BlueBits:
        os << "ParamName::BlueBits";
        break;
    case ParamName::BlueScale:
        os << "ParamName::BlueScale";
        break;
    case ParamName::ClientActiveTexture:
        os << "ParamName::ClientActiveTexture";
        break;
    case ParamName::ClientAttribStackDepth:
        os << "ParamName::ClientAttribStackDepth";
        break;
    case ParamName::ClipPlane0:
        os << "ParamName::ClipPlane0";
        break;
    case ParamName::ClipPlane1:
        os << "ParamName::ClipPlane1";
        break;
    case ParamName::ClipPlane2:
        os << "ParamName::ClipPlane2";
        break;
    case ParamName::ClipPlane3:
        os << "ParamName::ClipPlane3";
        break;
    case ParamName::ClipPlane4:
        os << "ParamName::ClipPlane4";
        break;
    case ParamName::ClipPlane5:
        os << "ParamName::ClipPlane5";
        break;
    case ParamName::ColorArray:
        os << "ParamName::ColorArray";
        break;
    case ParamName::ColorArrayBufferBinding:
        os << "ParamName::ColorArrayBufferBinding";
        break;
    case ParamName::ColorArraySize:
        os << "ParamName::ColorArraySize";
        break;
    case ParamName::ColorArrayStride:
        os << "ParamName::ColorArrayStride";
        break;
    case ParamName::ColorArrayType:
        os << "ParamName::ColorArrayType";
        break;
    case ParamName::ColorMaterial:
        os << "ParamName::ColorMaterial";
        break;
    case ParamName::ColorMaterialFace:
        os << "ParamName::ColorMaterialFace";
        break;
    case ParamName::ColorMaterialParameter:
        os << "ParamName::ColorMaterialParameter";
        break;
    case ParamName::ColorMatrix:
        os << "ParamName::ColorMatrix";
        break;
    case ParamName::ColorMatrixStackDepth:
        os << "ParamName::ColorMatrixStackDepth";
        break;
    case ParamName::ColorSum:
        os << "ParamName::ColorSum";
        break;
    case ParamName::ColorTable:
        os << "ParamName::ColorTable";
        break;
    case ParamName::Convolution1d:
        os << "ParamName::Convolution1d";
        break;
    case ParamName::Convolution2d:
        os << "ParamName::Convolution2d";
        break;
    case ParamName::CullFaceMode:
        os << "ParamName::CullFaceMode";
        break;
    case ParamName::CurrentColor:
        os << "ParamName::CurrentColor";
        break;
    case ParamName::CurrentFogCoord:
        os << "ParamName::CurrentFogCoord";
        break;
    case ParamName::CurrentIndex:
        os << "ParamName::CurrentIndex";
        break;
    case ParamName::CurrentNormal:
        os << "ParamName::CurrentNormal";
        break;
    case ParamName::CurrentRasterColor:
        os << "ParamName::CurrentRasterColor";
        break;
    case ParamName::CurrentRasterDistance:
        os << "ParamName::CurrentRasterDistance";
        break;
    case ParamName::CurrentRasterIndex:
        os << "ParamName::CurrentRasterIndex";
        break;
    case ParamName::CurrentRasterPosition:
        os << "ParamName::CurrentRasterPosition";
        break;
    case ParamName::CurrentRasterPositionValid:
        os << "ParamName::CurrentRasterPositionValid";
        break;
    case ParamName::CurrentRasterSecondaryColor:
        os << "ParamName::CurrentRasterSecondaryColor";
        break;
    case ParamName::CurrentRasterTextureCoords:
        os << "ParamName::CurrentRasterTextureCoords";
        break;
    case ParamName::CurrentSecondaryColor:
        os << "ParamName::CurrentSecondaryColor";
        break;
    case ParamName::CurrentTextureCoords:
        os << "ParamName::CurrentTextureCoords";
        break;
    case ParamName::DepthBias:
        os << "ParamName::DepthBias";
        break;
    case ParamName::DepthBits:
        os << "ParamName::DepthBits";
        break;
    case ParamName::DepthScale:
        os << "ParamName::DepthScale";
        break;
    case ParamName::EdgeFlag:
        os << "ParamName::EdgeFlag";
        break;
    case ParamName::EdgeFlagArray:
        os << "ParamName::EdgeFlagArray";
        break;
    case ParamName::EdgeFlagArrayBufferBinding:
        os << "ParamName::EdgeFlagArrayBufferBinding";
        break;
    case ParamName::EdgeFlagArrayStride:
        os << "ParamName::EdgeFlagArrayStride";
        break;
    case ParamName::FeedbackBufferSize:
        os << "ParamName::FeedbackBufferSize";
        break;
    case ParamName::FeedbackBufferType:
        os << "ParamName::FeedbackBufferType";
        break;
    case ParamName::Fog:
        os << "ParamName::Fog";
        break;
    case ParamName::FogColor:
        os << "ParamName::FogColor";
        break;
    case ParamName::FogCoordArray:
        os << "ParamName::FogCoordArray";
        break;
    case ParamName::FogCoordArrayBufferBinding:
        os << "ParamName::FogCoordArrayBufferBinding";
        break;
    case ParamName::FogCoordArrayStride:
        os << "ParamName::FogCoordArrayStride";
        break;
    case ParamName::FogCoordArrayType:
        os << "ParamName::FogCoordArrayType";
        break;
    case ParamName::FogCoordSrc:
        os << "ParamName::FogCoordSrc";
        break;
    case ParamName::FogDensity:
        os << "ParamName::FogDensity";
        break;
    case ParamName::FogEnd:
        os << "ParamName::FogEnd";
        break;
    case ParamName::FogHint:
        os << "ParamName::FogHint";
        break;
    case ParamName::FogIndex:
        os << "ParamName::FogIndex";
        break;
    case ParamName::FogMode:
        os << "ParamName::FogMode";
        break;
    case ParamName::FogStart:
        os << "ParamName::FogStart";
        break;
    case ParamName::FrontFace:
        os << "ParamName::FrontFace";
        break;
    case ParamName::GenerateMipmapHint:
        os << "ParamName::GenerateMipmapHint";
        break;
    case ParamName::GreenBias:
        os << "ParamName::GreenBias";
        break;
    case ParamName::GreenBits:
        os << "ParamName::GreenBits";
        break;
    case ParamName::GreenScale:
        os << "ParamName::GreenScale";
        break;
    case ParamName::Histogram:
        os << "ParamName::Histogram";
        break;
    case ParamName::IndexArray:
        os << "ParamName::IndexArray";
        break;
    case ParamName::IndexArrayBufferBinding:
        os << "ParamName::IndexArrayBufferBinding";
        break;
    case ParamName::IndexArrayStride:
        os << "ParamName::IndexArrayStride";
        break;
    case ParamName::IndexArrayType:
        os << "ParamName::IndexArrayType";
        break;
    case ParamName::IndexBits:
        os << "ParamName::IndexBits";
        break;
    case ParamName::IndexClearValue:
        os << "ParamName::IndexClearValue";
        break;
    case ParamName::IndexLogicOp:
        os << "ParamName::IndexLogicOp";
        break;
    case ParamName::IndexMode:
        os << "ParamName::IndexMode";
        break;
    case ParamName::IndexOffset:
        os << "ParamName::IndexOffset";
        break;
    case ParamName::IndexShift:
        os << "ParamName::IndexShift";
        break;
    case ParamName::IndexWritemask:
        os << "ParamName::IndexWritemask";
        break;
    case ParamName::LightModelAmbient:
        os << "ParamName::LightModelAmbient";
        break;
    case ParamName::LightModelColorControl:
        os << "ParamName::LightModelColorControl";
        break;
    case ParamName::LightModelLocalViewer:
        os << "ParamName::LightModelLocalViewer";
        break;
    case ParamName::LightModelTwoSide:
        os << "ParamName::LightModelTwoSide";
        break;
    case ParamName::Light0:
        os << "ParamName::Light0";
        break;
    case ParamName::Light1:
        os << "ParamName::Light1";
        break;
    case ParamName::Light2:
        os << "ParamName::Light2";
        break;
    case ParamName::Light3:
        os << "ParamName::Light3";
        break;
    case ParamName::Light4:
        os << "ParamName::Light4";
        break;
    case ParamName::Light5:
        os << "ParamName::Light5";
        break;
    case ParamName::Light6:
        os << "ParamName::Light6";
        break;
    case ParamName::Light7:
        os << "ParamName::Light7";
        break;
    case ParamName::Lighting:
        os << "ParamName::Lighting";
        break;
    case ParamName::LineStipple:
        os << "ParamName::LineStipple";
        break;
    case ParamName::LineStipplePattern:
        os << "ParamName::LineStipplePattern";
        break;
    case ParamName::LineStippleRepeat:
        os << "ParamName::LineStippleRepeat";
        break;
    case ParamName::ListBase:
        os << "ParamName::ListBase";
        break;
    case ParamName::ListIndex:
        os << "ParamName::ListIndex";
        break;
    case ParamName::ListMode:
        os << "ParamName::ListMode";
        break;
    case ParamName::Map1Color4:
        os << "ParamName::Map1Color4";
        break;
    case ParamName::Map1GridDomain:
        os << "ParamName::Map1GridDomain";
        break;
    case ParamName::Map1GridSegments:
        os << "ParamName::Map1GridSegments";
        break;
    case ParamName::Map1Index:
        os << "ParamName::Map1Index";
        break;
    case ParamName::Map1Normal:
        os << "ParamName::Map1Normal";
        break;
    case ParamName::Map1TextureCoord1:
        os << "ParamName::Map1TextureCoord1";
        break;
    case ParamName::Map1TextureCoord2:
        os << "ParamName::Map1TextureCoord2";
        break;
    case ParamName::Map1TextureCoord3:
        os << "ParamName::Map1TextureCoord3";
        break;
    case ParamName::Map1TextureCoord4:
        os << "ParamName::Map1TextureCoord4";
        break;
    case ParamName::Map1Vertex3:
        os << "ParamName::Map1Vertex3";
        break;
    case ParamName::Map1Vertex4:
        os << "ParamName::Map1Vertex4";
        break;
    case ParamName::Map2Color4:
        os << "ParamName::Map2Color4";
        break;
    case ParamName::Map2GridDomain:
        os << "ParamName::Map2GridDomain";
        break;
    case ParamName::Map2GridSegments:
        os << "ParamName::Map2GridSegments";
        break;
    case ParamName::Map2Index:
        os << "ParamName::Map2Index";
        break;
    case ParamName::Map2Normal:
        os << "ParamName::Map2Normal";
        break;
    case ParamName::Map2TextureCoord1:
        os << "ParamName::Map2TextureCoord1";
        break;
    case ParamName::Map2TextureCoord2:
        os << "ParamName::Map2TextureCoord2";
        break;
    case ParamName::Map2TextureCoord3:
        os << "ParamName::Map2TextureCoord3";
        break;
    case ParamName::Map2TextureCoord4:
        os << "ParamName::Map2TextureCoord4";
        break;
    case ParamName::Map2Vertex3:
        os << "ParamName::Map2Vertex3";
        break;
    case ParamName::Map2Vertex4:
        os << "ParamName::Map2Vertex4";
        break;
    case ParamName::MapColor:
        os << "ParamName::MapColor";
        break;
    case ParamName::MapStencil:
        os << "ParamName::MapStencil";
        break;
    case ParamName::MatrixMode:
        os << "ParamName::MatrixMode";
        break;
    case ParamName::MaxAttribStackDepth:
        os << "ParamName::MaxAttribStackDepth";
        break;
    case ParamName::MaxClientAttribStackDepth:
        os << "ParamName::MaxClientAttribStackDepth";
        break;
    case ParamName::MaxColorMatrixStackDepth:
        os << "ParamName::MaxColorMatrixStackDepth";
        break;
    case ParamName::MaxEvalOrder:
        os << "ParamName::MaxEvalOrder";
        break;
    case ParamName::MaxLights:
        os << "ParamName::MaxLights";
        break;
    case ParamName::MaxListNesting:
        os << "ParamName::MaxListNesting";
        break;
    case ParamName::MaxModelviewStackDepth:
        os << "ParamName::MaxModelviewStackDepth";
        break;
    case ParamName::MaxNameStackDepth:
        os << "ParamName::MaxNameStackDepth";
        break;
    case ParamName::MaxPixelMapTable:
        os << "ParamName::MaxPixelMapTable";
        break;
    case ParamName::MaxProjectionStackDepth:
        os << "ParamName::MaxProjectionStackDepth";
        break;
    case ParamName::MaxTextureCoords:
        os << "ParamName::MaxTextureCoords";
        break;
    case ParamName::MaxTextureStackDepth:
        os << "ParamName::MaxTextureStackDepth";
        break;
    case ParamName::MaxTextureUnits:
        os << "ParamName::MaxTextureUnits";
        break;
    case ParamName::Minmax:
        os << "ParamName::Minmax";
        break;
    case ParamName::ModelviewMatrix:
        os << "ParamName::ModelviewMatrix";
        break;
    case ParamName::ModelviewStackDepth:
        os << "ParamName::ModelviewStackDepth";
        break;
    case ParamName::NameStackDepth:
        os << "ParamName::NameStackDepth";
        break;
    case ParamName::NormalArray:
        os << "ParamName::NormalArray";
        break;
    case ParamName::NormalArrayBufferBinding:
        os << "ParamName::NormalArrayBufferBinding";
        break;
    case ParamName::NormalArrayStride:
        os << "ParamName::NormalArrayStride";
        break;
    case ParamName::NormalArrayType:
        os << "ParamName::NormalArrayType";
        break;
    case ParamName::Normalize:
        os << "ParamName::Normalize";
        break;
    case ParamName::PerspectiveCorrectionHint:
        os << "ParamName::PerspectiveCorrectionHint";
        break;
    case ParamName::PixelMapAToASize:
        os << "ParamName::PixelMapAToASize";
        break;
    case ParamName::PixelMapBToBSize:
        os << "ParamName::PixelMapBToBSize";
        break;
    case ParamName::PixelMapGToGSize:
        os << "ParamName::PixelMapGToGSize";
        break;
    case ParamName::PixelMapIToASize:
        os << "ParamName::PixelMapIToASize";
        break;
    case ParamName::PixelMapIToBSize:
        os << "ParamName::PixelMapIToBSize";
        break;
    case ParamName::PixelMapIToGSize:
        os << "ParamName::PixelMapIToGSize";
        break;
    case ParamName::PixelMapIToISize:
        os << "ParamName::PixelMapIToISize";
        break;
    case ParamName::PixelMapIToRSize:
        os << "ParamName::PixelMapIToRSize";
        break;
    case ParamName::PixelMapRToRSize:
        os << "ParamName::PixelMapRToRSize";
        break;
    case ParamName::PixelMapSToSSize:
        os << "ParamName::PixelMapSToSSize";
        break;
    case ParamName::PointDistanceAttenuation:
        os << "ParamName::PointDistanceAttenuation";
        break;
    case ParamName::PointSizeMax:
        os << "ParamName::PointSizeMax";
        break;
    case ParamName::PointSizeMin:
        os << "ParamName::PointSizeMin";
        break;
    case ParamName::PointSmooth:
        os << "ParamName::PointSmooth";
        break;
    case ParamName::PointSmoothHint:
        os << "ParamName::PointSmoothHint";
        break;
    case ParamName::PointSprite:
        os << "ParamName::PointSprite";
        break;
    case ParamName::PolygonMode:
        os << "ParamName::PolygonMode";
        break;
    case ParamName::PolygonStipple:
        os << "ParamName::PolygonStipple";
        break;
    case ParamName::PostColorMatrixAlphaBias:
        os << "ParamName::PostColorMatrixAlphaBias";
        break;
    case ParamName::PostColorMatrixAlphaScale:
        os << "ParamName::PostColorMatrixAlphaScale";
        break;
    case ParamName::PostColorMatrixBlueBias:
        os << "ParamName::PostColorMatrixBlueBias";
        break;
    case ParamName::PostColorMatrixBlueScale:
        os << "ParamName::PostColorMatrixBlueScale";
        break;
    case ParamName::PostColorMatrixColorTable:
        os << "ParamName::PostColorMatrixColorTable";
        break;
    case ParamName::PostColorMatrixGreenBias:
        os << "ParamName::PostColorMatrixGreenBias";
        break;
    case ParamName::PostColorMatrixGreenScale:
        os << "ParamName::PostColorMatrixGreenScale";
        break;
    case ParamName::PostColorMatrixRedBias:
        os << "ParamName::PostColorMatrixRedBias";
        break;
    case ParamName::PostColorMatrixRedScale:
        os << "ParamName::PostColorMatrixRedScale";
        break;
    case ParamName::PostConvolutionAlphaBias:
        os << "ParamName::PostConvolutionAlphaBias";
        break;
    case ParamName::PostConvolutionAlphaScale:
        os << "ParamName::PostConvolutionAlphaScale";
        break;
    case ParamName::PostConvolutionBlueBias:
        os << "ParamName::PostConvolutionBlueBias";
        break;
    case ParamName::PostConvolutionBlueScale:
        os << "ParamName::PostConvolutionBlueScale";
        break;
    case ParamName::PostConvolutionColorTable:
        os << "ParamName::PostConvolutionColorTable";
        break;
    case ParamName::PostConvolutionGreenBias:
        os << "ParamName::PostConvolutionGreenBias";
        break;
    case ParamName::PostConvolutionGreenScale:
        os << "ParamName::PostConvolutionGreenScale";
        break;
    case ParamName::PostConvolutionRedBias:
        os << "ParamName::PostConvolutionRedBias";
        break;
    case ParamName::PostConvolutionRedScale:
        os << "ParamName::PostConvolutionRedScale";
        break;
    case ParamName::ProjectionMatrix:
        os << "ParamName::ProjectionMatrix";
        break;
    case ParamName::ProjectionStackDepth:
        os << "ParamName::ProjectionStackDepth";
        break;
    case ParamName::RedBias:
        os << "ParamName::RedBias";
        break;
    case ParamName::RedBits:
        os << "ParamName::RedBits";
        break;
    case ParamName::RedScale:
        os << "ParamName::RedScale";
        break;
    case ParamName::RenderMode:
        os << "ParamName::RenderMode";
        break;
    case ParamName::RescaleNormal:
        os << "ParamName::RescaleNormal";
        break;
    case ParamName::RgbaMode:
        os << "ParamName::RgbaMode";
        break;
    case ParamName::SecondaryColorArray:
        os << "ParamName::SecondaryColorArray";
        break;
    case ParamName::SecondaryColorArrayBufferBinding:
        os << "ParamName::SecondaryColorArrayBufferBinding";
        break;
    case ParamName::SecondaryColorArraySize:
        os << "ParamName::SecondaryColorArraySize";
        break;
    case ParamName::SecondaryColorArrayStride:
        os << "ParamName::SecondaryColorArrayStride";
        break;
    case ParamName::SecondaryColorArrayType:
        os << "ParamName::SecondaryColorArrayType";
        break;
    case ParamName::SelectionBufferSize:
        os << "ParamName::SelectionBufferSize";
        break;
    case ParamName::Separable2d:
        os << "ParamName::Separable2d";
        break;
    case ParamName::ShadeModel:
        os << "ParamName::ShadeModel";
        break;
    case ParamName::StencilBits:
        os << "ParamName::StencilBits";
        break;
    case ParamName::Texture1d:
        os << "ParamName::Texture1d";
        break;
    case ParamName::Texture2d:
        os << "ParamName::Texture2d";
        break;
    case ParamName::Texture3d:
        os << "ParamName::Texture3d";
        break;
    case ParamName::TextureCoordArray:
        os << "ParamName::TextureCoordArray";
        break;
    case ParamName::TextureCoordArrayBufferBinding:
        os << "ParamName::TextureCoordArrayBufferBinding";
        break;
    case ParamName::TextureCoordArraySize:
        os << "ParamName::TextureCoordArraySize";
        break;
    case ParamName::TextureCoordArrayStride:
        os << "ParamName::TextureCoordArrayStride";
        break;
    case ParamName::TextureCoordArrayType:
        os << "ParamName::TextureCoordArrayType";
        break;
    case ParamName::TextureCubeMap:
        os << "ParamName::TextureCubeMap";
        break;
    case ParamName::TextureGenQ:
        os << "ParamName::TextureGenQ";
        break;
    case ParamName::TextureGenR:
        os << "ParamName::TextureGenR";
        break;
    case ParamName::TextureGenS:
        os << "ParamName::TextureGenS";
        break;
    case ParamName::TextureGenT:
        os << "ParamName::TextureGenT";
        break;
    case ParamName::TextureMatrix:
        os << "ParamName::TextureMatrix";
        break;
    case ParamName::TextureStackDepth:
        os << "ParamName::TextureStackDepth";
        break;
    case ParamName::TransposeColorMatrix:
        os << "ParamName::TransposeColorMatrix";
        break;
    case ParamName::TransposeModelviewMatrix:
        os << "ParamName::TransposeModelviewMatrix";
        break;
    case ParamName::TransposeProjectionMatrix:
        os << "ParamName::TransposeProjectionMatrix";
        break;
    case ParamName::TransposeTextureMatrix:
        os << "ParamName::TransposeTextureMatrix";
        break;
    case ParamName::VertexArray:
        os << "ParamName::VertexArray";
        break;
    case ParamName::VertexArrayBufferBinding:
        os << "ParamName::VertexArrayBufferBinding";
        break;
    case ParamName::VertexArraySize:
        os << "ParamName::VertexArraySize";
        break;
    case ParamName::VertexArrayStride:
        os << "ParamName::VertexArrayStride";
        break;
    case ParamName::VertexArrayType:
        os << "ParamName::VertexArrayType";
        break;
    case ParamName::VertexProgramTwoSide:
        os << "ParamName::VertexProgramTwoSide";
        break;
    case ParamName::ZoomX:
        os << "ParamName::ZoomX";
        break;
    case ParamName::ZoomY:
        os << "ParamName::ZoomY";
        break;
    case ParamName::ActiveTexture:
        os << "ParamName::ActiveTexture";
        break;
    case ParamName::AliasedLineWidthRange:
        os << "ParamName::AliasedLineWidthRange";
        break;
    case ParamName::ArrayBufferBinding:
        os << "ParamName::ArrayBufferBinding";
        break;
    case ParamName::Blend:
        os << "ParamName::Blend";
        break;
    case ParamName::BlendColor:
        os << "ParamName::BlendColor";
        break;
    case ParamName::BlendDstAlpha:
        os << "ParamName::BlendDstAlpha";
        break;
    case ParamName::BlendDstRgb:
        os << "ParamName::BlendDstRgb";
        break;
    case ParamName::BlendEquationAlpha:
        os << "ParamName::BlendEquationAlpha";
        break;
    case ParamName::BlendEquationRgb:
        os << "ParamName::BlendEquationRgb";
        break;
    case ParamName::BlendSrcAlpha:
        os << "ParamName::BlendSrcAlpha";
        break;
    case ParamName::BlendSrcRgb:
        os << "ParamName::BlendSrcRgb";
        break;
    case ParamName::ColorClearValue:
        os << "ParamName::ColorClearValue";
        break;
    case ParamName::ColorLogicOp:
        os << "ParamName::ColorLogicOp";
        break;
    case ParamName::ColorWritemask:
        os << "ParamName::ColorWritemask";
        break;
    case ParamName::CompressedTextureFormats:
        os << "ParamName::CompressedTextureFormats";
        break;
    case ParamName::ContextFlags:
        os << "ParamName::ContextFlags";
        break;
    case ParamName::CullFace:
        os << "ParamName::CullFace";
        break;
    case ParamName::CurrentProgram:
        os << "ParamName::CurrentProgram";
        break;
    case ParamName::DebugGroupStackDepth:
        os << "ParamName::DebugGroupStackDepth";
        break;
    case ParamName::DepthClearValue:
        os << "ParamName::DepthClearValue";
        break;
    case ParamName::DepthFunc:
        os << "ParamName::DepthFunc";
        break;
    case ParamName::DepthRange:
        os << "ParamName::DepthRange";
        break;
    case ParamName::DepthTest:
        os << "ParamName::DepthTest";
        break;
    case ParamName::DepthWritemask:
        os << "ParamName::DepthWritemask";
        break;
    case ParamName::DispatchIndirectBufferBinding:
        os << "ParamName::DispatchIndirectBufferBinding";
        break;
    case ParamName::Dither:
        os << "ParamName::Dither";
        break;
    case ParamName::Doublebuffer:
        os << "ParamName::Doublebuffer";
        break;
    case ParamName::DrawBuffer0:
        os << "ParamName::DrawBuffer0";
        break;
    case ParamName::DrawBuffer1:
        os << "ParamName::DrawBuffer1";
        break;
    case ParamName::DrawBuffer2:
        os << "ParamName::DrawBuffer2";
        break;
    case ParamName::DrawBuffer3:
        os << "ParamName::DrawBuffer3";
        break;
    case ParamName::DrawBuffer4:
        os << "ParamName::DrawBuffer4";
        break;
    case ParamName::DrawBuffer5:
        os << "ParamName::DrawBuffer5";
        break;
    case ParamName::DrawBuffer6:
        os << "ParamName::DrawBuffer6";
        break;
    case ParamName::DrawBuffer7:
        os << "ParamName::DrawBuffer7";
        break;
    case ParamName::DrawBuffer8:
        os << "ParamName::DrawBuffer8";
        break;
    case ParamName::DrawBuffer9:
        os << "ParamName::DrawBuffer9";
        break;
    case ParamName::DrawBuffer10:
        os << "ParamName::DrawBuffer10";
        break;
    case ParamName::DrawBuffer11:
        os << "ParamName::DrawBuffer11";
        break;
    case ParamName::DrawBuffer12:
        os << "ParamName::DrawBuffer12";
        break;
    case ParamName::DrawBuffer13:
        os << "ParamName::DrawBuffer13";
        break;
    case ParamName::DrawBuffer14:
        os << "ParamName::DrawBuffer14";
        break;
    case ParamName::DrawBuffer15:
        os << "ParamName::DrawBuffer15";
        break;
    case ParamName::DrawBuffer:
        os << "ParamName::DrawBuffer";
        break;
    case ParamName::DrawFramebufferBinding:
        os << "ParamName::DrawFramebufferBinding";
        break;
    case ParamName::ElementArrayBufferBinding:
        os << "ParamName::ElementArrayBufferBinding";
        break;
    case ParamName::FragmentShaderDerivativeHint:
        os << "ParamName::FragmentShaderDerivativeHint";
        break;
    case ParamName::ImplementationColorReadFormat:
        os << "ParamName::ImplementationColorReadFormat";
        break;
    case ParamName::ImplementationColorReadType:
        os << "ParamName::ImplementationColorReadType";
        break;
    case ParamName::LayerProvokingVertex:
        os << "ParamName::LayerProvokingVertex";
        break;
    case ParamName::LineSmooth:
        os << "ParamName::LineSmooth";
        break;
    case ParamName::LineSmoothHint:
        os << "ParamName::LineSmoothHint";
        break;
    case ParamName::LineWidth:
        os << "ParamName::LineWidth";
        break;
    case ParamName::LineWidthGranularity:
        os << "ParamName::LineWidthGranularity";
        break;
    case ParamName::LineWidthRange:
        os << "ParamName::LineWidthRange";
        break;
    case ParamName::LogicOpMode:
        os << "ParamName::LogicOpMode";
        break;
    case ParamName::MajorVersion:
        os << "ParamName::MajorVersion";
        break;
    case ParamName::Max3dTextureSize:
        os << "ParamName::Max3dTextureSize";
        break;
    case ParamName::MaxArrayTextureLayers:
        os << "ParamName::MaxArrayTextureLayers";
        break;
    case ParamName::MaxClipDistances:
        os << "ParamName::MaxClipDistances";
        break;
    case ParamName::MaxColorTextureSamples:
        os << "ParamName::MaxColorTextureSamples";
        break;
    case ParamName::MaxCombinedAtomicCounters:
        os << "ParamName::MaxCombinedAtomicCounters";
        break;
    case ParamName::MaxCombinedComputeUniformComponents:
        os << "ParamName::MaxCombinedComputeUniformComponents";
        break;
    case ParamName::MaxCombinedFragmentUniformComponents:
        os << "ParamName::MaxCombinedFragmentUniformComponents";
        break;
    case ParamName::MaxCombinedGeometryUniformComponents:
        os << "ParamName::MaxCombinedGeometryUniformComponents";
        break;
    case ParamName::MaxCombinedShaderStorageBlocks:
        os << "ParamName::MaxCombinedShaderStorageBlocks";
        break;
    case ParamName::MaxCombinedTextureImageUnits:
        os << "ParamName::MaxCombinedTextureImageUnits";
        break;
    case ParamName::MaxCombinedUniformBlocks:
        os << "ParamName::MaxCombinedUniformBlocks";
        break;
    case ParamName::MaxCombinedVertexUniformComponents:
        os << "ParamName::MaxCombinedVertexUniformComponents";
        break;
    case ParamName::MaxComputeAtomicCounterBuffers:
        os << "ParamName::MaxComputeAtomicCounterBuffers";
        break;
    case ParamName::MaxComputeAtomicCounters:
        os << "ParamName::MaxComputeAtomicCounters";
        break;
    case ParamName::MaxComputeShaderStorageBlocks:
        os << "ParamName::MaxComputeShaderStorageBlocks";
        break;
    case ParamName::MaxComputeTextureImageUnits:
        os << "ParamName::MaxComputeTextureImageUnits";
        break;
    case ParamName::MaxComputeUniformBlocks:
        os << "ParamName::MaxComputeUniformBlocks";
        break;
    case ParamName::MaxComputeUniformComponents:
        os << "ParamName::MaxComputeUniformComponents";
        break;
    case ParamName::MaxComputeWorkGroupCount:
        os << "ParamName::MaxComputeWorkGroupCount";
        break;
    case ParamName::MaxComputeWorkGroupInvocations:
        os << "ParamName::MaxComputeWorkGroupInvocations";
        break;
    case ParamName::MaxComputeWorkGroupSize:
        os << "ParamName::MaxComputeWorkGroupSize";
        break;
    case ParamName::MaxCubeMapTextureSize:
        os << "ParamName::MaxCubeMapTextureSize";
        break;
    case ParamName::MaxDebugGroupStackDepth:
        os << "ParamName::MaxDebugGroupStackDepth";
        break;
    case ParamName::MaxDepthTextureSamples:
        os << "ParamName::MaxDepthTextureSamples";
        break;
    case ParamName::MaxDrawBuffers:
        os << "ParamName::MaxDrawBuffers";
        break;
    case ParamName::MaxDualSourceDrawBuffers:
        os << "ParamName::MaxDualSourceDrawBuffers";
        break;
    case ParamName::MaxElementIndex:
        os << "ParamName::MaxElementIndex";
        break;
    case ParamName::MaxElementsIndices:
        os << "ParamName::MaxElementsIndices";
        break;
    case ParamName::MaxElementsVertices:
        os << "ParamName::MaxElementsVertices";
        break;
    case ParamName::MaxFragmentAtomicCounters:
        os << "ParamName::MaxFragmentAtomicCounters";
        break;
    case ParamName::MaxFragmentInputComponents:
        os << "ParamName::MaxFragmentInputComponents";
        break;
    case ParamName::MaxFragmentShaderStorageBlocks:
        os << "ParamName::MaxFragmentShaderStorageBlocks";
        break;
    case ParamName::MaxFragmentUniformBlocks:
        os << "ParamName::MaxFragmentUniformBlocks";
        break;
    case ParamName::MaxFragmentUniformComponents:
        os << "ParamName::MaxFragmentUniformComponents";
        break;
    case ParamName::MaxFragmentUniformVectors:
        os << "ParamName::MaxFragmentUniformVectors";
        break;
    case ParamName::MaxFramebufferHeight:
        os << "ParamName::MaxFramebufferHeight";
        break;
    case ParamName::MaxFramebufferLayers:
        os << "ParamName::MaxFramebufferLayers";
        break;
    case ParamName::MaxFramebufferSamples:
        os << "ParamName::MaxFramebufferSamples";
        break;
    case ParamName::MaxFramebufferWidth:
        os << "ParamName::MaxFramebufferWidth";
        break;
    case ParamName::MaxGeometryAtomicCounters:
        os << "ParamName::MaxGeometryAtomicCounters";
        break;
    case ParamName::MaxGeometryInputComponents:
        os << "ParamName::MaxGeometryInputComponents";
        break;
    case ParamName::MaxGeometryOutputComponents:
        os << "ParamName::MaxGeometryOutputComponents";
        break;
    case ParamName::MaxGeometryShaderStorageBlocks:
        os << "ParamName::MaxGeometryShaderStorageBlocks";
        break;
    case ParamName::MaxGeometryTextureImageUnits:
        os << "ParamName::MaxGeometryTextureImageUnits";
        break;
    case ParamName::MaxGeometryUniformBlocks:
        os << "ParamName::MaxGeometryUniformBlocks";
        break;
    case ParamName::MaxGeometryUniformComponents:
        os << "ParamName::MaxGeometryUniformComponents";
        break;
    case ParamName::MaxIntegerSamples:
        os << "ParamName::MaxIntegerSamples";
        break;
    case ParamName::MaxLabelLength:
        os << "ParamName::MaxLabelLength";
        break;
    case ParamName::MaxProgramTexelOffset:
        os << "ParamName::MaxProgramTexelOffset";
        break;
    case ParamName::MaxRectangleTextureSize:
        os << "ParamName::MaxRectangleTextureSize";
        break;
    case ParamName::MaxRenderbufferSize:
        os << "ParamName::MaxRenderbufferSize";
        break;
    case ParamName::MaxSampleMaskWords:
        os << "ParamName::MaxSampleMaskWords";
        break;
    case ParamName::MaxServerWaitTimeout:
        os << "ParamName::MaxServerWaitTimeout";
        break;
    case ParamName::MaxShaderStorageBufferBindings:
        os << "ParamName::MaxShaderStorageBufferBindings";
        break;
    case ParamName::MaxTessControlAtomicCounters:
        os << "ParamName::MaxTessControlAtomicCounters";
        break;
    case ParamName::MaxTessControlShaderStorageBlocks:
        os << "ParamName::MaxTessControlShaderStorageBlocks";
        break;
    case ParamName::MaxTessEvaluationAtomicCounters:
        os << "ParamName::MaxTessEvaluationAtomicCounters";
        break;
    case ParamName::MaxTessEvaluationShaderStorageBlocks:
        os << "ParamName::MaxTessEvaluationShaderStorageBlocks";
        break;
    case ParamName::MaxTextureBufferSize:
        os << "ParamName::MaxTextureBufferSize";
        break;
    case ParamName::MaxTextureImageUnits:
        os << "ParamName::MaxTextureImageUnits";
        break;
    case ParamName::MaxTextureLodBias:
        os << "ParamName::MaxTextureLodBias";
        break;
    case ParamName::MaxTextureSize:
        os << "ParamName::MaxTextureSize";
        break;
    case ParamName::MaxUniformBlockSize:
        os << "ParamName::MaxUniformBlockSize";
        break;
    case ParamName::MaxUniformBufferBindings:
        os << "ParamName::MaxUniformBufferBindings";
        break;
    case ParamName::MaxUniformLocations:
        os << "ParamName::MaxUniformLocations";
        break;
    case ParamName::MaxVaryingComponents:
        os << "ParamName::MaxVaryingComponents";
        break;
    case ParamName::MaxVaryingVectors:
        os << "ParamName::MaxVaryingVectors";
        break;
    case ParamName::MaxVertexAtomicCounters:
        os << "ParamName::MaxVertexAtomicCounters";
        break;
    case ParamName::MaxVertexAttribBindings:
        os << "ParamName::MaxVertexAttribBindings";
        break;
    case ParamName::MaxVertexAttribRelativeOffset:
        os << "ParamName::MaxVertexAttribRelativeOffset";
        break;
    case ParamName::MaxVertexAttribs:
        os << "ParamName::MaxVertexAttribs";
        break;
    case ParamName::MaxVertexOutputComponents:
        os << "ParamName::MaxVertexOutputComponents";
        break;
    case ParamName::MaxVertexShaderStorageBlocks:
        os << "ParamName::MaxVertexShaderStorageBlocks";
        break;
    case ParamName::MaxVertexTextureImageUnits:
        os << "ParamName::MaxVertexTextureImageUnits";
        break;
    case ParamName::MaxVertexUniformBlocks:
        os << "ParamName::MaxVertexUniformBlocks";
        break;
    case ParamName::MaxVertexUniformComponents:
        os << "ParamName::MaxVertexUniformComponents";
        break;
    case ParamName::MaxVertexUniformVectors:
        os << "ParamName::MaxVertexUniformVectors";
        break;
    case ParamName::MaxViewportDims:
        os << "ParamName::MaxViewportDims";
        break;
    case ParamName::MaxViewports:
        os << "ParamName::MaxViewports";
        break;
    case ParamName::MinMapBufferAlignment:
        os << "ParamName::MinMapBufferAlignment";
        break;
    case ParamName::MinorVersion:
        os << "ParamName::MinorVersion";
        break;
    case ParamName::MinProgramTexelOffset:
        os << "ParamName::MinProgramTexelOffset";
        break;
    case ParamName::NumCompressedTextureFormats:
        os << "ParamName::NumCompressedTextureFormats";
        break;
    case ParamName::NumExtensions:
        os << "ParamName::NumExtensions";
        break;
    case ParamName::NumProgramBinaryFormats:
        os << "ParamName::NumProgramBinaryFormats";
        break;
    case ParamName::NumShaderBinaryFormats:
        os << "ParamName::NumShaderBinaryFormats";
        break;
    case ParamName::PackAlignment:
        os << "ParamName::PackAlignment";
        break;
    case ParamName::PackImageHeight:
        os << "ParamName::PackImageHeight";
        break;
    case ParamName::PackLsbFirst:
        os << "ParamName::PackLsbFirst";
        break;
    case ParamName::PackRowLength:
        os << "ParamName::PackRowLength";
        break;
    case ParamName::PackSkipImages:
        os << "ParamName::PackSkipImages";
        break;
    case ParamName::PackSkipPixels:
        os << "ParamName::PackSkipPixels";
        break;
    case ParamName::PackSkipRows:
        os << "ParamName::PackSkipRows";
        break;
    case ParamName::PackSwapBytes:
        os << "ParamName::PackSwapBytes";
        break;
    case ParamName::PixelPackBufferBinding:
        os << "ParamName::PixelPackBufferBinding";
        break;
    case ParamName::PixelUnpackBufferBinding:
        os << "ParamName::PixelUnpackBufferBinding";
        break;
    case ParamName::PointFadeThresholdSize:
        os << "ParamName::PointFadeThresholdSize";
        break;
    case ParamName::PointSize:
        os << "ParamName::PointSize";
        break;
    case ParamName::PointSizeGranularity:
        os << "ParamName::PointSizeGranularity";
        break;
    case ParamName::PointSizeRange:
        os << "ParamName::PointSizeRange";
        break;
    case ParamName::PolygonOffsetFactor:
        os << "ParamName::PolygonOffsetFactor";
        break;
    case ParamName::PolygonOffsetFill:
        os << "ParamName::PolygonOffsetFill";
        break;
    case ParamName::PolygonOffsetLine:
        os << "ParamName::PolygonOffsetLine";
        break;
    case ParamName::PolygonOffsetPoint:
        os << "ParamName::PolygonOffsetPoint";
        break;
    case ParamName::PolygonOffsetUnits:
        os << "ParamName::PolygonOffsetUnits";
        break;
    case ParamName::PolygonSmooth:
        os << "ParamName::PolygonSmooth";
        break;
    case ParamName::PolygonSmoothHint:
        os << "ParamName::PolygonSmoothHint";
        break;
    case ParamName::PrimitiveRestartIndex:
        os << "ParamName::PrimitiveRestartIndex";
        break;
    case ParamName::ProgramBinaryFormats:
        os << "ParamName::ProgramBinaryFormats";
        break;
    case ParamName::ProgramPipelineBinding:
        os << "ParamName::ProgramPipelineBinding";
        break;
    case ParamName::ProgramPointSize:
        os << "ParamName::ProgramPointSize";
        break;
    case ParamName::ProvokingVertex:
        os << "ParamName::ProvokingVertex";
        break;
    case ParamName::ReadBuffer:
        os << "ParamName::ReadBuffer";
        break;
    case ParamName::ReadFramebufferBinding:
        os << "ParamName::ReadFramebufferBinding";
        break;
    case ParamName::RenderbufferBinding:
        os << "ParamName::RenderbufferBinding";
        break;
    case ParamName::SampleBuffers:
        os << "ParamName::SampleBuffers";
        break;
    case ParamName::SampleCoverageInvert:
        os << "ParamName::SampleCoverageInvert";
        break;
    case ParamName::SampleCoverageValue:
        os << "ParamName::SampleCoverageValue";
        break;
    case ParamName::SamplerBinding:
        os << "ParamName::SamplerBinding";
        break;
    case ParamName::Samples:
        os << "ParamName::Samples";
        break;
    case ParamName::ScissorBox:
        os << "ParamName::ScissorBox";
        break;
    case ParamName::ScissorTest:
        os << "ParamName::ScissorTest";
        break;
    case ParamName::ShaderCompiler:
        os << "ParamName::ShaderCompiler";
        break;
    case ParamName::ShaderStorageBufferBinding:
        os << "ParamName::ShaderStorageBufferBinding";
        break;
    case ParamName::ShaderStorageBufferOffsetAlignment:
        os << "ParamName::ShaderStorageBufferOffsetAlignment";
        break;
    case ParamName::ShaderStorageBufferSize:
        os << "ParamName::ShaderStorageBufferSize";
        break;
    case ParamName::ShaderStorageBufferStart:
        os << "ParamName::ShaderStorageBufferStart";
        break;
    case ParamName::StencilBackFail:
        os << "ParamName::StencilBackFail";
        break;
    case ParamName::StencilBackFunc:
        os << "ParamName::StencilBackFunc";
        break;
    case ParamName::StencilBackPassDepthFail:
        os << "ParamName::StencilBackPassDepthFail";
        break;
    case ParamName::StencilBackPassDepthPass:
        os << "ParamName::StencilBackPassDepthPass";
        break;
    case ParamName::StencilBackRef:
        os << "ParamName::StencilBackRef";
        break;
    case ParamName::StencilBackValueMask:
        os << "ParamName::StencilBackValueMask";
        break;
    case ParamName::StencilBackWritemask:
        os << "ParamName::StencilBackWritemask";
        break;
    case ParamName::StencilClearValue:
        os << "ParamName::StencilClearValue";
        break;
    case ParamName::StencilFail:
        os << "ParamName::StencilFail";
        break;
    case ParamName::StencilFunc:
        os << "ParamName::StencilFunc";
        break;
    case ParamName::StencilPassDepthFail:
        os << "ParamName::StencilPassDepthFail";
        break;
    case ParamName::StencilPassDepthPass:
        os << "ParamName::StencilPassDepthPass";
        break;
    case ParamName::StencilRef:
        os << "ParamName::StencilRef";
        break;
    case ParamName::StencilTest:
        os << "ParamName::StencilTest";
        break;
    case ParamName::StencilValueMask:
        os << "ParamName::StencilValueMask";
        break;
    case ParamName::StencilWritemask:
        os << "ParamName::StencilWritemask";
        break;
    case ParamName::Stereo:
        os << "ParamName::Stereo";
        break;
    case ParamName::SubpixelBits:
        os << "ParamName::SubpixelBits";
        break;
    case ParamName::TextureBinding1d:
        os << "ParamName::TextureBinding1d";
        break;
    case ParamName::TextureBinding1dArray:
        os << "ParamName::TextureBinding1dArray";
        break;
    case ParamName::TextureBinding2d:
        os << "ParamName::TextureBinding2d";
        break;
    case ParamName::TextureBinding2dArray:
        os << "ParamName::TextureBinding2dArray";
        break;
    case ParamName::TextureBinding2dMultisample:
        os << "ParamName::TextureBinding2dMultisample";
        break;
    case ParamName::TextureBinding2dMultisampleArray:
        os << "ParamName::TextureBinding2dMultisampleArray";
        break;
    case ParamName::TextureBinding3d:
        os << "ParamName::TextureBinding3d";
        break;
    case ParamName::TextureBindingBuffer:
        os << "ParamName::TextureBindingBuffer";
        break;
    case ParamName::TextureBindingCubeMap:
        os << "ParamName::TextureBindingCubeMap";
        break;
    case ParamName::TextureBindingRectangle:
        os << "ParamName::TextureBindingRectangle";
        break;
    case ParamName::TextureBufferOffsetAlignment:
        os << "ParamName::TextureBufferOffsetAlignment";
        break;
    case ParamName::TextureCompressionHint:
        os << "ParamName::TextureCompressionHint";
        break;
    case ParamName::Timestamp:
        os << "ParamName::Timestamp";
        break;
    case ParamName::TransformFeedbackBufferBinding:
        os << "ParamName::TransformFeedbackBufferBinding";
        break;
    case ParamName::TransformFeedbackBufferSize:
        os << "ParamName::TransformFeedbackBufferSize";
        break;
    case ParamName::TransformFeedbackBufferStart:
        os << "ParamName::TransformFeedbackBufferStart";
        break;
    case ParamName::UniformBufferBinding:
        os << "ParamName::UniformBufferBinding";
        break;
    case ParamName::UniformBufferOffsetAlignment:
        os << "ParamName::UniformBufferOffsetAlignment";
        break;
    case ParamName::UniformBufferSize:
        os << "ParamName::UniformBufferSize";
        break;
    case ParamName::UniformBufferStart:
        os << "ParamName::UniformBufferStart";
        break;
    case ParamName::UnpackAlignment:
        os << "ParamName::UnpackAlignment";
        break;
    case ParamName::UnpackImageHeight:
        os << "ParamName::UnpackImageHeight";
        break;
    case ParamName::UnpackLsbFirst:
        os << "ParamName::UnpackLsbFirst";
        break;
    case ParamName::UnpackRowLength:
        os << "ParamName::UnpackRowLength";
        break;
    case ParamName::UnpackSkipImages:
        os << "ParamName::UnpackSkipImages";
        break;
    case ParamName::UnpackSkipPixels:
        os << "ParamName::UnpackSkipPixels";
        break;
    case ParamName::UnpackSkipRows:
        os << "ParamName::UnpackSkipRows";
        break;
    case ParamName::UnpackSwapBytes:
        os << "ParamName::UnpackSwapBytes";
        break;
    case ParamName::VertexArrayBinding:
        os << "ParamName::VertexArrayBinding";
        break;
    case ParamName::VertexBindingDivisor:
        os << "ParamName::VertexBindingDivisor";
        break;
    case ParamName::VertexBindingOffset:
        os << "ParamName::VertexBindingOffset";
        break;
    case ParamName::VertexBindingStride:
        os << "ParamName::VertexBindingStride";
        break;
    case ParamName::Viewport:
        os << "ParamName::Viewport";
        break;
    case ParamName::ViewportBoundsRange:
        os << "ParamName::ViewportBoundsRange";
        break;
    case ParamName::ViewportIndexProvokingVertex:
        os << "ParamName::ViewportIndexProvokingVertex";
        break;
    case ParamName::ViewportSubpixelBits:
        os << "ParamName::ViewportSubpixelBits";
        break;
    }

    return os;
}

}
}
