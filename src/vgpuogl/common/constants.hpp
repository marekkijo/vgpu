#ifndef VGPUCONSTANTS_H
#define VGPUCONSTANTS_H

#include <array>
#include <map>
#include <algorithm>
#include "common/contextversion.hpp"

namespace vgpu {

struct Constants {
    // Context
    static constexpr int max_contexts_num_{128};

    static constexpr std::array<ContextVersion, 18>supported_context_versions_{
        ContextVersion::Version_1_0,
        ContextVersion::Version_1_1,
        ContextVersion::Version_1_2,
        ContextVersion::Version_1_3,
        ContextVersion::Version_1_4,
        ContextVersion::Version_1_5,
        ContextVersion::Version_2_0,
        ContextVersion::Version_2_1,
        ContextVersion::Version_3_0,
        ContextVersion::Version_3_1,
        ContextVersion::Version_3_2,
        ContextVersion::Version_3_3,
        ContextVersion::Version_4_0,
        ContextVersion::Version_4_1,
        ContextVersion::Version_4_2,
        ContextVersion::Version_4_3,
        ContextVersion::Version_4_4,
        ContextVersion::Version_4_5
    };
    static bool IsSupportedVersion(ContextVersion cv) {
        return std::find(supported_context_versions_.begin(), supported_context_versions_.end(), cv) != supported_context_versions_.end();
    }

    static constexpr char *gl_vendor_ = "Marek Kijo";
    static constexpr char *gl_renderer_ = "VGPU";
    static constexpr char *gl_extensions_ = "GL_ARB_none";
    static const std::map<ContextVersion, const char *> gl_version_;
    static const std::map<ContextVersion, const char *> gl_shading_language_version_;

    static constexpr std::array<int, 2> max_viewport_dims_{8192, 8192};

    // Extensions
    // TODO: Add WGL_ARB_extensions_string
    // TODO: Add WGL_ARB_pixel_format
    // TODO: Add WGL_ARB_create_context WGL_ARB_create_context_profile

private:
};

}

#endif // !VGPUCONSTANTS_H
