#ifndef VGPULOGGER_H
#define VGPULOGGER_H

#include "logger/logger_config.hpp"

#if _DEBUG
# include "logger/logger_impl.hpp"

namespace vgpu {
extern LoggerImpl g_log;
}
#endif // _DEBUG

#if _DEBUG && VGPU_LOG_GENERAL_IS_ON
# define VGPU_LOG_FUNCTION(...) vgpu::g_log.FunctionLog("", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG(...) vgpu::g_log.Log("", __VA_ARGS__)
# define VGPU_LOG_IF(cond, ...) vgpu::g_log.LogIf(cond, "", __VA_ARGS__)
#else
# define VGPU_LOG_FUNCTION(...)
# define VGPU_LOG(...)
# define VGPU_LOG_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_ICD_IS_ON
# define VGPU_LOG_ICD_FUNCTION(...) vgpu::g_log.FunctionLog("ICD", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_ICD(...) vgpu::g_log.Log("ICD", "// ", __VA_ARGS__)
# define VGPU_LOG_ICD_IF(cond, ...) vgpu::g_log.LogIf(cond, "ICD", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_ICD_FUNCTION(...)
# define VGPU_LOG_ICD(...)
# define VGPU_LOG_ICD_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_WGL_IS_ON
# define VGPU_LOG_WGL_FUNCTION(...) vgpu::g_log.FunctionLog("WGL", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_WGL(...) vgpu::g_log.Log("WGL", "// ", __VA_ARGS__)
# define VGPU_LOG_WGL_IF(cond, ...) vgpu::g_log.LogIf(cond, "WGL", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_WGL_FUNCTION(...)
# define VGPU_LOG_WGL(...)
# define VGPU_LOG_WGL_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_GL_IS_ON
# define VGPU_LOG_GL_FUNCTION(...) vgpu::g_log.FunctionLog("GL", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_GL(...) vgpu::g_log.Log("GL", "// ", __VA_ARGS__)
# define VGPU_LOG_GL_IF(cond, ...) vgpu::g_log.LogIf(cond, "GL", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_GL_FUNCTION(...)
# define VGPU_LOG_GL(...)
# define VGPU_LOG_GL_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_DRV_IS_ON
# define VGPU_LOG_DRV_FUNCTION(...) vgpu::g_log.FunctionLog("DRV", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_DRV(...) vgpu::g_log.Log("DRV", "// ", __VA_ARGS__)
# define VGPU_LOG_DRV_IF(cond, ...) vgpu::g_log.LogIf(cond, "DRV", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_DRV_FUNCTION(...)
# define VGPU_LOG_DRV(...)
# define VGPU_LOG_DRV_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_CTX_IS_ON
# define VGPU_LOG_CTX_FUNCTION(...) vgpu::g_log.FunctionLog("CTX", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_CTX(...) vgpu::g_log.Log("CTX", "// ", __VA_ARGS__)
# define VGPU_LOG_CTX_IF(cond, ...) vgpu::g_log.LogIf(cond, "CTX", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_CTX_FUNCTION(...)
# define VGPU_LOG_CTX(...)
# define VGPU_LOG_CTX_IF(cond, ...)
#endif

#if _DEBUG && VGPU_LOG_VGPU_IS_ON
# define VGPU_LOG_VGPU_FUNCTION(...) vgpu::g_log.FunctionLog("VGPU", __FUNCSIG__, __VA_ARGS__)
# define VGPU_LOG_VGPU(...) vgpu::g_log.Log("VGPU", "// ", __VA_ARGS__)
# define VGPU_LOG_VGPU_IF(cond, ...) vgpu::g_log.LogIf(cond, "VGPU", "// ", __VA_ARGS__)
#else
# define VGPU_LOG_VGPU_FUNCTION(...)
# define VGPU_LOG_VGPU(...)
# define VGPU_LOG_VGPU_IF(cond, ...)
#endif

#endif // !VGPULOGGER_H
