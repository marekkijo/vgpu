#ifndef VGPUSHADER_H
#define VGPUSHADER_H

#include "shadertype.hpp"

namespace vgpu {

class Shader {
public:
    virtual ShaderType GetShaderType() const = 0;
};

}

#endif // !VGPUSHADER_H
