#include "vgpuicd.hpp"
#include "win32driver.hpp"
#include "common/logger.hpp"
#include "common/assert.hpp"

vgpu::Win32Driver g_driver;

BOOL APIENTRY DrvCopyContext(DHGLRC dhrcSource, DHGLRC dhrcDest, UINT fuMask) {
    VGPU_LOG_ICD_FUNCTION(dhrcSource, dhrcDest, fuMask);
    VGPU_NOT_IMPLEMENTED();
    return 0;
}

DHGLRC APIENTRY DrvCreateContext(HDC hdc) {
    VGPU_LOG_ICD_FUNCTION(hdc);
    return g_driver.icd_CreateContext(hdc);
}

DHGLRC APIENTRY DrvCreateLayerContext(HDC hdc, INT iLayerPlane) {
    VGPU_LOG_ICD_FUNCTION(hdc, iLayerPlane);
    return g_driver.icd_CreateContext(hdc, iLayerPlane);
}

BOOL APIENTRY DrvDeleteContext(DHGLRC dhglrc) {
    VGPU_LOG_ICD_FUNCTION(dhglrc);
    return g_driver.icd_DeleteContext(dhglrc);
}

BOOL APIENTRY DrvDescribeLayerPlane(HDC hdc, INT iPixelFormat, INT iLayerPlane, UINT nBytes, LPLAYERPLANEDESCRIPTOR plpd) {
    VGPU_LOG_ICD_FUNCTION(hdc, iPixelFormat, iLayerPlane, nBytes, plpd);
    VGPU_ERROR("Used obsolete DrvDescribeLayerPlane function");
    return FALSE;
}

LONG APIENTRY DrvDescribePixelFormat(HDC hdc, LONG iPixelFormat, ULONG cjpfd, PIXELFORMATDESCRIPTOR *ppfd) {
    VGPU_LOG_ICD_FUNCTION(hdc, iPixelFormat, cjpfd, ppfd);
    return g_driver.icd_DescribePixelFormat(hdc, iPixelFormat, cjpfd, ppfd);
}

int APIENTRY DrvGetLayerPaletteEntries(HDC hdc, INT iLayerPlane, INT iStart, INT cEntries, COLORREF *pcr) {
    VGPU_LOG_ICD_FUNCTION(hdc, iLayerPlane, iStart, cEntries, pcr);
    VGPU_ERROR("Used obsolete DrvGetLayerPaletteEntries function");
    return 0;
}

PROC APIENTRY DrvGetProcAddress(LPCSTR lpszProc) {
    VGPU_LOG_ICD_FUNCTION(lpszProc);
    return g_driver.icd_GetProcAddress(lpszProc);
}

BOOL APIENTRY DrvPresentBuffers(HDC hdc, LPPRESENTBUFFERS pprsbData) {
    VGPU_LOG_ICD_FUNCTION(hdc, pprsbData);
    VGPU_NOT_IMPLEMENTED();
    return 0;
}

int APIENTRY DrvRealizeLayerPalette(HDC hdc, INT iLayerPlane, BOOL bRealize) {
    VGPU_LOG_ICD_FUNCTION(hdc, iLayerPlane, bRealize);
    VGPU_ERROR("Used obsolete DrvRealizeLayerPalette function");
    return 0;
}

BOOL APIENTRY DrvReleaseContext(DHGLRC dhglrc) {
    VGPU_LOG_ICD_FUNCTION(dhglrc);
    return g_driver.icd_ReleaseContext(dhglrc);
}

void APIENTRY DrvSetCallbackProcs(INT nProcs, PROC *pProcs) {
    VGPU_LOG_ICD_FUNCTION(nProcs, pProcs);
    g_driver.icd_SetCallbackProcs(nProcs, pProcs);
}

PGLCLTPROCTABLE APIENTRY DrvSetContext(HDC hdc, DHGLRC dhglrc, PFN_SETPROCTABLE pfnSetProcTable) {
    VGPU_LOG_ICD_FUNCTION(hdc, dhglrc, pfnSetProcTable);
    return g_driver.icd_SetContext(hdc, dhglrc, pfnSetProcTable);
}

int APIENTRY DrvSetLayerPaletteEntries(HDC hdc, INT iLayerPlane, INT iStart, INT cEntries, CONST COLORREF *pcr) {
    VGPU_LOG_ICD_FUNCTION(hdc, iLayerPlane, iStart, cEntries, pcr);
    VGPU_ERROR("Used obsolete DrvSetLayerPaletteEntries function");
    return 0;
}

BOOL APIENTRY DrvSetPixelFormat(HDC hdc, LONG iPixelFormat) {
    VGPU_LOG_ICD_FUNCTION(hdc, iPixelFormat);
    return g_driver.icd_SetPixelFormat(hdc, iPixelFormat);
}

BOOL APIENTRY DrvShareLists(DHGLRC dhglrc1, DHGLRC dhglrc2) {
    VGPU_LOG_ICD_FUNCTION(dhglrc1, dhglrc2);
    VGPU_NOT_IMPLEMENTED();
    return 0;
}

BOOL APIENTRY DrvSwapBuffers(HDC hdc) {
    VGPU_LOG_ICD_FUNCTION(hdc);
    return g_driver.icd_SwapBuffers(hdc);
}

BOOL APIENTRY DrvSwapLayerBuffers(HDC hdc, UINT fuPlanes) {
    VGPU_LOG_ICD_FUNCTION(hdc, fuPlanes);
    VGPU_NOT_IMPLEMENTED();
    return 0;
}

BOOL APIENTRY DrvValidateVersion(ULONG ulVersion) {
    VGPU_LOG_ICD_FUNCTION(ulVersion);
    return g_driver.icd_ValidateVersion(ulVersion);
}

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved) {
    VGPU_LOG_ICD_FUNCTION(hinstDLL, fdwReason, lpReserved);
    switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
        g_driver.icd_ThreadAttach();
        break;
    case DLL_PROCESS_DETACH:
        g_driver.icd_ThreadDetach();
        break;
    case DLL_THREAD_ATTACH:
        g_driver.icd_ThreadAttach();
        break;
    case DLL_THREAD_DETACH:
        g_driver.icd_ThreadDetach();
        break;
    }
    
    return TRUE;
}
