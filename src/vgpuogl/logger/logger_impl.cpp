#include "logger_impl.hpp"
#include <iostream>
#include <fstream>
#include <ctime>
#include <array>
#include <regex>
#include <algorithm>

// Windows.h included for CreateDirectory
// TODO: move platform specific stuff to other location
#include <Windows.h>

#include "common/assert.hpp"

using namespace std::chrono;

namespace vgpu {

#if _DEBUG
LoggerImpl g_log;
#endif // _DEBUG

LoggerImpl::LoggerImpl() {
#if VGPU_LOG_TO_FILE
    CreateLogDirectory();
    std::ofstream *file = new std::ofstream(GetLogFileFullPath());
    VGPU_ASSERT(file && file->is_open(), "Log file creation");
    log_ = std::unique_ptr<std::ostream, void(*)(std::ostream *)>{file, LogDeleter};
#else
    log_ = std::unique_ptr<std::ostream, void(*)(std::ostream *)>{&std::cout, LogDeleter};
#endif

    timer_ = high_resolution_clock::now();
}

void LoggerImpl::LogDeleter(std::ostream *log) {
#if VGPU_LOG_TO_FILE
    delete log;
#endif
}

#if VGPU_LOG_TO_FILE
std::string LoggerImpl::GetLogFileFullPath() {
    std::string log_file_name = "vgpu_";

    std::time_t tt = system_clock::to_time_t(system_clock::now());
    std::array<char, 26> ctime_string{};
    ctime_s(ctime_string.data(), ctime_string.size(), &tt);
    log_file_name += ctime_string.data();
    std::replace(log_file_name.begin(), log_file_name.end(), ' ', '_');
    std::replace(log_file_name.begin(), log_file_name.end(), ':', '-');
    std::replace(log_file_name.begin(), log_file_name.end(), '\n', '.');
    log_file_name += "log";

    return VGPU_LOG_FILE_LOCATION + log_file_name;
}

void LoggerImpl::CreateLogDirectory() {
    BOOL result = CreateDirectory(VGPU_LOG_FILE_LOCATION_W, NULL);
    VGPU_ASSERT(result != ERROR_PATH_NOT_FOUND, "Log directory creation");
}
#endif

void LoggerImpl::ParseFunctionSyntax(const std::string &syntax, std::string *name, std::vector<std::string> *args) {
    // find function name (optionally with '::'), and function arguments
    // TODO: implement proper parsing of function pointer arguments
    std::regex reg{R"regex(([A-Za-z0-9:_<>]+)\(([^\)]*)\))regex"};
    std::smatch res;
    std::regex_search(syntax, res, reg);

    VGPU_ASSERT(res.size() == 3, "Parsing log function syntax");

    *name = res[1];

    std::string args_string = res[2];
    auto args_string_it = args_string.begin();
    auto args_string_prev = args_string.begin();
    while (args_string_it != args_string.end()) {
        if (*args_string_it == ',') {
            args->emplace_back(args_string_prev, args_string_it);
            args_string_it++;
            args_string_prev = args_string_it;
        } else {
            args_string_it++;
        }
    }
    if (args_string_prev != args_string_it) {
        args->emplace_back(args_string_prev, args_string_it);
    }
}

}
