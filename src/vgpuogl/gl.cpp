#include "vgpuicd.hpp"
#include "win32driver.hpp"
#include "win32context.hpp"
#include "common/logger.hpp"
#include "common/assert.hpp"

extern vgpu::Win32Driver g_driver;

VGPUOGLAPI void GLAPIENTRY glAccum(GLenum op, GLfloat value) {
    VGPU_LOG_GL_FUNCTION(op, value);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glAlphaFunc(GLenum func, GLclampf ref) {
    VGPU_LOG_GL_FUNCTION(func, ref);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLboolean GLAPIENTRY glAreTexturesResident(GLsizei n, const GLuint *textures, GLboolean *residences) {
    VGPU_LOG_GL_FUNCTION(n, textures, residences);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLboolean>(0);
}

VGPUOGLAPI void GLAPIENTRY glArrayElement(GLint i) {
    VGPU_LOG_GL_FUNCTION(i);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBegin(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBindTexture(GLenum target, GLuint texture) {
    VGPU_LOG_GL_FUNCTION(target, texture);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBitmap(GLsizei width, GLsizei height, GLfloat xorig, GLfloat yorig, GLfloat xmove, GLfloat ymove, const GLubyte *bitmap) {
    VGPU_LOG_GL_FUNCTION(width, height, xorig, yorig, xmove, ymove, bitmap);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBlendFunc(GLenum sfactor, GLenum dfactor) {
    VGPU_LOG_GL_FUNCTION(sfactor, dfactor);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCallList(GLuint list) {
    VGPU_LOG_GL_FUNCTION(list);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCallLists(GLsizei n, GLenum type, const GLvoid *lists) {
    VGPU_LOG_GL_FUNCTION(n, type, lists);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClear(GLbitfield mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClearAccum(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClearDepth(GLclampd depth) {
    VGPU_LOG_GL_FUNCTION(depth);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClearIndex(GLfloat c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClearStencil(GLint s) {
    VGPU_LOG_GL_FUNCTION(s);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glClipPlane(GLenum plane, const GLdouble *equation) {
    VGPU_LOG_GL_FUNCTION(plane, equation);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3b(GLbyte red, GLbyte green, GLbyte blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3bv(const GLbyte *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3d(GLdouble red, GLdouble green, GLdouble blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3f(GLfloat red, GLfloat green, GLfloat blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3i(GLint red, GLint green, GLint blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3s(GLshort red, GLshort green, GLshort blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3ub(GLubyte red, GLubyte green, GLubyte blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3ubv(const GLubyte *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3ui(GLuint red, GLuint green, GLuint blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3uiv(const GLuint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3us(GLushort red, GLushort green, GLushort blue) {
    VGPU_LOG_GL_FUNCTION(red, green, blue);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor3usv(const GLushort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4b(GLbyte red, GLbyte green, GLbyte blue, GLbyte alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4bv(const GLbyte *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4d(GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4i(GLint red, GLint green, GLint blue, GLint alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4s(GLshort red, GLshort green, GLshort blue, GLshort alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4ub(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4ubv(const GLubyte *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4ui(GLuint red, GLuint green, GLuint blue, GLuint alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4uiv(const GLuint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4us(GLushort red, GLushort green, GLushort blue, GLushort alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColor4usv(const GLushort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) {
    VGPU_LOG_GL_FUNCTION(red, green, blue, alpha);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColorMaterial(GLenum face, GLenum mode) {
    VGPU_LOG_GL_FUNCTION(face, mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glColorPointer(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(size, type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCopyPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum type) {
    VGPU_LOG_GL_FUNCTION(x, y, width, height, type);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCopyTexImage1D(GLenum target, GLint level, GLenum internalFormat, GLint x, GLint y, GLsizei width, GLint border) {
    VGPU_LOG_GL_FUNCTION(target, level, internalFormat, x, y, width, border);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCopyTexImage2D(GLenum target, GLint level, GLenum internalFormat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border) {
    VGPU_LOG_GL_FUNCTION(target, level, internalFormat, x, y, width, height, border);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCopyTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) {
    VGPU_LOG_GL_FUNCTION(target, level, xoffset, x, y, width);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
    VGPU_LOG_GL_FUNCTION(target, level, xoffset, yoffset, x, y, width, height);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCullFace(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDeleteLists(GLuint list, GLsizei range) {
    VGPU_LOG_GL_FUNCTION(list, range);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDeleteTextures(GLsizei n, const GLuint *textures) {
    VGPU_LOG_GL_FUNCTION(n, textures);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDepthFunc(GLenum func) {
    VGPU_LOG_GL_FUNCTION(func);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDepthMask(GLboolean flag) {
    VGPU_LOG_GL_FUNCTION(flag);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDepthRange(GLclampd zNear, GLclampd zFar) {
    VGPU_LOG_GL_FUNCTION(zNear, zFar);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDisable(GLenum cap) {
    VGPU_LOG_GL_FUNCTION(cap);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDisableClientState(GLenum array) {
    VGPU_LOG_GL_FUNCTION(array);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDrawArrays(GLenum mode, GLint first, GLsizei count) {
    VGPU_LOG_GL_FUNCTION(mode, first, count);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDrawBuffer(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices) {
    VGPU_LOG_GL_FUNCTION(mode, count, type, indices);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDrawPixels(GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(width, height, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEdgeFlag(GLboolean flag) {
    VGPU_LOG_GL_FUNCTION(flag);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEdgeFlagPointer(GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEdgeFlagv(const GLboolean *flag) {
    VGPU_LOG_GL_FUNCTION(flag);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEnable(GLenum cap) {
    VGPU_LOG_GL_FUNCTION(cap);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEnableClientState(GLenum array) {
    VGPU_LOG_GL_FUNCTION(array);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEnd() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEndList() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord1d(GLdouble u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord1dv(const GLdouble *u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord1f(GLfloat u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord1fv(const GLfloat *u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord2d(GLdouble u, GLdouble v) {
    VGPU_LOG_GL_FUNCTION(u, v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord2dv(const GLdouble *u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord2f(GLfloat u, GLfloat v) {
    VGPU_LOG_GL_FUNCTION(u, v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalCoord2fv(const GLfloat *u) {
    VGPU_LOG_GL_FUNCTION(u);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalMesh1(GLenum mode, GLint i1, GLint i2) {
    VGPU_LOG_GL_FUNCTION(mode, i1, i2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalMesh2(GLenum mode, GLint i1, GLint i2, GLint j1, GLint j2) {
    VGPU_LOG_GL_FUNCTION(mode, i1, i2, j1, j2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalPoint1(GLint i) {
    VGPU_LOG_GL_FUNCTION(i);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEvalPoint2(GLint i, GLint j) {
    VGPU_LOG_GL_FUNCTION(i, j);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFeedbackBuffer(GLsizei size, GLenum type, GLfloat *buffer) {
    VGPU_LOG_GL_FUNCTION(size, type, buffer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFinish() {
    VGPU_LOG_GL_FUNCTION();

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    context->gl_Finish();
}

VGPUOGLAPI void GLAPIENTRY glFlush() {
    VGPU_LOG_GL_FUNCTION();

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    context->gl_Flush();
}

VGPUOGLAPI void GLAPIENTRY glFogf(GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFogfv(GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFogi(GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFogiv(GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFrontFace(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar) {
    VGPU_LOG_GL_FUNCTION(left, right, bottom, top, zNear, zFar);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLuint GLAPIENTRY glGenLists(GLsizei range) {
    VGPU_LOG_GL_FUNCTION(range);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLuint>(0);
}

VGPUOGLAPI void GLAPIENTRY glGenTextures(GLsizei n, GLuint *textures) {
    VGPU_LOG_GL_FUNCTION(n, textures);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetBooleanv(GLenum pname, GLboolean *data) {
    VGPU_LOG_GL_FUNCTION(pname, data);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    vgpu::gl::ParamName param_name;
    if (!vgpu::gl::ToParamName(pname, param_name, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return;
    }

    context->gl_GetBooleanv(param_name, data);
}

VGPUOGLAPI void GLAPIENTRY glGetClipPlane(GLenum plane, GLdouble *equation) {
    VGPU_LOG_GL_FUNCTION(plane, equation);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetDoublev(GLenum pname, GLdouble *data) {
    VGPU_LOG_GL_FUNCTION(pname, data);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    vgpu::gl::ParamName param_name;
    if (!vgpu::gl::ToParamName(pname, param_name, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return;
    }

    context->gl_GetDoublev(param_name, data);
}

VGPUOGLAPI GLenum GLAPIENTRY glGetError() {
    VGPU_LOG_GL_FUNCTION();

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return 0;
    }

    return context->gl_GetError();
}

VGPUOGLAPI void GLAPIENTRY glGetFloatv(GLenum pname, GLfloat *data) {
    VGPU_LOG_GL_FUNCTION(pname, data);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    vgpu::gl::ParamName param_name;
    if (!vgpu::gl::ToParamName(pname, param_name, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return;
    }

    context->gl_GetFloatv(param_name, data);
}

VGPUOGLAPI void GLAPIENTRY glGetIntegerv(GLenum pname, GLint *data) {
    VGPU_LOG_GL_FUNCTION(pname, data);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    vgpu::gl::ParamName param_name;
    if (!vgpu::gl::ToParamName(pname, param_name, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return;
    }

    context->gl_GetIntegerv(param_name, data);
}

VGPUOGLAPI void GLAPIENTRY glGetLightfv(GLenum light, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(light, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetLightiv(GLenum light, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(light, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetMapdv(GLenum target, GLenum query, GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(target, query, v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetMapfv(GLenum target, GLenum query, GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(target, query, v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetMapiv(GLenum target, GLenum query, GLint *v) {
    VGPU_LOG_GL_FUNCTION(target, query, v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetMaterialfv(GLenum face, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(face, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetMaterialiv(GLenum face, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(face, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetPixelMapfv(GLenum map, GLfloat *values) {
    VGPU_LOG_GL_FUNCTION(map, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetPixelMapuiv(GLenum map, GLuint *values) {
    VGPU_LOG_GL_FUNCTION(map, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetPixelMapusv(GLenum map, GLushort *values) {
    VGPU_LOG_GL_FUNCTION(map, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetPointerv(GLenum pname, GLvoid **params) {
    VGPU_LOG_GL_FUNCTION(pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetPolygonStipple(GLubyte *mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI const GLubyte *GLAPIENTRY glGetString(GLenum name) {
    VGPU_LOG_GL_FUNCTION(name);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return nullptr;
    }

    vgpu::gl::StringName string_name;
    if (!vgpu::gl::ToStringName(name, string_name)) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return nullptr;
    }

    return context->gl_GetString(string_name);
}

VGPUOGLAPI void GLAPIENTRY glGetTexEnvfv(GLenum target, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexEnviv(GLenum target, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexGendv(GLenum coord, GLenum pname, GLdouble *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexGenfv(GLenum coord, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexGeniv(GLenum coord, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexImage(GLenum target, GLint level, GLenum format, GLenum type, GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(target, level, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(target, level, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(target, level, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexParameterfv(GLenum target, GLenum pname, GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetTexParameteriv(GLenum target, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glHint(GLenum target, GLenum mode) {
    VGPU_LOG_GL_FUNCTION(target, mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexMask(GLuint mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexPointer(GLenum type, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexd(GLdouble c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexdv(const GLdouble *c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexf(GLfloat c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexfv(const GLfloat *c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexi(GLint c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexiv(const GLint *c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexs(GLshort c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexsv(const GLshort *c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexub(GLubyte c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glIndexubv(const GLubyte *c) {
    VGPU_LOG_GL_FUNCTION(c);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glInitNames() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glInterleavedArrays(GLenum format, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(format, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLboolean GLAPIENTRY glIsEnabled(GLenum cap) {
    VGPU_LOG_GL_FUNCTION(cap);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLboolean>(0);
}

VGPUOGLAPI GLboolean GLAPIENTRY glIsList(GLuint list) {
    VGPU_LOG_GL_FUNCTION(list);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLboolean>(0);
}

VGPUOGLAPI GLboolean GLAPIENTRY glIsTexture(GLuint texture) {
    VGPU_LOG_GL_FUNCTION(texture);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLboolean>(0);
}

VGPUOGLAPI void GLAPIENTRY glLightModelf(GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightModelfv(GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightModeli(GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightModeliv(GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightf(GLenum light, GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(light, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightfv(GLenum light, GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(light, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLighti(GLenum light, GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(light, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLightiv(GLenum light, GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(light, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLineStipple(GLint factor, GLushort pattern) {
    VGPU_LOG_GL_FUNCTION(factor, pattern);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLineWidth(GLfloat width) {
    VGPU_LOG_GL_FUNCTION(width);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glListBase(GLuint base) {
    VGPU_LOG_GL_FUNCTION(base);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLoadIdentity() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLoadMatrixd(const GLdouble *m) {
    VGPU_LOG_GL_FUNCTION(m);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLoadMatrixf(const GLfloat *m) {
    VGPU_LOG_GL_FUNCTION(m);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLoadName(GLuint name) {
    VGPU_LOG_GL_FUNCTION(name);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glLogicOp(GLenum opcode) {
    VGPU_LOG_GL_FUNCTION(opcode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMap1d(GLenum target, GLdouble u1, GLdouble u2, GLint stride, GLint order, const GLdouble *points) {
    VGPU_LOG_GL_FUNCTION(target, u1, u2, stride, order, points);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMap1f(GLenum target, GLfloat u1, GLfloat u2, GLint stride, GLint order, const GLfloat *points) {
    VGPU_LOG_GL_FUNCTION(target, u1, u2, stride, order, points);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMap2d(GLenum target, GLdouble u1, GLdouble u2, GLint ustride, GLint uorder, GLdouble v1, GLdouble v2, GLint vstride, GLint vorder, const GLdouble *points) {
    VGPU_LOG_GL_FUNCTION(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMap2f(GLenum target, GLfloat u1, GLfloat u2, GLint ustride, GLint uorder, GLfloat v1, GLfloat v2, GLint vstride, GLint vorder, const GLfloat *points) {
    VGPU_LOG_GL_FUNCTION(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMapGrid1d(GLint un, GLdouble u1, GLdouble u2) {
    VGPU_LOG_GL_FUNCTION(un, u1, u2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMapGrid1f(GLint un, GLfloat u1, GLfloat u2) {
    VGPU_LOG_GL_FUNCTION(un, u1, u2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMapGrid2d(GLint un, GLdouble u1, GLdouble u2, GLint vn, GLdouble v1, GLdouble v2) {
    VGPU_LOG_GL_FUNCTION(un, u1, u2, vn, v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMapGrid2f(GLint un, GLfloat u1, GLfloat u2, GLint vn, GLfloat v1, GLfloat v2) {
    VGPU_LOG_GL_FUNCTION(un, u1, u2, vn, v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMaterialf(GLenum face, GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(face, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMaterialfv(GLenum face, GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(face, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMateriali(GLenum face, GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(face, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMaterialiv(GLenum face, GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(face, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMatrixMode(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMultMatrixd(const GLdouble *m) {
    VGPU_LOG_GL_FUNCTION(m);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glMultMatrixf(const GLfloat *m) {
    VGPU_LOG_GL_FUNCTION(m);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNewList(GLuint list, GLenum mode) {
    VGPU_LOG_GL_FUNCTION(list, mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3b(GLbyte nx, GLbyte ny, GLbyte nz) {
    VGPU_LOG_GL_FUNCTION(nx, ny, nz);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3bv(const GLbyte *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3d(GLdouble nx, GLdouble ny, GLdouble nz) {
    VGPU_LOG_GL_FUNCTION(nx, ny, nz);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3f(GLfloat nx, GLfloat ny, GLfloat nz) {
    VGPU_LOG_GL_FUNCTION(nx, ny, nz);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3i(GLint nx, GLint ny, GLint nz) {
    VGPU_LOG_GL_FUNCTION(nx, ny, nz);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3s(GLshort nx, GLshort ny, GLshort nz) {
    VGPU_LOG_GL_FUNCTION(nx, ny, nz);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormal3sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glNormalPointer(GLenum type, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar) {
    VGPU_LOG_GL_FUNCTION(left, right, bottom, top, zNear, zFar);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPassThrough(GLfloat token) {
    VGPU_LOG_GL_FUNCTION(token);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelMapfv(GLenum map, GLsizei mapsize, const GLfloat *values) {
    VGPU_LOG_GL_FUNCTION(map, mapsize, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelMapuiv(GLenum map, GLsizei mapsize, const GLuint *values) {
    VGPU_LOG_GL_FUNCTION(map, mapsize, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelMapusv(GLenum map, GLsizei mapsize, const GLushort *values) {
    VGPU_LOG_GL_FUNCTION(map, mapsize, values);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelStoref(GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelStorei(GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelTransferf(GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelTransferi(GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPixelZoom(GLfloat xfactor, GLfloat yfactor) {
    VGPU_LOG_GL_FUNCTION(xfactor, yfactor);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPointSize(GLfloat size) {
    VGPU_LOG_GL_FUNCTION(size);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPolygonMode(GLenum face, GLenum mode) {
    VGPU_LOG_GL_FUNCTION(face, mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPolygonOffset(GLfloat factor, GLfloat units) {
    VGPU_LOG_GL_FUNCTION(factor, units);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPolygonStipple(const GLubyte *mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPopAttrib() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPopClientAttrib() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPopMatrix() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPopName() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPrioritizeTextures(GLsizei n, const GLuint *textures, const GLclampf *priorities) {
    VGPU_LOG_GL_FUNCTION(n, textures, priorities);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPushAttrib(GLbitfield mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPushClientAttrib(GLbitfield mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPushMatrix() {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glPushName(GLuint name) {
    VGPU_LOG_GL_FUNCTION(name);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2d(GLdouble x, GLdouble y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2f(GLfloat x, GLfloat y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2i(GLint x, GLint y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2s(GLshort x, GLshort y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos2sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3d(GLdouble x, GLdouble y, GLdouble z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3f(GLfloat x, GLfloat y, GLfloat z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3i(GLint x, GLint y, GLint z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3s(GLshort x, GLshort y, GLshort z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos3sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4d(GLdouble x, GLdouble y, GLdouble z, GLdouble w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4f(GLfloat x, GLfloat y, GLfloat z, GLfloat w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4i(GLint x, GLint y, GLint z, GLint w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4s(GLshort x, GLshort y, GLshort z, GLshort w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRasterPos4sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glReadBuffer(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(x, y, width, height, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectd(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2) {
    VGPU_LOG_GL_FUNCTION(x1, y1, x2, y2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectdv(const GLdouble *v1, const GLdouble *v2) {
    VGPU_LOG_GL_FUNCTION(v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectf(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2) {
    VGPU_LOG_GL_FUNCTION(x1, y1, x2, y2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectfv(const GLfloat *v1, const GLfloat *v2) {
    VGPU_LOG_GL_FUNCTION(v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRecti(GLint x1, GLint y1, GLint x2, GLint y2) {
    VGPU_LOG_GL_FUNCTION(x1, y1, x2, y2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectiv(const GLint *v1, const GLint *v2) {
    VGPU_LOG_GL_FUNCTION(v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRects(GLshort x1, GLshort y1, GLshort x2, GLshort y2) {
    VGPU_LOG_GL_FUNCTION(x1, y1, x2, y2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRectsv(const GLshort *v1, const GLshort *v2) {
    VGPU_LOG_GL_FUNCTION(v1, v2);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLint GLAPIENTRY glRenderMode(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLint>(0);
}

VGPUOGLAPI void GLAPIENTRY glRotated(GLdouble angle, GLdouble x, GLdouble y, GLdouble z) {
    VGPU_LOG_GL_FUNCTION(angle, x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glRotatef(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
    VGPU_LOG_GL_FUNCTION(angle, x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glScaled(GLdouble x, GLdouble y, GLdouble z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glScalef(GLfloat x, GLfloat y, GLfloat z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glScissor(GLint x, GLint y, GLsizei width, GLsizei height) {
    VGPU_LOG_GL_FUNCTION(x, y, width, height);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glSelectBuffer(GLsizei size, GLuint *buffer) {
    VGPU_LOG_GL_FUNCTION(size, buffer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glShadeModel(GLenum mode) {
    VGPU_LOG_GL_FUNCTION(mode);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glStencilFunc(GLenum func, GLint ref, GLuint mask) {
    VGPU_LOG_GL_FUNCTION(func, ref, mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glStencilMask(GLuint mask) {
    VGPU_LOG_GL_FUNCTION(mask);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glStencilOp(GLenum fail, GLenum zfail, GLenum zpass) {
    VGPU_LOG_GL_FUNCTION(fail, zfail, zpass);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1d(GLdouble s) {
    VGPU_LOG_GL_FUNCTION(s);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1f(GLfloat s) {
    VGPU_LOG_GL_FUNCTION(s);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1i(GLint s) {
    VGPU_LOG_GL_FUNCTION(s);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1s(GLshort s) {
    VGPU_LOG_GL_FUNCTION(s);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord1sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2d(GLdouble s, GLdouble t) {
    VGPU_LOG_GL_FUNCTION(s, t);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2f(GLfloat s, GLfloat t) {
    VGPU_LOG_GL_FUNCTION(s, t);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2i(GLint s, GLint t) {
    VGPU_LOG_GL_FUNCTION(s, t);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2s(GLshort s, GLshort t) {
    VGPU_LOG_GL_FUNCTION(s, t);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord2sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3d(GLdouble s, GLdouble t, GLdouble r) {
    VGPU_LOG_GL_FUNCTION(s, t, r);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3f(GLfloat s, GLfloat t, GLfloat r) {
    VGPU_LOG_GL_FUNCTION(s, t, r);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3i(GLint s, GLint t, GLint r) {
    VGPU_LOG_GL_FUNCTION(s, t, r);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3s(GLshort s, GLshort t, GLshort r) {
    VGPU_LOG_GL_FUNCTION(s, t, r);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord3sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4d(GLdouble s, GLdouble t, GLdouble r, GLdouble q) {
    VGPU_LOG_GL_FUNCTION(s, t, r, q);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4f(GLfloat s, GLfloat t, GLfloat r, GLfloat q) {
    VGPU_LOG_GL_FUNCTION(s, t, r, q);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4i(GLint s, GLint t, GLint r, GLint q) {
    VGPU_LOG_GL_FUNCTION(s, t, r, q);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4s(GLshort s, GLshort t, GLshort r, GLshort q) {
    VGPU_LOG_GL_FUNCTION(s, t, r, q);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoord4sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexCoordPointer(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(size, type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexEnvf(GLenum target, GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(target, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexEnvfv(GLenum target, GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexEnvi(GLenum target, GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(target, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexEnviv(GLenum target, GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGend(GLenum coord, GLenum pname, GLdouble param) {
    VGPU_LOG_GL_FUNCTION(coord, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGendv(GLenum coord, GLenum pname, const GLdouble *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGenf(GLenum coord, GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(coord, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGenfv(GLenum coord, GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGeni(GLenum coord, GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(coord, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexGeniv(GLenum coord, GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(coord, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexImage1D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(target, level, internalformat, width, border, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(target, level, internalformat, width, height, border, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexParameterf(GLenum target, GLenum pname, GLfloat param) {
    VGPU_LOG_GL_FUNCTION(target, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexParameterfv(GLenum target, GLenum pname, const GLfloat *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexParameteri(GLenum target, GLenum pname, GLint param) {
    VGPU_LOG_GL_FUNCTION(target, pname, param);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexParameteriv(GLenum target, GLenum pname, const GLint *params) {
    VGPU_LOG_GL_FUNCTION(target, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(target, level, xoffset, width, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels) {
    VGPU_LOG_GL_FUNCTION(target, level, xoffset, yoffset, width, height, format, type, pixels);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTranslated(GLdouble x, GLdouble y, GLdouble z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glTranslatef(GLfloat x, GLfloat y, GLfloat z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2d(GLdouble x, GLdouble y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2f(GLfloat x, GLfloat y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2i(GLint x, GLint y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2s(GLshort x, GLshort y) {
    VGPU_LOG_GL_FUNCTION(x, y);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex2sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3d(GLdouble x, GLdouble y, GLdouble z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3f(GLfloat x, GLfloat y, GLfloat z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3i(GLint x, GLint y, GLint z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3s(GLshort x, GLshort y, GLshort z) {
    VGPU_LOG_GL_FUNCTION(x, y, z);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex3sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4d(GLdouble x, GLdouble y, GLdouble z, GLdouble w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4dv(const GLdouble *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4f(GLfloat x, GLfloat y, GLfloat z, GLfloat w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4fv(const GLfloat *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4i(GLint x, GLint y, GLint z, GLint w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4iv(const GLint *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4s(GLshort x, GLshort y, GLshort z, GLshort w) {
    VGPU_LOG_GL_FUNCTION(x, y, z, w);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertex4sv(const GLshort *v) {
    VGPU_LOG_GL_FUNCTION(v);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertexPointer(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    VGPU_LOG_GL_FUNCTION(size, type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glViewport(GLint x, GLint y, GLsizei width, GLsizei height) {
    VGPU_LOG_GL_FUNCTION(x, y, width, height);
    VGPU_NOT_IMPLEMENTED();
}

#ifdef GL_VERSION_1_5
VGPUOGLAPI void GLAPIENTRY glBindBuffer(GLenum target, GLuint buffer) {
    VGPU_LOG_GL_FUNCTION(target, buffer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDeleteBuffers(GLsizei n, const GLuint *buffers) {
    VGPU_LOG_GL_FUNCTION(n, buffers);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGenBuffers(GLsizei n, GLuint *buffers) {
    VGPU_LOG_GL_FUNCTION(n, buffers);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBufferData(GLenum target, GLsizeiptr size, const void *data, GLenum usage) {
    VGPU_LOG_GL_FUNCTION(target, size, data, usage);
    VGPU_NOT_IMPLEMENTED();
}
#endif // GL_VERSION_1_5

#ifdef GL_VERSION_2_0
VGPUOGLAPI void GLAPIENTRY glAttachShader(GLuint program, GLuint shader) {
    VGPU_LOG_GL_FUNCTION(program, shader);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glCompileShader(GLuint shader) {
    VGPU_LOG_GL_FUNCTION(shader);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLuint GLAPIENTRY glCreateProgram(void) {
    VGPU_LOG_GL_FUNCTION();
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLuint>(0);
}

VGPUOGLAPI GLuint GLAPIENTRY glCreateShader(GLenum type) {
    VGPU_LOG_GL_FUNCTION(type);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return static_cast<GLuint>(0);
    }

    vgpu::gl::ShaderType shader_type;
    if (!vgpu::gl::ToShaderType(type, shader_type, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return static_cast<GLuint>(0);
    }

    return context->gl_CreateShader(shader_type);
}

VGPUOGLAPI void GLAPIENTRY glDeleteProgram(GLuint program) {
    VGPU_LOG_GL_FUNCTION(program);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDeleteShader(GLuint shader) {
    VGPU_LOG_GL_FUNCTION(shader);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDetachShader(GLuint program, GLuint shader) {
    VGPU_LOG_GL_FUNCTION(program, shader);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDisableVertexAttribArray(GLuint index) {
    VGPU_LOG_GL_FUNCTION(index);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glEnableVertexAttribArray(GLuint index) {
    VGPU_LOG_GL_FUNCTION(index);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetProgramiv(GLuint program, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(program, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) {
    VGPU_LOG_GL_FUNCTION(program, bufSize, length, infoLog);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetShaderiv(GLuint shader, GLenum pname, GLint *params) {
    VGPU_LOG_GL_FUNCTION(shader, pname, params);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) {
    VGPU_LOG_GL_FUNCTION(shader, bufSize, length, infoLog);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI GLint GLAPIENTRY glGetUniformLocation(GLuint program, const GLchar *name) {
    VGPU_LOG_GL_FUNCTION(program, name);
    VGPU_NOT_IMPLEMENTED();
    return static_cast<GLuint>(0);
}

VGPUOGLAPI void GLAPIENTRY glLinkProgram(GLuint program) {
    VGPU_LOG_GL_FUNCTION(program);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glShaderSource(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length) {
    VGPU_LOG_GL_FUNCTION(shader, count, string, length);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    context->gl_ShaderSource(shader, count, string, length);
}

VGPUOGLAPI void GLAPIENTRY glUseProgram(GLuint program) {
    VGPU_LOG_GL_FUNCTION(program);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) {
    VGPU_LOG_GL_FUNCTION(location, count, transpose, value);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer) {
    VGPU_LOG_GL_FUNCTION(index, size, type, normalized, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}
#endif // GL_VERSION_2_0

#ifdef GL_VERSION_3_0
VGPUOGLAPI void GLAPIENTRY glVertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) {
    VGPU_LOG_GL_FUNCTION(index, size, type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glBindVertexArray(GLuint array) {
    VGPU_LOG_GL_FUNCTION(array);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glDeleteVertexArrays(GLsizei n, const GLuint *arrays) {
    VGPU_LOG_GL_FUNCTION(n, arrays);
    VGPU_NOT_IMPLEMENTED();
}

VGPUOGLAPI void GLAPIENTRY glGenVertexArrays(GLsizei n, GLuint *arrays) {
    VGPU_LOG_GL_FUNCTION(n, arrays);
    VGPU_NOT_IMPLEMENTED();
}
#endif // GL_VERSION_3_0

#ifdef GL_VERSION_4_1
VGPUOGLAPI void GLAPIENTRY glVertexAttribLPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) {
    VGPU_LOG_GL_FUNCTION(index, size, type, stride, pointer);
    VGPU_NOT_IMPLEMENTED();
}
#endif // GL_VERSION_4_1

#ifdef GL_VERSION_3_2
VGPUOGLAPI void GLAPIENTRY glGetInteger64v(GLenum pname, GLint64 *data) {
    VGPU_LOG_GL_FUNCTION(pname, data);

    std::shared_ptr<vgpu::Win32Context> context = g_driver.CurrentContext();
    if (!context) {
        return;
    }

    vgpu::gl::ParamName param_name;
    if (!vgpu::gl::ToParamName(pname, param_name, context->GetContextDescriptor())) {
        context->gl_SetError(vgpu::gl::Error::InvalidEnum);
        return;
    }

    context->gl_GetInteger64v(param_name, data);
}
#endif // GL_VERSION_3_2

#ifdef GL_VERSION_4_5
VGPUOGLAPI void GLAPIENTRY glNamedBufferData(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage) {
    VGPU_LOG_GL_FUNCTION(buffer, size, data, usage);
    VGPU_NOT_IMPLEMENTED();
}
#endif // GL_VERSION_4_5
