#include "win32driver.hpp"
#include <set>
#include "win32context.hpp"
#include "common/constants.hpp"
#include "common/assert.hpp"
#include "common/logger.hpp"
#include "pixelformat/pixelformatdescriptor.hpp"
#include "vgpu/command_buffer/command.hpp"

namespace vgpu {

Win32Driver::Win32Driver() {
    GeneratePixelFormats();
}

Win32Driver::~Win32Driver() {
    if (vgpu_thread_.joinable()) {
        vgpu_->PushCommand(Command::Kill);
        vgpu_->FlushCommandBuffer();
        vgpu_thread_.join();
    }
}

DHGLRC Win32Driver::icd_CreateContext(HDC hdc, INT iLayerPlane /* = 0 */) {
    VGPU_LOG_DRV_FUNCTION(hdc, iLayerPlane);
    if (context_creation_notifier_ != 0x0) {
        uintptr_t dhglrc = context_creation_notifier_;
        context_creation_notifier_ = 0x0;
        return reinterpret_cast<DHGLRC>(dhglrc);
    }

    ContextDescriptor cd{
        Constants::Constants::supported_context_versions_.back(),
        iLayerPlane,
        false, false,
        true, false
    };
    return reinterpret_cast<DHGLRC>(CreateContext(hdc, cd));
}

BOOL Win32Driver::icd_DeleteContext(DHGLRC dhglrc) {
    VGPU_LOG_DRV_FUNCTION(dhglrc);
    uintptr_t context_index = reinterpret_cast<uintptr_t>(dhglrc);
    std::lock_guard<std::mutex> lg{mutex_};

    auto context_it = contexts_.find(context_index);
    if (context_it == contexts_.end())
        return FALSE;

    contexts_.erase(context_it);
    return TRUE;
}

LONG Win32Driver::icd_DescribePixelFormat(HDC hdc, LONG iPixelFormat, ULONG cjpfd, PIXELFORMATDESCRIPTOR *ppfd) {
    VGPU_LOG_DRV_FUNCTION(hdc, iPixelFormat, cjpfd, ppfd);
    LONG max_index = static_cast<LONG>(pfds_.size());

    if (ppfd) {
        if (cjpfd != sizeof(PIXELFORMATDESCRIPTOR) || iPixelFormat > max_index)
            return 0;
        *ppfd = pfds_[iPixelFormat - 1].pfd;
    }

    return max_index;
}

PROC Win32Driver::icd_GetProcAddress(LPCSTR lpszProc) {
    VGPU_LOG_DRV_FUNCTION(lpszProc);
    if (!CurrentContext())
        return NULL;

    return Win32Context::wgl_GetProcAddress(lpszProc);
}

BOOL Win32Driver::icd_ReleaseContext(DHGLRC dhglrc) {
    VGPU_LOG_DRV_FUNCTION(dhglrc);
    uintptr_t context_index = reinterpret_cast<uintptr_t>(dhglrc);

    std::unique_lock<std::mutex> ul{mutex_};
    auto context_it = contexts_.find(context_index);
    if (context_it == contexts_.end() || context_it->second != thread_current_contex_)
        return FALSE;
    ul.unlock();

    thread_current_contex_->gl_Flush();
    thread_current_contex_.reset();
    return TRUE;
}

void Win32Driver::icd_SetCallbackProcs(INT nProcs, PROC *pProcs) {
    VGPU_LOG_DRV_FUNCTION(nProcs, pProcs);
    std::lock_guard<std::mutex> lg{mutex_};

    if (nProcs > 0 && !wgl_callbacks_.SetCurrentValue)
        wgl_callbacks_.SetCurrentValue = reinterpret_cast<PFN_SETCURRENTVALUE>(pProcs[0]);
    if (nProcs > 1 && !wgl_callbacks_.GetCurrentValue)
        wgl_callbacks_.GetCurrentValue = reinterpret_cast<PFN_GETCURRENTVALUE>(pProcs[1]);
    if (nProcs > 2 && !wgl_callbacks_.GetDHGLRC)
        wgl_callbacks_.GetDHGLRC = reinterpret_cast<PFN_GETDHGLRC>(pProcs[2]);
    if (nProcs > 3 && !wgl_callbacks_.GetDDHandle)
        wgl_callbacks_.GetDDHandle = reinterpret_cast<PFN_GETDDHANDLE>(pProcs[3]);
    if (nProcs > 4 && !wgl_callbacks_.PresentBuffers)
        wgl_callbacks_.PresentBuffers = reinterpret_cast<PFN_PRESENTBUFFERS>(pProcs[4]);
}

PGLCLTPROCTABLE Win32Driver::icd_SetContext(HDC hdc, DHGLRC dhglrc, PFN_SETPROCTABLE pfnSetProcTable) {
    VGPU_LOG_DRV_FUNCTION(hdc, dhglrc, pfnSetProcTable);
    uintptr_t context_index = reinterpret_cast<uintptr_t>(dhglrc);

    std::unique_lock<std::mutex> ul{mutex_};
    auto context_it = contexts_.find(context_index);
    if (context_it == contexts_.end())
        return NULL;
    
    if (thread_current_contex_) {
        thread_current_contex_->gl_Flush();
    }

    thread_current_contex_ = context_it->second;
    ul.unlock();

    return Win32Context::GetGL110ProcTable();
}

BOOL Win32Driver::icd_SetPixelFormat(HDC hdc, LONG iPixelFormat) {
    VGPU_LOG_DRV_FUNCTION(hdc, iPixelFormat);
    if (thread_pfd_index_locked_ || iPixelFormat > static_cast<LONG>(pfds_.size()))
        return FALSE;

    thread_current_pfd_index_ = iPixelFormat - 1;
    return TRUE;
}

BOOL Win32Driver::icd_SwapBuffers(HDC hdc) {
    VGPU_LOG_DRV_FUNCTION(hdc);
    /* PresentBuffers swap method.
    using pfnD3DKMTOpenAdapterFromHdc = NTSTATUS(APIENTRY *)(D3DKMT_OPENADAPTERFROMHDC *pData);
    constexpr uint32_t width = 1024;
    constexpr uint32_t height = 1024;
    constexpr uint8_t bpp = 4;

    if (!s_wglCallbacks.PresentBuffers) {
        assert(false);
    }

    static pfnD3DKMTOpenAdapterFromHdc D3DKMTOpenAdapterFromHdc{nullptr};

    if (!D3DKMTOpenAdapterFromHdc) {
        HINSTANCE gdi32 = LoadLibraryA("gdi32.dll");
        assert(gdi32);
        D3DKMTOpenAdapterFromHdc = (pfnD3DKMTOpenAdapterFromHdc)GetProcAddress(gdi32, "D3DKMTOpenAdapterFromHdc");
        assert(D3DKMTOpenAdapterFromHdc);
    }

    typedef struct _DISPLAY_DEVICEW {
        DWORD  cb;
        WCHAR  DeviceName[32];
        WCHAR  DeviceString[128];
        DWORD  StateFlags;
        WCHAR  DeviceID[128];
        WCHAR  DeviceKey[128];
    } DISPLAY_DEVICEW, *PDISPLAY_DEVICEW, *LPDISPLAY_DEVICEW;

    DISPLAY_DEVICE dd;
    memset(&dd, 0, sizeof(DISPLAY_DEVICE));
    dd.cb = sizeof(DISPLAY_DEVICE);
    for (int i = 0; EnumDisplayDevices(NULL, i, &dd, 0); ++i) {
        std::wcout << dd.DeviceName << std::endl;
        std::wcout << dd.DeviceString << std::endl;
        std::wcout << dd.DeviceID << std::endl;
        std::wcout << dd.DeviceKey << std::endl;
        std::wcout << std::endl;

        if (dd.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE) {
            break;
        }
    }

    HDC dd_hdc = CreateDC(NULL, dd.DeviceName, NULL, NULL);


    static D3DKMT_OPENADAPTERFROMHDC OpenAdapterData{
        dd_hdc,
        0,
        0,
        0
    };
    static NTSTATUS status = D3DKMTOpenAdapterFromHdc(&OpenAdapterData);
    assert(status == STATUS_SUCCESS);

    static uint8_t buffer[width * height * bpp]{0xDB};
    if (buffer[0] == 0xDB) {
        for (uint32_t h = 0; h < height; h++) {
            for (uint32_t w = 0; w < width; w++) {
                buffer[h*width*bpp + w*bpp + 0] = h < (height / 2) ? 0xFF : 0x0;
                buffer[h*width*bpp + w*bpp + 1] = 0x0;
                buffer[h*width*bpp + w*bpp + 2] = 0x0;
                buffer[h*width*bpp + w*bpp + 3] = 0xFF;
            }
        }
    }
    static RECT rect{
        0,
        0,
        rect.left + width,
        rect.top - height
    };

    static PRESENTBUFFERSCB pbData{
        2,
        0,
        OpenAdapterData.AdapterLuid,
        buffer,
        rect
    };

    BOOL result = s_wglCallbacks.PresentBuffers(dd_hdc, &pbData);
    DeleteDC(dd_hdc);

    return result;
    */

    constexpr uint32_t width = 512;
    constexpr uint32_t height = 512;
    constexpr uint8_t bpp = sizeof(uint16_t);

    static uint16_t buffer1[width * height]{0xDB};
    static uint16_t buffer2[width * height]{0xDB};
    static bool swap{true};
    if (buffer1[0] == 0xDB) {
        for (uint32_t h = 0; h < height; h++) {
            for (uint32_t w = 0; w < width; w++) {
                buffer1[h*width + w] = 0x001F;
                buffer2[h*width + w] = 0x07E0;
            }
        }
    }

    char *bmi_buff[sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 3];
    BITMAPINFO *bmi = reinterpret_cast<BITMAPINFO *>(bmi_buff);
    BITMAPINFOHEADER *bmih = reinterpret_cast<BITMAPINFOHEADER *>(&bmi->bmiHeader);
    bmih->biSize = sizeof(BITMAPINFOHEADER);
    bmih->biWidth = width;
    bmih->biHeight = height;
    bmih->biPlanes = 1;
    bmih->biBitCount = bpp * 8;
    bmih->biCompression = BI_BITFIELDS;
    bmih->biSizeImage = 0;
    bmih->biXPelsPerMeter = 0;
    bmih->biYPelsPerMeter = 0;
    bmih->biClrUsed = 0;
    bmih->biClrImportant = 0;
    DWORD *mask = reinterpret_cast<DWORD *>(bmi->bmiColors);
    mask[0] = 0x001F;
    mask[1] = 0x07E0;
    mask[2] = 0xF800;
    int result = StretchDIBits(hdc, 0, 0, width, height, 0, 0, width, height, swap ? buffer1 : buffer2, bmi, 0, SRCCOPY);
    swap = !swap;
    return result;
}

BOOL Win32Driver::icd_ValidateVersion(ULONG ulVersion) {
    VGPU_LOG_DRV_FUNCTION(ulVersion);

    // New thread cannot be created at DllMain that is why
    // creation of ExecutionLoop thread is done here - it is
    // first ICD call right after DllMain
    if (!vgpu_) {
        vgpu_ = std::make_shared<VGPU>();
        vgpu_thread_ = std::thread([&] { vgpu_->ExecutionLoop(); });
    }

    if (ulVersion == GL_VERSION_1_1)
        return TRUE;
    return FALSE;
}

void Win32Driver::icd_ThreadAttach() {
    VGPU_LOG_DRV_FUNCTION();
}

void Win32Driver::icd_ThreadDetach() {
    VGPU_LOG_DRV_FUNCTION();
}

BOOL Win32Driver::wgl_ChoosePixelFormat(HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats) {
    VGPU_LOG_DRV_FUNCTION(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
    PIXELFORMATDESCRIPTOR pfd = AttribsToPixelFormatDescriptor(piAttribIList, pfAttribFList);

    int pf_indices[] = {ChoosePixelFormat(hdc, &pfd)};
    UINT pf_indices_size = sizeof(pf_indices) / sizeof(*pf_indices);
    UINT stored_pfds = 0;

    while (stored_pfds < nMaxFormats && stored_pfds < pf_indices_size) {
        if (pf_indices[stored_pfds] == 0) {
            return FALSE;
        }
        piFormats[stored_pfds] = pf_indices[stored_pfds];
        stored_pfds++;
    }

    *nNumFormats = stored_pfds;

    return TRUE;
}

HGLRC Win32Driver::wgl_CreateContextAttribs(HDC hdc, HGLRC hShareContext, const int *attribList) {
    VGPU_LOG_DRV_FUNCTION(hdc, hShareContext, attribList);
    ContextDescriptor cd = AttribsToContextDescriptor(attribList);
    return reinterpret_cast<HGLRC>(CreateContext(hdc, cd, reinterpret_cast<uintptr_t>(hShareContext), true));
}

void Win32Driver::GeneratePixelFormats() {
    if (!pfds_.empty()) {
        return;
    }

    DWORD dwFlags = PFD_DOUBLEBUFFER |
        PFD_DRAW_TO_WINDOW |
        PFD_SUPPORT_OPENGL |
        PFD_SWAP_COPY |
        PFD_SUPPORT_COMPOSITION;

    PIXELFORMATDESCRIPTOR pfd{
        sizeof(PIXELFORMATDESCRIPTOR),  // WORD  nSize
        1,                              // WORD  nVersion
        dwFlags,                        // DWORD dwFlags
        PFD_TYPE_RGBA,                  // BYTE  iPixelType
        0,                              // BYTE  cColorBits
        0, 0, 0, 0, 0, 0, 0, 0,         // BYTE  c[Red,Green,Blue,Alpha][Bits,Shift]
        64,                             // BYTE  cAccumBits
        16, 16, 16, 16,                 // BYTE  cAccum[Red,Green,Blue,Alpha]Bits
        0, 0,                           // BYTE  c[Depth,Stencil]Bits
        4,                              // BYTE  cAuxBuffers
        0,                              // BYTE  iLayerType
        0,                              // BYTE  bReserved
        16, 16, 16                      // DWORD  dw[Layer,Visible,Damage]Mask
    };

    std::vector<PixelFormat> supported_color_formats{
        PixelFormat::B8G8R8A8_UNorm,
        PixelFormat::B8G8R8X8_UNorm,
        PixelFormat::A8R8G8B8_UNorm,
        PixelFormat::X8R8G8B8_UNorm,
        PixelFormat::B5G5R5A1_UNorm,
        PixelFormat::B4G4R4A4_UNorm,
        PixelFormat::B5G6R5_UNorm,
        PixelFormat::R10G10B10A2_UNorm
    };

    std::vector<PixelFormat> supported_ds_formats{
        PixelFormat::Z16_UNorm,
        PixelFormat::Z32_UNorm,
        PixelFormat::Z24_UNorm_S8_UInt,
        PixelFormat::S8_UInt_Z24_UNorm,
        PixelFormat::Z24X8_UNorm,
        PixelFormat::X8Z24_UNorm,
        PixelFormat::S8_UInt
    };

    std::set<PixelFormatEntity> pfds_set;
    for (PixelFormat color : supported_color_formats) {
        auto color_desc = PFD::Get(color);

        pfd.cColorBits = color_desc.GetTexelSize() * 8;
        pfd.cRedBits = color_desc.GetBits(ChannelType::Red);
        pfd.cRedShift = color_desc.GetShift(ChannelType::Red);
        pfd.cGreenBits = color_desc.GetBits(ChannelType::Green);
        pfd.cGreenShift = color_desc.GetShift(ChannelType::Green);
        pfd.cBlueBits = color_desc.GetBits(ChannelType::Blue);
        pfd.cBlueShift = color_desc.GetShift(ChannelType::Blue);
        pfd.cAlphaBits = color_desc.GetBits(ChannelType::Alpha);
        pfd.cAlphaShift = color_desc.GetShift(ChannelType::Alpha);

        for (PixelFormat ds : supported_ds_formats) {
            auto ds_desc = PFD::Get(ds);

            pfd.cDepthBits = ds_desc.GetBits(ChannelType::Depth);
            pfd.cStencilBits = ds_desc.GetBits(ChannelType::Stencil);

            pfds_set.insert(PixelFormatEntity{pfd, color, ds});
        }
    }

    pfds_.assign(pfds_set.begin(), pfds_set.end());
}

PIXELFORMATDESCRIPTOR Win32Driver::AttribsToPixelFormatDescriptor(const int *piAttribIList, const FLOAT *pfAttribFList) {
    PIXELFORMATDESCRIPTOR pfd{};

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwLayerMask = 16;
    pfd.dwVisibleMask = 16;
    pfd.dwDamageMask = 16;

    if (piAttribIList) {
        while (*piAttribIList != 0) {
            switch (*piAttribIList++) {
            case WGL_DRAW_TO_WINDOW_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_DRAW_TO_WINDOW : 0x0;
                break;
            case WGL_DRAW_TO_BITMAP_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_DRAW_TO_BITMAP : 0x0;
                break;
            case WGL_ACCELERATION_ARB:
                switch (*piAttribIList) {
                case WGL_NO_ACCELERATION_ARB:
                case WGL_GENERIC_ACCELERATION_ARB:
                case WGL_FULL_ACCELERATION_ARB:
                    break;
                default:
                    VGPU_LOG_WGL("Unexpected attrib value - WGL_ACCELERATION_ARB : ", *piAttribIList);
                    break;
                }
                break;
            case WGL_NEED_PALETTE_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_NEED_PALETTE : 0x0;
                break;
            case WGL_NEED_SYSTEM_PALETTE_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_NEED_SYSTEM_PALETTE : 0x0;
                break;
            case WGL_SWAP_LAYER_BUFFERS_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_SWAP_LAYER_BUFFERS : 0x0;
                break;
            case WGL_SWAP_METHOD_ARB:
                switch (*piAttribIList) {
                case WGL_SWAP_EXCHANGE_ARB:
                    pfd.dwFlags |= PFD_SWAP_EXCHANGE;
                    break;
                case WGL_SWAP_COPY_ARB:
                    pfd.dwFlags |= PFD_SWAP_COPY;
                    break;
                case WGL_SWAP_UNDEFINED_ARB:
                    break;
                default:
                    VGPU_LOG_WGL("Unexpected attrib value - WGL_SWAP_METHOD_ARB : ", *piAttribIList);
                    break;
                }
                break;
            case WGL_NUMBER_OVERLAYS_ARB:
                pfd.bReserved |= *piAttribIList & 0xF;
                break;
            case WGL_NUMBER_UNDERLAYS_ARB:
                pfd.bReserved |= (*piAttribIList >> 4) & 0xF;
                break;
            case WGL_SHARE_DEPTH_ARB:
                VGPU_NOT_IMPLEMENTED();
                break;
            case WGL_SHARE_STENCIL_ARB:
                VGPU_NOT_IMPLEMENTED();
                break;
            case WGL_SHARE_ACCUM_ARB:
                VGPU_NOT_IMPLEMENTED();
                break;
            case WGL_SUPPORT_GDI_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_SUPPORT_GDI : 0x0;
                break;
            case WGL_SUPPORT_OPENGL_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_SUPPORT_OPENGL : 0x0;
                break;
            case WGL_DOUBLE_BUFFER_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_DOUBLEBUFFER : 0x0;
                break;
            case WGL_STEREO_ARB:
                pfd.dwFlags |= !!(*piAttribIList) ? PFD_STEREO : 0x0;
                break;
            case WGL_PIXEL_TYPE_ARB:
                switch (*piAttribIList) {
                case WGL_TYPE_RGBA_ARB:
                    pfd.iPixelType = PFD_TYPE_RGBA;
                    break;
                case WGL_TYPE_COLORINDEX_ARB:
                    pfd.iPixelType = PFD_TYPE_COLORINDEX;
                    break;
                default:
                    VGPU_LOG_WGL("Unexpected attrib value - WGL_PIXEL_TYPE_ARB : ", *piAttribIList);
                    break;
                }
                break;
            case WGL_COLOR_BITS_ARB:
                pfd.cColorBits = *piAttribIList;
                break;
            case WGL_RED_BITS_ARB:
                pfd.cRedBits = *piAttribIList;
                break;
            case WGL_RED_SHIFT_ARB:
                pfd.cRedShift = *piAttribIList;
                break;
            case WGL_GREEN_BITS_ARB:
                pfd.cGreenBits = *piAttribIList;
                break;
            case WGL_GREEN_SHIFT_ARB:
                pfd.cGreenShift = *piAttribIList;
                break;
            case WGL_BLUE_BITS_ARB:
                pfd.cBlueBits = *piAttribIList;
                break;
            case WGL_BLUE_SHIFT_ARB:
                pfd.cBlueShift = *piAttribIList;
                break;
            case WGL_ALPHA_BITS_ARB:
                pfd.cAlphaBits = *piAttribIList;
                break;
            case WGL_ALPHA_SHIFT_ARB:
                pfd.cAlphaShift = *piAttribIList;
                break;
            case WGL_ACCUM_BITS_ARB:
                pfd.cAccumBits = *piAttribIList;
                break;
            case WGL_ACCUM_RED_BITS_ARB:
                pfd.cAccumRedBits = *piAttribIList;
                break;
            case WGL_ACCUM_GREEN_BITS_ARB:
                pfd.cAccumGreenBits = *piAttribIList;
                break;
            case WGL_ACCUM_BLUE_BITS_ARB:
                pfd.cAccumBlueBits = *piAttribIList;
                break;
            case WGL_ACCUM_ALPHA_BITS_ARB:
                pfd.cAccumAlphaBits = *piAttribIList;
                break;
            case WGL_DEPTH_BITS_ARB:
                pfd.cDepthBits = *piAttribIList;
                break;
            case WGL_STENCIL_BITS_ARB:
                pfd.cStencilBits = *piAttribIList;
                break;
            case WGL_AUX_BUFFERS_ARB:
                pfd.cAuxBuffers = *piAttribIList;
                break;
            default:
#if VGPU_LOG_WGL_IS_ON
                --piAttribIList;
                VGPU_LOG_WGL("Unsupported pixel format attrib: ", *piAttribIList);
                piAttribIList++;
#endif
                break;
            }
            piAttribIList++;
        }
    }

    if (piAttribIList) {
        if (*pfAttribFList != 0) {
            VGPU_LOG_WGL("Float pixel format attribs aren't supported");
        }
    }

    return pfd;
}

ContextDescriptor Win32Driver::AttribsToContextDescriptor(const int *attribList) {
    int major_version{-1};
    int minor_version{-1};
    int layer_plane{0};
    int context_flags{0x0};
    int context_profile_mask{0x0};

    if (attribList) {
        while (*attribList != 0) {
            switch (*attribList++) {
            case WGL_CONTEXT_MAJOR_VERSION_ARB:
                major_version = *attribList;
                break;
            case WGL_CONTEXT_MINOR_VERSION_ARB:
                minor_version = *attribList;
                break;
            case WGL_CONTEXT_LAYER_PLANE_ARB:
                layer_plane = *attribList;
                break;
            case WGL_CONTEXT_FLAGS_ARB:
                context_flags = *attribList;
                break;
            case WGL_CONTEXT_PROFILE_MASK_ARB:
                context_profile_mask = *attribList;
                break;
            default:
#if VGPU_LOG_WGL_IS_ON
                --attribList;
                VGPU_LOG_WGL("Unsupported context attrib: ", *attribList);
                attribList++;
#endif
                // If an unrecognized attribute is present in <attribList>,
                // then ERROR_INVALID_PARAMETER is generated.
                SetLastError(ERROR_INVALID_PARAMETER);
                return ContextVersion::Undefined;
            }
            attribList++;
        }
    }

    bool debug{false};
    bool forward_compatible{false};
    debug = (context_flags & WGL_CONTEXT_DEBUG_BIT_ARB) != 0x0;
    forward_compatible = (context_flags & WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB) != 0x0;

    bool core_profile{false};
    bool compatibility_profile{false};
    core_profile = (context_profile_mask & WGL_CONTEXT_CORE_PROFILE_BIT_ARB) != 0x0;
    compatibility_profile = (context_profile_mask & WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB) != 0x0;
    // If attribute WGL_CONTEXT_PROFILE_MASK_ARB has no bits set; has any
    // bits set other than WGL_CONTEXT_CORE_PROFILE_BIT_ARB and
    // WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB; has more than one of
    // these bits set; or if the implementation does not supported the
    // requested profile, then ERROR_INVALID_PROFILE_ARB is generated.
    if (!core_profile && !compatibility_profile) {
        SetLastError(ERROR_INVALID_PROFILE_ARB);
        return ContextVersion::Undefined;
    }
    if (context_profile_mask & ~(WGL_CONTEXT_CORE_PROFILE_BIT_ARB | WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB)) {
        SetLastError(ERROR_INVALID_PROFILE_ARB);
        return ContextVersion::Undefined;
    }
    if (core_profile && compatibility_profile) {
        SetLastError(ERROR_INVALID_PROFILE_ARB);
        return ContextVersion::Undefined;
    }

    ContextVersion cv = ContextVersion_ToContextVersion(major_version, minor_version);

    if (cv == ContextVersion::Version_1_0) {
        cv = Constants::supported_context_versions_.back();
    }

    if (cv == ContextVersion::Undefined || !Constants::IsSupportedVersion(cv)) {
        SetLastError(ERROR_INVALID_VERSION_ARB);
        return ContextVersion::Undefined;
    }

    // Feature deprecation was introduced with OpenGL 3.0, so forward-compatible
    // contexts may only be requested for OpenGL 3.0 and above.
    if (forward_compatible && (cv < ContextVersion::Version_3_0)) {
        SetLastError(ERROR_INVALID_VERSION_ARB);
        return ContextVersion::Undefined;
    }

    if (compatibility_profile) {
        cv = Constants::supported_context_versions_.back();
    }

    return ContextDescriptor{cv, layer_plane, debug, forward_compatible, core_profile, compatibility_profile};
}

uintptr_t Win32Driver::CreateContext(HDC hdc, const ContextDescriptor &cd, uintptr_t shared_context /* = 0 */, bool from_attribs /* = false */) {
    std::unique_lock<std::mutex> ul{mutex_};
    using PFNWGLCREATECONTEXTPROC = HGLRC(WINAPI *)(HDC);
    static PFNWGLCREATECONTEXTPROC wglCreateContext{nullptr};
    if (!wglCreateContext) {
        HMODULE opengl_lib = GetModuleHandleA("opengl32.dll");
        VGPU_ASSERT(opengl_lib, "Getting opengl32.dll handle");
        wglCreateContext = reinterpret_cast<PFNWGLCREATECONTEXTPROC>(GetProcAddress(opengl_lib, "wglCreateContext"));
        VGPU_ASSERT(wglCreateContext, "Getting wglCreateContext proc address");
    }

    if (cd.GetVersion() == ContextVersion::Undefined)
        return 0;

    VGPU_ASSERT(cd.GetLayerPlane() == 0, "Layer planes not supported");
    if (cd.GetLayerPlane() != 0) {
        return 0;
    }

    // If the server does not have enough resources to allocate the new
    // context, then ERROR_NO_SYSTEM_RESOURCES is generated
    if (contexts_.size() >= Constants::max_contexts_num_) {
        SetLastError(ERROR_NO_SYSTEM_RESOURCES);
        return 0;
    }

    // If <hShareContext> is neither zero nor a valid context handle, then
    // ERROR_INVALID_OPERATION is generated.
    auto context_it = contexts_.find(shared_context);
    if (shared_context != 0 && context_it == contexts_.end()) {
        SetLastError(ERROR_INVALID_OPERATION);
        return 0;
    }

    thread_pfd_index_locked_ = true;
    ContextDescriptor complete_cd
        {cd, pfds_[thread_current_pfd_index_].color_format, pfds_[thread_current_pfd_index_].depthstencil_format};

    while (contexts_.count(++last_context_id_created_) != 0 || last_context_id_created_ == 0);
    contexts_.emplace(
        last_context_id_created_,
        std::make_shared<Win32Context>(hdc, complete_cd, vgpu_, shared_context == 0 ? 0 : context_it->second));
    ul.unlock();

    if (from_attribs) {
        context_creation_notifier_ = last_context_id_created_;
        return reinterpret_cast<uintptr_t>(wglCreateContext(hdc));
    }

    return last_context_id_created_;
}

std::vector<Win32Driver::PixelFormatEntity> Win32Driver::pfds_{};
thread_local uintptr_t Win32Driver::context_creation_notifier_{0x0};
thread_local uintptr_t Win32Driver::thread_current_pfd_index_{0x0};
thread_local bool Win32Driver::thread_pfd_index_locked_{false};
thread_local std::shared_ptr<Win32Context> Win32Driver::thread_current_contex_{nullptr};

bool Win32Driver::PixelFormatEntity::operator<(const Win32Driver::PixelFormatEntity &rhs) const {
    return memcmp(&pfd, &rhs.pfd, sizeof(PIXELFORMATDESCRIPTOR)) < 0;
}

}
