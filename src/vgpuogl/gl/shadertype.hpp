#ifndef VGPUGLSHADERTYPE_H
#define VGPUGLSHADERTYPE_H

#include "vgpuicd.hpp"
#include <ostream>

namespace vgpu {
class ContextDescriptor;
namespace gl {

enum class ShaderType : GLenum {
    CumputeShader           = GL_COMPUTE_SHADER,
    VertexShader            = GL_VERTEX_SHADER,
    TessControlShader       = GL_TESS_CONTROL_SHADER,
    TessEvaluationShader    = GL_TESS_EVALUATION_SHADER,
    GeometryShader          = GL_GEOMETRY_SHADER,
    FragmentShader          = GL_FRAGMENT_SHADER
};

bool ToShaderType(GLenum arg, ShaderType &shader_type, const ContextDescriptor &cd);
std::ostream &operator<<(std::ostream &os, ShaderType shader_type);

}
}

#endif // !VGPUGLSHADERTYPE_H
