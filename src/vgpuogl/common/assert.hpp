#ifndef VGPUASSERT_H
#define VGPUASSERT_H

#if _DEBUG
# include "assert/assert_impl.hpp"

# define VGPU_ASSERT(test, msg) vgpu::Assert(!!(test), std::string(#test) + " - " + msg)
# define VGPU_ERROR(msg) vgpu::Assert(false, msg)
# define VGPU_NOT_IMPLEMENTED() vgpu::Assert(false, __FUNCTION__ "{" __FILE__ ":" + std::to_string(__LINE__) + "}")
#else // !_DEBUG
# define VGPU_ASSERT(test, msg)
# define VGPU_ERROR(msg)
# define VGPU_NOT_IMPLEMENTED()
#endif // _DEBUG

# define VGPU_STATIC_ASSERT(test, msg) static_assert(!!(test), #test " - " msg)
# define VGPU_STATIC_ERROR(msg) static_assert(false, msg)

#endif // !VGPUASSERT_H
