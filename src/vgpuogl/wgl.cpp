#include "vgpuicd.hpp"
#include "win32driver.hpp"
#include "common/logger.hpp"
#include "common/assert.hpp"

extern vgpu::Win32Driver g_driver;

BOOL GLAPIENTRY wglGetPixelFormatAttribivARB(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int *piAttributes, int *piValues) {
    VGPU_LOG_WGL_FUNCTION(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, piValues);
    VGPU_ASSERT(iLayerPlane == 0, "Layer planes not supported");
    VGPU_NOT_IMPLEMENTED();
    return FALSE;
}

BOOL GLAPIENTRY wglGetPixelFormatAttribivEXT(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, int *piAttributes, int *piValues) {
    VGPU_LOG_WGL_FUNCTION(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, piValues);
    return wglGetPixelFormatAttribivARB(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, piValues);
}

BOOL GLAPIENTRY wglGetPixelFormatAttribfvARB(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int *piAttributes, FLOAT *pfValues) {
    VGPU_LOG_WGL_FUNCTION(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, pfValues);
    VGPU_ASSERT(iLayerPlane == 0, "Layer planes not supported");
    VGPU_NOT_IMPLEMENTED();
    return FALSE;
}

BOOL GLAPIENTRY wglGetPixelFormatAttribfvEXT(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, int *piAttributes, FLOAT *pfValues) {
    VGPU_LOG_WGL_FUNCTION(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, pfValues);
    return wglGetPixelFormatAttribfvARB(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, pfValues);
}

BOOL GLAPIENTRY wglChoosePixelFormatARB(HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats) {
    VGPU_LOG_WGL_FUNCTION(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
    return g_driver.wgl_ChoosePixelFormat(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
}

BOOL GLAPIENTRY wglChoosePixelFormatEXT(HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats) {
    VGPU_LOG_WGL_FUNCTION(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
    return wglChoosePixelFormatARB(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
}

HGLRC GLAPIENTRY wglCreateContextAttribsARB(HDC hdc, HGLRC hShareContext, const int *attribList) {
    VGPU_LOG_WGL_FUNCTION(hdc, hShareContext, attribList);
    return g_driver.wgl_CreateContextAttribs(hdc, hShareContext, attribList);
}
