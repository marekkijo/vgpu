#ifndef VGPUVGPU_H
#define VGPUVGPU_H

#include <memory>
#include <queue>
#include <mutex>
#include "common/assert.hpp"
#include "common/logger.hpp"
#include "command_buffer/command.hpp"
#include "command_buffer/commandparam.hpp"

namespace vgpu {

class VGPU {
public:
    VGPU();
    void ExecutionLoop();
    template<typename... Targs>
    void PushCommand(Command command, Targs&... args) {
        VGPU_LOG_VGPU_FUNCTION(command, args...);
        switch (command) {
        case Command::Kill:
            PushCommandSet({command, std::make_unique<CommandParam>()});
            return;
        case Command::None:
            return;
        default:
            VGPU_NOT_IMPLEMENTED();
            return;
        }
    }
    void FlushCommandBuffer();
    void FinishCommandBuffer();

private:
    void PushCommandSet(std::pair<Command, std::unique_ptr<CommandParam>> &&command_set);
    std::pair<Command, std::unique_ptr<CommandParam>> PopCommandSet();
    void DropCommandBuffer();

    std::queue<std::pair<Command, std::unique_ptr<CommandParam>>> command_buffer_;
    std::mutex command_buffer_mutex_;

    std::mutex execution_mutex_;
    std::condition_variable execution_start_cond_var_;
    std::condition_variable execution_end_cond_var_;
};

}

#endif // !VGPUVGPU_H
