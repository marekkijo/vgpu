#ifndef VGPUCONTEXTSHAREDRESOURCES_H
#define VGPUCONTEXTSHAREDRESOURCES_H

#include <map>
#include <memory>
#include "vgpuicd.hpp"
#include "gl/enums.hpp"

namespace vgpu {

class Shader;

class ContextSharedResources {
public:
    ContextSharedResources();
    GLuint CreateShader(gl::ShaderType shader_type);
    std::shared_ptr<Shader> GetShader(GLuint shader_id);
    const std::shared_ptr<Shader> GetShader(GLuint shader_id) const;

private:
    std::map<GLuint, std::shared_ptr<Shader>> shaders_{};
    GLuint last_shader_id_created_{0};
};

}

#endif // !VGPUCONTEXTSHAREDRESOURCES_H
