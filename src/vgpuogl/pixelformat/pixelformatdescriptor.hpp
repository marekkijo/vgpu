#ifndef VGPUPIXELFORMATDESCRIPTOR_H
#define VGPUPIXELFORMATDESCRIPTOR_H

#include <map>
#include <array>
#include "pixelformat.hpp"
#include "channeltype.hpp"

namespace vgpu {

class PixelFormatDescriptor {
public:
    static const PixelFormatDescriptor &Get(PixelFormat pf);
    bool HasChannel(ChannelType ct) const;
    int ChannelTypeToIndex(ChannelType ct) const;
    ChannelType IndexToChannelType(int index) const;
    uint8_t GetBits(ChannelType ct) const;
    uint8_t GetBits(int index) const;
    uint8_t GetShift(ChannelType ct) const;
    uint8_t GetShift(int index) const;
    uint8_t GetTexelSize() const;

private:
    static std::map<PixelFormat, PixelFormatDescriptor> pfds_;

    explicit PixelFormatDescriptor(PixelFormat pf);
    void UpdateChannelIndices();
    void UpdateChannelBits();
    void UpdateChannelShift();
    void UpdateTexelSize();

    PixelFormat pf_{};
    std::array<ChannelType, 4> channels_{};
    std::array<uint8_t, 4> bits_{};
    std::array<uint8_t, 4> shift_{};
    uint8_t texel_size{};
};

using PFD = PixelFormatDescriptor;

}

#endif // !VGPUPIXELFORMATDESCRIPTOR_H
