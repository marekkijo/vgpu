#include "command.hpp"

namespace vgpu {

std::ostream &operator<<(std::ostream &os, Command command) {
    switch (command) {
    case Command::None:
        os << "Command::None";
        break;
    case Command::Kill:
        os << "Command::Kill";
        break;
    }

    return os;
}

}
