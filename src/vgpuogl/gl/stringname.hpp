#ifndef VGPUGLSTRINGNAME_H
#define VGPUGLSTRINGNAME_H

#include "vgpuicd.hpp"
#include <ostream>

namespace vgpu {
namespace gl {

enum class StringName : GLenum {
    Vendor                  = GL_VENDOR,
    Renderer                = GL_RENDERER,
    Version                 = GL_VERSION,
    Extensions              = GL_EXTENSIONS,
    ShadingLanguageVersion  = GL_SHADING_LANGUAGE_VERSION
};

bool ToStringName(GLenum arg, StringName &string_name);
std::ostream &operator<<(std::ostream &os, StringName string_name);

}
}

#endif // !VGPUGLSTRINGNAME_H