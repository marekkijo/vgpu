#ifndef VGPUGLERROR_H
#define VGPUGLERROR_H

#include "vgpuicd.hpp"
#include <ostream>

namespace vgpu {
namespace gl {

enum class Error : GLenum {
    NoError                     = GL_NO_ERROR,
    InvalidEnum                 = GL_INVALID_ENUM,
    InvalidValue                = GL_INVALID_VALUE,
    InvalidOperation            = GL_INVALID_OPERATION,
    InvalidFrameBufferOperation = GL_INVALID_FRAMEBUFFER_OPERATION,
    OutOfMemory                 = GL_OUT_OF_MEMORY,
    StackUnderflow              = GL_STACK_UNDERFLOW,
    StackOverflow               = GL_STACK_OVERFLOW
};

bool ToError(GLenum arg, Error &error);
std::ostream &operator<<(std::ostream &os, Error error);

}
}

#endif // !VGPUGLERROR_H
