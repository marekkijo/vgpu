#include "contextsharedresources.hpp"
#include "common/assert.hpp"
#include "shaders/shader.hpp"
#include "shaders/vertexshader.hpp"
#include "shaders/fragmentshader.hpp"

namespace vgpu {

ContextSharedResources::ContextSharedResources() {
}

GLuint ContextSharedResources::CreateShader(gl::ShaderType shader_type) {
    switch (shader_type) {
    case gl::ShaderType::CumputeShader:
        VGPU_NOT_IMPLEMENTED();
        break;
    case gl::ShaderType::VertexShader:
        while (shaders_.count(++last_shader_id_created_) != 0 || last_shader_id_created_ == 0);
        shaders_.emplace(
            last_shader_id_created_,
            std::make_shared<VertexShader>());

        return last_shader_id_created_;
    case gl::ShaderType::TessControlShader:
    case gl::ShaderType::TessEvaluationShader:
    case gl::ShaderType::GeometryShader:
        VGPU_NOT_IMPLEMENTED();
        break;
    case gl::ShaderType::FragmentShader:
        while (shaders_.count(++last_shader_id_created_) != 0 || last_shader_id_created_ == 0);
        shaders_.emplace(
            last_shader_id_created_,
            std::make_shared<FragmentShader>());

        return last_shader_id_created_;
    }

    return 0;
}

std::shared_ptr<Shader> ContextSharedResources::GetShader(GLuint shader_id) {
    if (shaders_.count(shader_id) != 0) {
        return shaders_[shader_id];
    }
    return nullptr;
}

const std::shared_ptr<Shader> ContextSharedResources::GetShader(GLuint shader_id) const {
    if (shaders_.count(shader_id) != 0) {
        return shaders_.at(shader_id);
    }
    return nullptr;
}

}
