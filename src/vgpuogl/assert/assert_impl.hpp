#ifndef VGPUASSERT_IMPL_H
#define VGPUASSERT_IMPL_H

#include <string>

namespace vgpu {

#if _DEBUG
void Assert(bool test, const std::string &msg);
#endif // _DEBUG

}

#endif // !VGPUASSERT_IMPL_H
