#ifndef VGPUCOMMAND_H
#define VGPUCOMMAND_H

#include <ostream>

namespace vgpu {

enum class Command {
    None,
    Kill
};

std::ostream &operator<<(std::ostream &os, Command command);

}

#endif // !VGPUCOMMAND_H
