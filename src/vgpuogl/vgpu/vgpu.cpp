#include "vgpu.hpp"
#include <iostream>

namespace vgpu {

VGPU::VGPU() {
}

void VGPU::ExecutionLoop() {
    VGPU_LOG_VGPU_FUNCTION();
    std::unique_lock<std::mutex> ul{execution_mutex_};
    bool alive = true;

    VGPU_LOG_VGPU("Execution loop born");
    do {
        execution_start_cond_var_.wait(ul);
        VGPU_LOG_VGPU("Execution triggered");

        std::pair<Command, std::unique_ptr<CommandParam>> command_set;
        do {
            command_set = PopCommandSet();
            switch (command_set.first) {
            case Command::Kill:
                VGPU_LOG_VGPU("Execution Command::Kill");
                DropCommandBuffer();
                alive = false;
                break;
            case Command::None:
            default:
                break;
            }
        } while (command_set.first != Command::None);

        VGPU_LOG_VGPU("Execution done");
        execution_end_cond_var_.notify_all();
    } while (alive);
    VGPU_LOG_VGPU("Execution loop died");
}

void VGPU::FlushCommandBuffer() {
    VGPU_LOG_VGPU_FUNCTION();

    VGPU_LOG_VGPU("Attempting to FLUSH command buffer - without waiting");
    execution_start_cond_var_.notify_all();
}

void VGPU::FinishCommandBuffer() {
    VGPU_LOG_VGPU_FUNCTION();

    VGPU_LOG_VGPU("Attempting to FINISH command buffer - with waiting");
    execution_start_cond_var_.notify_all();

    std::unique_lock<std::mutex> ul{execution_mutex_};
    execution_end_cond_var_.wait(ul);
    VGPU_LOG_VGPU("Command buffer FINISHed");
}

void VGPU::PushCommandSet(std::pair<Command, std::unique_ptr<CommandParam>> &&command_set) {
    VGPU_LOG_VGPU_FUNCTION("&&command_set");
    std::lock_guard<std::mutex> lg{command_buffer_mutex_};
    command_buffer_.push(std::move(command_set));
}

std::pair<Command, std::unique_ptr<CommandParam>> VGPU::PopCommandSet() {
    VGPU_LOG_VGPU_FUNCTION();
    std::lock_guard<std::mutex> lg{command_buffer_mutex_};
    if (command_buffer_.empty()) {
        return std::pair<Command, std::unique_ptr<CommandParam>>{Command::None, nullptr};
    }
    auto command_set = std::move(command_buffer_.front());
    command_buffer_.pop();
    return command_set;
}

void VGPU::DropCommandBuffer() {
    VGPU_LOG_VGPU_FUNCTION();
    std::lock_guard<std::mutex> lg{command_buffer_mutex_};
    command_buffer_.swap(std::queue<std::pair<Command, std::unique_ptr<CommandParam>>>());
}

}
