#include "stringname.hpp"

namespace vgpu {
namespace gl {

bool ToStringName(GLenum arg, StringName &string_name) {
    StringName tmp_string_name = static_cast<StringName>(arg);

    switch (tmp_string_name) {
    case StringName::Vendor:
    case StringName::Renderer:
    case StringName::Version:
    case StringName::Extensions:
    case StringName::ShadingLanguageVersion:
        break;
    default:
        return false;
    }

    string_name = tmp_string_name;
    return true;
}

std::ostream &operator<<(std::ostream &os, StringName string_name) {
    switch (string_name) {
    case StringName::Vendor:
        os << "StringName::Vendor";
        break;
    case StringName::Renderer:
        os << "StringName::Renderer";
        break;
    case StringName::Version:
        os << "StringName::Version";
        break;
    case StringName::Extensions:
        os << "StringName::Extensions";
        break;
    case StringName::ShadingLanguageVersion:
        os << "StringName::ShadingLanguageVersion";
        break;
    }

    return os;
}

}
}
