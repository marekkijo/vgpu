#include "win32context.hpp"
#include "contextsharedresources.hpp"
#include "common/assert.hpp"
#include "common/logger.hpp"
#include "common/constants.hpp"
#include "vgpu/vgpu.hpp"

namespace vgpu {

Win32Context::Win32Context(HDC hdc, const ContextDescriptor &cd, std::shared_ptr<VGPU> vgpu, std::shared_ptr<Win32Context> shared_context /* = nullptr */) :
    hdc_(hdc),
    cd_{cd},
    vgpu_{vgpu} {
    SetSharedContext(shared_context);
}

PGLCLTPROCTABLE Win32Context::GetGL110ProcTable() {
    return const_cast<PGLCLTPROCTABLE>(&gl_proc_table_);
}

PROC Win32Context::wgl_GetProcAddress(LPCSTR lpszProc) {
    VGPU_LOG_CTX_FUNCTION(lpszProc);

    auto wgl_proc = wgl_proc_map_.find(lpszProc);
    if (wgl_proc != wgl_proc_map_.end())
        return static_cast<PROC>(wgl_proc->second);

    auto gl_proc = gl_proc_map_.find(lpszProc);
    if (gl_proc != gl_proc_map_.end())
        return static_cast<PROC>(gl_proc->second);

    return NULL;
}

void Win32Context::gl_Finish() {
    VGPU_LOG_CTX_FUNCTION();
    vgpu_->FinishCommandBuffer();
}

void Win32Context::gl_Flush() {
    VGPU_LOG_CTX_FUNCTION();
    vgpu_->FlushCommandBuffer();
}

void Win32Context::gl_SetError(gl::Error error) {
    VGPU_LOG_CTX_FUNCTION(error);
    error_ = error;
}

void Win32Context::gl_GetBooleanv(gl::ParamName param_name, GLboolean *data) {
    VGPU_LOG_CTX_FUNCTION(param_name, data);
    GetterUniform getter;
    bool has_next;
    size_t index = 0;
    do {
        has_next = gl_Get(param_name, index, getter);
        data[index] = getter.GetBoolean();
        index++;
    } while (has_next);
}

void Win32Context::gl_GetDoublev(gl::ParamName param_name, GLdouble *data) {
    VGPU_LOG_CTX_FUNCTION(param_name, data);
    GetterUniform getter;
    bool has_next;
    size_t index = 0;
    do {
        has_next = gl_Get(param_name, index, getter);
        data[index] = getter.GetDouble();
        index++;
    } while (has_next);
}

GLenum Win32Context::gl_GetError() {
    VGPU_LOG_CTX_FUNCTION();
    GLenum error = static_cast<GLenum>(error_);
    error_ = gl::Error::NoError;
    return error;
}

void Win32Context::gl_GetFloatv(gl::ParamName param_name, GLfloat *data) {
    VGPU_LOG_CTX_FUNCTION(param_name, data);
    GetterUniform getter;
    bool has_next;
    size_t index = 0;
    do {
        has_next = gl_Get(param_name, index, getter);
        data[index] = getter.GetFloat();
        index++;
    } while (has_next);
}

void Win32Context::gl_GetIntegerv(gl::ParamName param_name, GLint *data) {
    VGPU_LOG_CTX_FUNCTION(param_name, data);
    GetterUniform getter;
    bool has_next;
    size_t index = 0;
    do {
        has_next = gl_Get(param_name, index, getter);
        data[index] = getter.GetInteger();
        index++;
    } while (has_next);
}

const GLubyte *Win32Context::gl_GetString(gl::StringName name) const {
    VGPU_LOG_CTX_FUNCTION(name);
    const char *str{nullptr};

    switch (name) {
    case gl::StringName::Vendor:
        str = Constants::gl_vendor_;
        break;
    case gl::StringName::Renderer:
        str = Constants::gl_renderer_;
        break;
    case gl::StringName::Version:
        str = Constants::gl_version_.at(cd_.GetVersion());
        break;
    case gl::StringName::Extensions:
        str = Constants::gl_extensions_;
        break;
    case gl::StringName::ShadingLanguageVersion:
        str = Constants::gl_shading_language_version_.at(cd_.GetVersion());
        break;
    }

    return reinterpret_cast<const GLubyte *>(str);
}

GLuint Win32Context::gl_CreateShader(gl::ShaderType shader_type) {
    VGPU_LOG_CTX_FUNCTION(shader_type);
    return shared_resources_->CreateShader(shader_type);
}

void Win32Context::gl_ShaderSource(GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length) {
    VGPU_LOG_CTX_FUNCTION(shader, count, string, length);
    if (!shared_resources_->GetShader(shader)) {
        gl_SetError(vgpu::gl::Error::InvalidOperation);
        return;
    }

    if (count < 0) {
        gl_SetError(vgpu::gl::Error::InvalidValue);
        return;
    }

    std::string source;
    for (int i = 0; i < count; i++) {
        if (!length || length[i] < 0) {
            source += string[i];
        } else {
            source.append(string[i], length[i]);
        }
    }

    VGPU_LOG_CTX("Shader source: ", source);
}

void Win32Context::gl_GetInteger64v(gl::ParamName param_name, GLint64 *data) {
    VGPU_LOG_CTX_FUNCTION(param_name, data);
    GetterUniform getter;
    bool has_next;
    size_t index = 0;
    do {
        has_next = gl_Get(param_name, index, getter);
        data[index] = getter.GetInteger64();
        index++;
    } while (has_next);
}

const ContextDescriptor &Win32Context::GetContextDescriptor() const {
    return cd_;
}

void Win32Context::SetSharedContext(std::shared_ptr<Win32Context> shared_context) {
    if (!shared_context && !shared_resources_) {
        shared_resources_ = std::make_shared<ContextSharedResources>();
    } else {
        VGPU_ASSERT(shared_context->shared_resources_, "Shared context has no initialized resources");
        shared_resources_ = shared_context->shared_resources_;
    }
}

bool Win32Context::gl_Get(gl::ParamName param_name, size_t index, GetterUniform &data) {
    switch (param_name) {
    case gl::ParamName::ActiveTexture:
    case gl::ParamName::AliasedLineWidthRange:
    case gl::ParamName::ArrayBufferBinding:
    case gl::ParamName::Blend:
    case gl::ParamName::BlendColor:
    case gl::ParamName::BlendDstAlpha:
    case gl::ParamName::BlendDstRgb:
    case gl::ParamName::BlendEquationRgb:
    case gl::ParamName::BlendEquationAlpha:
    case gl::ParamName::BlendSrcAlpha:
    case gl::ParamName::BlendSrcRgb:
    case gl::ParamName::ColorClearValue:
    case gl::ParamName::ColorLogicOp:
    case gl::ParamName::ColorWritemask:
    case gl::ParamName::CompressedTextureFormats:
    case gl::ParamName::MaxComputeShaderStorageBlocks:
    case gl::ParamName::MaxCombinedShaderStorageBlocks:
    case gl::ParamName::MaxComputeUniformBlocks:
    case gl::ParamName::MaxComputeTextureImageUnits:
    case gl::ParamName::MaxComputeUniformComponents:
    case gl::ParamName::MaxComputeAtomicCounters:
    case gl::ParamName::MaxComputeAtomicCounterBuffers:
    case gl::ParamName::MaxCombinedComputeUniformComponents:
    case gl::ParamName::MaxComputeWorkGroupInvocations:
    case gl::ParamName::MaxComputeWorkGroupCount:
    case gl::ParamName::MaxComputeWorkGroupSize:
    case gl::ParamName::DispatchIndirectBufferBinding:
    case gl::ParamName::MaxDebugGroupStackDepth:
    case gl::ParamName::DebugGroupStackDepth:
    case gl::ParamName::ContextFlags:
    case gl::ParamName::CullFace:
    case gl::ParamName::CurrentProgram:
    case gl::ParamName::DepthClearValue:
    case gl::ParamName::DepthFunc:
    case gl::ParamName::DepthRange:
    case gl::ParamName::DepthTest:
    case gl::ParamName::DepthWritemask:
    case gl::ParamName::Dither:
    case gl::ParamName::Doublebuffer:
    case gl::ParamName::DrawBuffer:
    case gl::ParamName::DrawBuffer0:
    case gl::ParamName::DrawBuffer1:
    case gl::ParamName::DrawBuffer2:
    case gl::ParamName::DrawBuffer3:
    case gl::ParamName::DrawBuffer4:
    case gl::ParamName::DrawBuffer5:
    case gl::ParamName::DrawBuffer6:
    case gl::ParamName::DrawBuffer7:
    case gl::ParamName::DrawBuffer8:
    case gl::ParamName::DrawBuffer9:
    case gl::ParamName::DrawBuffer10:
    case gl::ParamName::DrawBuffer11:
    case gl::ParamName::DrawBuffer12:
    case gl::ParamName::DrawBuffer13:
    case gl::ParamName::DrawBuffer14:
    case gl::ParamName::DrawBuffer15:
    case gl::ParamName::DrawFramebufferBinding:
    case gl::ParamName::ReadFramebufferBinding:
    case gl::ParamName::ElementArrayBufferBinding:
    case gl::ParamName::FragmentShaderDerivativeHint:
    case gl::ParamName::ImplementationColorReadFormat:
    case gl::ParamName::ImplementationColorReadType:
    case gl::ParamName::LineSmooth:
    case gl::ParamName::LineSmoothHint:
    case gl::ParamName::LineWidth:
    case gl::ParamName::LineWidthRange:
    case gl::ParamName::LineWidthGranularity:
    case gl::ParamName::LayerProvokingVertex:
    case gl::ParamName::LogicOpMode:
    case gl::ParamName::MajorVersion:
    case gl::ParamName::Max3dTextureSize:
    case gl::ParamName::MaxArrayTextureLayers:
    case gl::ParamName::MaxClipDistances:
    case gl::ParamName::MaxColorTextureSamples:
    case gl::ParamName::MaxCombinedAtomicCounters:
    case gl::ParamName::MaxCombinedFragmentUniformComponents:
    case gl::ParamName::MaxCombinedGeometryUniformComponents:
    case gl::ParamName::MaxCombinedTextureImageUnits:
    case gl::ParamName::MaxCombinedUniformBlocks:
    case gl::ParamName::MaxCombinedVertexUniformComponents:
    case gl::ParamName::MaxCubeMapTextureSize:
    case gl::ParamName::MaxDepthTextureSamples:
    case gl::ParamName::MaxDrawBuffers:
    case gl::ParamName::MaxDualSourceDrawBuffers:
    case gl::ParamName::MaxElementsIndices:
    case gl::ParamName::MaxElementsVertices:
    case gl::ParamName::MaxFragmentAtomicCounters:
    case gl::ParamName::MaxFragmentShaderStorageBlocks:
    case gl::ParamName::MaxFragmentInputComponents:
    case gl::ParamName::MaxFragmentUniformComponents:
    case gl::ParamName::MaxFragmentUniformVectors:
    case gl::ParamName::MaxFragmentUniformBlocks:
    case gl::ParamName::MaxFramebufferWidth:
    case gl::ParamName::MaxFramebufferHeight:
    case gl::ParamName::MaxFramebufferLayers:
    case gl::ParamName::MaxFramebufferSamples:
    case gl::ParamName::MaxGeometryAtomicCounters:
    case gl::ParamName::MaxGeometryShaderStorageBlocks:
    case gl::ParamName::MaxGeometryInputComponents:
    case gl::ParamName::MaxGeometryOutputComponents:
    case gl::ParamName::MaxGeometryTextureImageUnits:
    case gl::ParamName::MaxGeometryUniformBlocks:
    case gl::ParamName::MaxGeometryUniformComponents:
    case gl::ParamName::MaxIntegerSamples:
    case gl::ParamName::MinMapBufferAlignment:
    case gl::ParamName::MaxLabelLength:
    case gl::ParamName::MaxProgramTexelOffset:
    case gl::ParamName::MinProgramTexelOffset:
    case gl::ParamName::MaxRectangleTextureSize:
    case gl::ParamName::MaxRenderbufferSize:
    case gl::ParamName::MaxSampleMaskWords:
    case gl::ParamName::MaxServerWaitTimeout:
    case gl::ParamName::MaxShaderStorageBufferBindings:
    case gl::ParamName::MaxTessControlAtomicCounters:
    case gl::ParamName::MaxTessEvaluationAtomicCounters:
    case gl::ParamName::MaxTessControlShaderStorageBlocks:
    case gl::ParamName::MaxTessEvaluationShaderStorageBlocks:
    case gl::ParamName::MaxTextureBufferSize:
    case gl::ParamName::MaxTextureImageUnits:
    case gl::ParamName::MaxTextureLodBias:
    case gl::ParamName::MaxTextureSize:
    case gl::ParamName::MaxUniformBufferBindings:
    case gl::ParamName::MaxUniformBlockSize:
    case gl::ParamName::MaxUniformLocations:
    case gl::ParamName::MaxVaryingComponents:
    case gl::ParamName::MaxVaryingVectors:
    case gl::ParamName::MaxVertexAtomicCounters:
    case gl::ParamName::MaxVertexAttribs:
    case gl::ParamName::MaxVertexShaderStorageBlocks:
    case gl::ParamName::MaxVertexTextureImageUnits:
    case gl::ParamName::MaxVertexUniformComponents:
    case gl::ParamName::MaxVertexUniformVectors:
    case gl::ParamName::MaxVertexOutputComponents:
    case gl::ParamName::MaxVertexUniformBlocks:
        VGPU_NOT_IMPLEMENTED();
        break;
    case gl::ParamName::MaxViewportDims:
        data.Set(Constants::max_viewport_dims_[index]);
        return (index + 1) < Constants::max_viewport_dims_.size();
    case gl::ParamName::MaxViewports:
    case gl::ParamName::MinorVersion:
    case gl::ParamName::NumCompressedTextureFormats:
    case gl::ParamName::NumExtensions:
    case gl::ParamName::NumProgramBinaryFormats:
    case gl::ParamName::NumShaderBinaryFormats:
    case gl::ParamName::PackAlignment:
    case gl::ParamName::PackImageHeight:
    case gl::ParamName::PackLsbFirst:
    case gl::ParamName::PackRowLength:
    case gl::ParamName::PackSkipImages:
    case gl::ParamName::PackSkipPixels:
    case gl::ParamName::PackSkipRows:
    case gl::ParamName::PackSwapBytes:
    case gl::ParamName::PixelPackBufferBinding:
    case gl::ParamName::PixelUnpackBufferBinding:
    case gl::ParamName::PointFadeThresholdSize:
    case gl::ParamName::PrimitiveRestartIndex:
    case gl::ParamName::ProgramBinaryFormats:
    case gl::ParamName::ProgramPipelineBinding:
    case gl::ParamName::ProgramPointSize:
    case gl::ParamName::ProvokingVertex:
    case gl::ParamName::PointSize:
    case gl::ParamName::PointSizeGranularity:
    case gl::ParamName::PointSizeRange:
    case gl::ParamName::PolygonOffsetFactor:
    case gl::ParamName::PolygonOffsetUnits:
    case gl::ParamName::PolygonOffsetFill:
    case gl::ParamName::PolygonOffsetLine:
    case gl::ParamName::PolygonOffsetPoint:
    case gl::ParamName::PolygonSmooth:
    case gl::ParamName::PolygonSmoothHint:
    case gl::ParamName::ReadBuffer:
    case gl::ParamName::RenderbufferBinding:
    case gl::ParamName::SampleBuffers:
    case gl::ParamName::SampleCoverageValue:
    case gl::ParamName::SampleCoverageInvert:
    case gl::ParamName::SamplerBinding:
    case gl::ParamName::Samples:
    case gl::ParamName::ScissorBox:
    case gl::ParamName::ScissorTest:
    case gl::ParamName::ShaderCompiler:
    case gl::ParamName::ShaderStorageBufferBinding:
    case gl::ParamName::ShaderStorageBufferOffsetAlignment:
    case gl::ParamName::ShaderStorageBufferStart:
    case gl::ParamName::ShaderStorageBufferSize:
    case gl::ParamName::StencilBackFail:
    case gl::ParamName::StencilBackFunc:
    case gl::ParamName::StencilBackPassDepthFail:
    case gl::ParamName::StencilBackPassDepthPass:
    case gl::ParamName::StencilBackRef:
    case gl::ParamName::StencilBackValueMask:
    case gl::ParamName::StencilBackWritemask:
    case gl::ParamName::StencilClearValue:
    case gl::ParamName::StencilFail:
    case gl::ParamName::StencilFunc:
    case gl::ParamName::StencilPassDepthFail:
    case gl::ParamName::StencilPassDepthPass:
    case gl::ParamName::StencilRef:
    case gl::ParamName::StencilTest:
    case gl::ParamName::StencilValueMask:
    case gl::ParamName::StencilWritemask:
    case gl::ParamName::Stereo:
    case gl::ParamName::SubpixelBits:
    case gl::ParamName::TextureBinding1d:
    case gl::ParamName::TextureBinding1dArray:
    case gl::ParamName::TextureBinding2d:
    case gl::ParamName::TextureBinding2dArray:
    case gl::ParamName::TextureBinding2dMultisample:
    case gl::ParamName::TextureBinding2dMultisampleArray:
    case gl::ParamName::TextureBinding3d:
    case gl::ParamName::TextureBindingBuffer:
    case gl::ParamName::TextureBindingCubeMap:
    case gl::ParamName::TextureBindingRectangle:
    case gl::ParamName::TextureCompressionHint:
    case gl::ParamName::TextureBufferOffsetAlignment:
    case gl::ParamName::Timestamp:
    case gl::ParamName::TransformFeedbackBufferBinding:
    case gl::ParamName::TransformFeedbackBufferStart:
    case gl::ParamName::TransformFeedbackBufferSize:
    case gl::ParamName::UniformBufferBinding:
    case gl::ParamName::UniformBufferOffsetAlignment:
    case gl::ParamName::UniformBufferSize:
    case gl::ParamName::UniformBufferStart:
    case gl::ParamName::UnpackAlignment:
    case gl::ParamName::UnpackImageHeight:
    case gl::ParamName::UnpackLsbFirst:
    case gl::ParamName::UnpackRowLength:
    case gl::ParamName::UnpackSkipImages:
    case gl::ParamName::UnpackSkipPixels:
    case gl::ParamName::UnpackSkipRows:
    case gl::ParamName::UnpackSwapBytes:
    case gl::ParamName::VertexArrayBinding:
    case gl::ParamName::VertexBindingDivisor:
    case gl::ParamName::VertexBindingOffset:
    case gl::ParamName::VertexBindingStride:
    case gl::ParamName::MaxVertexAttribRelativeOffset:
    case gl::ParamName::MaxVertexAttribBindings:
    case gl::ParamName::Viewport:
    case gl::ParamName::ViewportBoundsRange:
    case gl::ParamName::ViewportIndexProvokingVertex:
    case gl::ParamName::ViewportSubpixelBits:
    case gl::ParamName::MaxElementIndex:
        VGPU_NOT_IMPLEMENTED();
        break;
    }
    return false;
}

const GLCLTPROCTABLE Win32Context::gl_proc_table_{
    ogl_110_entries_,
    {
        &glNewList,
        &glEndList,
        &glCallList,
        &glCallLists,
        &glDeleteLists,
        &glGenLists,
        &glListBase,
        &glBegin,
        &glBitmap,
        &glColor3b,
        &glColor3bv,
        &glColor3d,
        &glColor3dv,
        &glColor3f,
        &glColor3fv,
        &glColor3i,
        &glColor3iv,
        &glColor3s,
        &glColor3sv,
        &glColor3ub,
        &glColor3ubv,
        &glColor3ui,
        &glColor3uiv,
        &glColor3us,
        &glColor3usv,
        &glColor4b,
        &glColor4bv,
        &glColor4d,
        &glColor4dv,
        &glColor4f,
        &glColor4fv,
        &glColor4i,
        &glColor4iv,
        &glColor4s,
        &glColor4sv,
        &glColor4ub,
        &glColor4ubv,
        &glColor4ui,
        &glColor4uiv,
        &glColor4us,
        &glColor4usv,
        &glEdgeFlag,
        &glEdgeFlagv,
        &glEnd,
        &glIndexd,
        &glIndexdv,
        &glIndexf,
        &glIndexfv,
        &glIndexi,
        &glIndexiv,
        &glIndexs,
        &glIndexsv,
        &glNormal3b,
        &glNormal3bv,
        &glNormal3d,
        &glNormal3dv,
        &glNormal3f,
        &glNormal3fv,
        &glNormal3i,
        &glNormal3iv,
        &glNormal3s,
        &glNormal3sv,
        &glRasterPos2d,
        &glRasterPos2dv,
        &glRasterPos2f,
        &glRasterPos2fv,
        &glRasterPos2i,
        &glRasterPos2iv,
        &glRasterPos2s,
        &glRasterPos2sv,
        &glRasterPos3d,
        &glRasterPos3dv,
        &glRasterPos3f,
        &glRasterPos3fv,
        &glRasterPos3i,
        &glRasterPos3iv,
        &glRasterPos3s,
        &glRasterPos3sv,
        &glRasterPos4d,
        &glRasterPos4dv,
        &glRasterPos4f,
        &glRasterPos4fv,
        &glRasterPos4i,
        &glRasterPos4iv,
        &glRasterPos4s,
        &glRasterPos4sv,
        &glRectd,
        &glRectdv,
        &glRectf,
        &glRectfv,
        &glRecti,
        &glRectiv,
        &glRects,
        &glRectsv,
        &glTexCoord1d,
        &glTexCoord1dv,
        &glTexCoord1f,
        &glTexCoord1fv,
        &glTexCoord1i,
        &glTexCoord1iv,
        &glTexCoord1s,
        &glTexCoord1sv,
        &glTexCoord2d,
        &glTexCoord2dv,
        &glTexCoord2f,
        &glTexCoord2fv,
        &glTexCoord2i,
        &glTexCoord2iv,
        &glTexCoord2s,
        &glTexCoord2sv,
        &glTexCoord3d,
        &glTexCoord3dv,
        &glTexCoord3f,
        &glTexCoord3fv,
        &glTexCoord3i,
        &glTexCoord3iv,
        &glTexCoord3s,
        &glTexCoord3sv,
        &glTexCoord4d,
        &glTexCoord4dv,
        &glTexCoord4f,
        &glTexCoord4fv,
        &glTexCoord4i,
        &glTexCoord4iv,
        &glTexCoord4s,
        &glTexCoord4sv,
        &glVertex2d,
        &glVertex2dv,
        &glVertex2f,
        &glVertex2fv,
        &glVertex2i,
        &glVertex2iv,
        &glVertex2s,
        &glVertex2sv,
        &glVertex3d,
        &glVertex3dv,
        &glVertex3f,
        &glVertex3fv,
        &glVertex3i,
        &glVertex3iv,
        &glVertex3s,
        &glVertex3sv,
        &glVertex4d,
        &glVertex4dv,
        &glVertex4f,
        &glVertex4fv,
        &glVertex4i,
        &glVertex4iv,
        &glVertex4s,
        &glVertex4sv,
        &glClipPlane,
        &glColorMaterial,
        &glCullFace,
        &glFogf,
        &glFogfv,
        &glFogi,
        &glFogiv,
        &glFrontFace,
        &glHint,
        &glLightf,
        &glLightfv,
        &glLighti,
        &glLightiv,
        &glLightModelf,
        &glLightModelfv,
        &glLightModeli,
        &glLightModeliv,
        &glLineStipple,
        &glLineWidth,
        &glMaterialf,
        &glMaterialfv,
        &glMateriali,
        &glMaterialiv,
        &glPointSize,
        &glPolygonMode,
        &glPolygonStipple,
        &glScissor,
        &glShadeModel,
        &glTexParameterf,
        &glTexParameterfv,
        &glTexParameteri,
        &glTexParameteriv,
        &glTexImage1D,
        &glTexImage2D,
        &glTexEnvf,
        &glTexEnvfv,
        &glTexEnvi,
        &glTexEnviv,
        &glTexGend,
        &glTexGendv,
        &glTexGenf,
        &glTexGenfv,
        &glTexGeni,
        &glTexGeniv,
        &glFeedbackBuffer,
        &glSelectBuffer,
        &glRenderMode,
        &glInitNames,
        &glLoadName,
        &glPassThrough,
        &glPopName,
        &glPushName,
        &glDrawBuffer,
        &glClear,
        &glClearAccum,
        &glClearIndex,
        &glClearColor,
        &glClearStencil,
        &glClearDepth,
        &glStencilMask,
        &glColorMask,
        &glDepthMask,
        &glIndexMask,
        &glAccum,
        &glDisable,
        &glEnable,
        &glFinish,
        &glFlush,
        &glPopAttrib,
        &glPushAttrib,
        &glMap1d,
        &glMap1f,
        &glMap2d,
        &glMap2f,
        &glMapGrid1d,
        &glMapGrid1f,
        &glMapGrid2d,
        &glMapGrid2f,
        &glEvalCoord1d,
        &glEvalCoord1dv,
        &glEvalCoord1f,
        &glEvalCoord1fv,
        &glEvalCoord2d,
        &glEvalCoord2dv,
        &glEvalCoord2f,
        &glEvalCoord2fv,
        &glEvalMesh1,
        &glEvalPoint1,
        &glEvalMesh2,
        &glEvalPoint2,
        &glAlphaFunc,
        &glBlendFunc,
        &glLogicOp,
        &glStencilFunc,
        &glStencilOp,
        &glDepthFunc,
        &glPixelZoom,
        &glPixelTransferf,
        &glPixelTransferi,
        &glPixelStoref,
        &glPixelStorei,
        &glPixelMapfv,
        &glPixelMapuiv,
        &glPixelMapusv,
        &glReadBuffer,
        &glCopyPixels,
        &glReadPixels,
        &glDrawPixels,
        &glGetBooleanv,
        &glGetClipPlane,
        &glGetDoublev,
        &glGetError,
        &glGetFloatv,
        &glGetIntegerv,
        &glGetLightfv,
        &glGetLightiv,
        &glGetMapdv,
        &glGetMapfv,
        &glGetMapiv,
        &glGetMaterialfv,
        &glGetMaterialiv,
        &glGetPixelMapfv,
        &glGetPixelMapuiv,
        &glGetPixelMapusv,
        &glGetPolygonStipple,
        &glGetString,
        &glGetTexEnvfv,
        &glGetTexEnviv,
        &glGetTexGendv,
        &glGetTexGenfv,
        &glGetTexGeniv,
        &glGetTexImage,
        &glGetTexParameterfv,
        &glGetTexParameteriv,
        &glGetTexLevelParameterfv,
        &glGetTexLevelParameteriv,
        &glIsEnabled,
        &glIsList,
        &glDepthRange,
        &glFrustum,
        &glLoadIdentity,
        &glLoadMatrixf,
        &glLoadMatrixd,
        &glMatrixMode,
        &glMultMatrixf,
        &glMultMatrixd,
        &glOrtho,
        &glPopMatrix,
        &glPushMatrix,
        &glRotated,
        &glRotatef,
        &glScaled,
        &glScalef,
        &glTranslated,
        &glTranslatef,
        &glViewport,
        &glArrayElement,
        &glBindTexture,
        &glColorPointer,
        &glDisableClientState,
        &glDrawArrays,
        &glDrawElements,
        &glEdgeFlagPointer,
        &glEnableClientState,
        &glIndexPointer,
        &glIndexub,
        &glIndexubv,
        &glInterleavedArrays,
        &glNormalPointer,
        &glPolygonOffset,
        &glTexCoordPointer,
        &glVertexPointer,
        &glAreTexturesResident,
        &glCopyTexImage1D,
        &glCopyTexImage2D,
        &glCopyTexSubImage1D,
        &glCopyTexSubImage2D,
        &glDeleteTextures,
        &glGenTextures,
        &glGetPointerv,
        &glIsTexture,
        &glPrioritizeTextures,
        &glTexSubImage1D,
        &glTexSubImage2D,
        &glPopClientAttrib,
        &glPushClientAttrib
    }
};

const std::map<std::string, void *> Win32Context::wgl_proc_map_{
    {"wglGetPixelFormatAttribivARB", &wglGetPixelFormatAttribivARB},
    {"wglGetPixelFormatAttribivEXT", &wglGetPixelFormatAttribivEXT},
    {"wglGetPixelFormatAttribfvARB", &wglGetPixelFormatAttribfvARB},
    {"wglGetPixelFormatAttribfvEXT", &wglGetPixelFormatAttribfvEXT},
    {"wglChoosePixelFormatARB", &wglChoosePixelFormatARB},
    {"wglChoosePixelFormatEXT", &wglChoosePixelFormatEXT},
    {"wglCreateContextAttribsARB", &wglCreateContextAttribsARB}
};

const std::map<std::string, void *> Win32Context::gl_proc_map_{
// GL_VERSION_1_5
    {"glBindBuffer", &glBindBuffer},
    {"glDeleteBuffers", &glDeleteBuffers},
    {"glGenBuffers", &glGenBuffers},
    {"glBufferData", &glBufferData},
// GL_VERSION_2_0
    {"glAttachShader", &glAttachShader},
    {"glCompileShader", &glCompileShader},
    {"glCreateProgram", &glCreateProgram},
    {"glCreateShader", &glCreateShader},
    {"glDeleteProgram", &glDeleteProgram},
    {"glDeleteShader", &glDeleteShader},
    {"glDetachShader", &glDetachShader},
    {"glDisableVertexAttribArray", &glDisableVertexAttribArray},
    {"glEnableVertexAttribArray", &glEnableVertexAttribArray},
    {"glGetProgramiv", &glGetProgramiv},
    {"glGetProgramInfoLog", &glGetProgramInfoLog},
    {"glGetShaderiv", &glGetShaderiv},
    {"glGetShaderInfoLog", &glGetShaderInfoLog},
    {"glGetUniformLocation", &glGetUniformLocation},
    {"glLinkProgram", &glLinkProgram},
    {"glShaderSource", &glShaderSource},
    {"glUseProgram", &glUseProgram},
    {"glUniformMatrix4fv", &glUniformMatrix4fv},
    {"glVertexAttribPointer", &glVertexAttribPointer},
// GL_VERSION_3_0
    {"glVertexAttribIPointer", &glVertexAttribIPointer},
    {"glBindVertexArray", &glBindVertexArray},
    {"glDeleteVertexArrays", &glDeleteVertexArrays},
    {"glGenVertexArrays", &glGenVertexArrays},
// GL_VERSION_3_2
    {"glGetInteger64v", &glGetInteger64v},
// GL_VERSION_4_1
    {"glVertexAttribLPointer", &glVertexAttribLPointer},
// GL_VERSION_4_5
    {"glNamedBufferData", &glNamedBufferData}
};

}
