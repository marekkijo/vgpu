#ifndef VGPULOGGER_IMPL_H
#define VGPULOGGER_IMPL_H

#include <string>
#include <vector>
#include <chrono>
#include <memory>
#include <mutex>
#include <sstream>
#include <type_traits>
#include "logger_config.hpp"

namespace vgpu {

class LoggerImpl {
    using StrVector = std::vector<std::string>;
    using StrVectorIt = std::vector<std::string>::iterator;
public:
    LoggerImpl();

    template<typename... Targs>
    void Log(const std::string &label, Targs&... args) {
        std::stringstream ss;

#if VGPU_LOG_LABEL_ON || VGPU_LOG_TIMER_ON
        ss << "/* ";

# if VGPU_LOG_LABEL_ON
        if (label.length() > 0) {
            ss << "[" << label << "] ";
        }
# endif

# if VGPU_LOG_TIMER_ON
        std::chrono::milliseconds ms;
        std::chrono::high_resolution_clock::time_point now =
            std::chrono::high_resolution_clock::now();
        ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - timer_);
#  if VGPU_LOG_TIMER_DURATION
        timer_ = now;
#  endif
        ss << "{" << ms.count() << "} ";
# endif

        ss << "*/ ";
#endif

        ss << LogCombiner(args...) << std::endl;

        mtx_.lock();
        *log_ << ss.str();
        log_->flush();
        mtx_.unlock();
    }
    template<typename... Targs>
    void LogIf(bool cond, const std::string &label, Targs&... args) {
        if (cond) {
            Log(label, args...);
        }
    }

    template<typename T, typename... Targs>
    std::string LogCombiner(T arg, Targs&... args) {
        std::stringstream ss;
        ss << arg;
        return ss.str() + LogCombiner(args...);
    }

    std::string LogCombiner() {
        return std::string{};
    }

    template<typename... Targs>
    void FunctionLog(const std::string &label, const std::string &syntax, Targs&... args) {
        std::string function_name;
        StrVector function_args;
        ParseFunctionSyntax(syntax, &function_name, &function_args);
        std::string msg = function_name + "(" + ArgsToLog(function_args.begin(), function_args.end(), args...) + ");";
        Log(label, msg);
    }

private:
    std::chrono::high_resolution_clock::time_point timer_;
    std::unique_ptr<std::ostream, void(*)(std::ostream *)> log_{nullptr, LogDeleter};
    std::mutex mtx_;

    static void LogDeleter(std::ostream *log);
    static std::string GetLogFileFullPath();
    static void CreateLogDirectory();
    void ParseFunctionSyntax(const std::string &syntax, std::string *name, std::vector<std::string> *args);

    template<typename T, typename... Targs>
    std::string ArgsToLog(StrVectorIt &first, StrVectorIt &last, T arg, Targs&... args) {
        std::stringstream ss;

        WriteData(ss, arg);

#if VGPU_LOG_ARG_TYPE_ON
        ss << " /*" + *first << "*/";
#endif

        auto next = first + 1;
        if (next != last) {
            ss << ", ";
        }

        return ss.str() + ArgsToLog(next, last, args...);
    }

    template<typename T>
    void WriteData(std::stringstream &ss, T arg) {
        if (std::is_pointer<T>::value) {
            ss << "0x" << arg;
        } else if (VGPU_LOG_HEX_INTEGER && std::is_integral<T>::value) {
            ss << "0x" << std::hex << arg << std::dec;
        } else {
            ss << arg;
        }
    }

    template<>
    void WriteData(std::stringstream &ss, const char *arg) {
        if (arg) {
            ss << "\"" << arg << "\"";
        } else {
            ss << "nullptr";
        }
    }

    template<>
    void WriteData(std::stringstream &ss, char *arg) {
        const char *const_arg = arg;
        WriteData(ss, const_arg);
    }

    std::string ArgsToLog(StrVectorIt &first, StrVectorIt &last) {
        return std::string{};
    }
};

}

#endif // !VGPULOGGER_IMPL_H
